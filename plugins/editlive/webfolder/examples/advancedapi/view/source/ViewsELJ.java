/*
 * Copyright (c) 2013 Ephox Corporation.
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.ephox.editlive.*;

import com.ephox.editlive.common.EventListener;

/**
 * Class receives an instance of EditLive! for Java through the constructor. The EditLive! for Java instance uses class
 * ELJCustomViewListener to register the custom tag employeeTag. Contents of EditLive! for Java includes instances of the
 * employeeTag custom tag.
 *
 * @author Jack Mason
 */
public class ViewsELJ{

    /** Base class for EditLive! for Java */
    private ELJBean _bean;

    /**
     * Constructor is sent the current instance of EditLive! for Java. ELJCustomViewListener is registered with the
     * EditLive! for Java instance to detect double clicks on custom tags. The custom tag employeeTag is
     * registered with the EditLive! for Java instance.
     *
     * @param bean Current instance of EditLive! for Java, represented by an ELJBean class
     */
    public ViewsELJ(ELJBean bean) {
        _bean = bean;

        ELJCustomViewListener viewList = new ELJCustomViewListener(_bean);

        // defining images to appear when rendering employeeTag custom tag
        ImageIcon startIcon = new ImageIcon(getClass().getResource("images/empbmp2.jpg"));
        ImageIcon endIcon = new ImageIcon(getClass().getResource("images/empbmp2.jpg"));

        // Registering Custom Tag <employeeTag> Inline Tag with ELJBean
        _bean.registerCustomInlineTag("employeeTag", startIcon, "", endIcon, "", viewList);
    }
}