/*
 * Copyright (c) 2013 Ephox Corporation.
 */


import java.awt.event.*;
import java.util.*;
import java.io.*;

import java.awt.*;

import javax.swing.*;

import com.ephox.editlive.*;
import com.ephox.editlive.common.TextEvent;
import com.ephox.editlive.common.ViewClickListener;
import com.ephox.editlive.common.CustomView;

/**
 * Class implements the ViewClickListener class to detect double clicks on custom tags
 * existing in an instance of EditLive!. Upon a double click an instance of the
 * ViewDialog class is created and the EditLive! ELJBean variable is sent to this
 * class.
 *
 * @author Jack Mason
 */
public class ELJCustomViewListener implements ViewClickListener
{
    /** Base class for EditLive! */
    private ELJBean elj;

    /** Constructor. Sets private ELJBean property
     *
     * @param elj_ Instance of EditLive! registered with this class
     */
    public ELJCustomViewListener(ELJBean elj_)
    {
       elj = elj_;
    }

    /** Method called upon double clicking a custom tag in registered EditLive! instance.
     *
     * @param view CustomView class containg information about the custom tag clicked
     * @param e MouseEvent information sent with the double click of a custom tag.
     */
    public void mouseClicked(CustomView view, MouseEvent e)
    {
	   // Map variable contains attributes of the custom tag double clicked
       Map attrs = view.getTagAttributes();

       // Creating a instance of the ViewDialog JFrame
       ViewDialog empInfo = new ViewDialog(elj, attrs);
       empInfo.setVisible(true);
	}
}

/**
 * ViewDialog class displays a JFrame allowing users to enter values that correspond
 * to the attributes of the custom tag employeeTag double clicked.
 *
 * @author Jack Mason
 */
class ViewDialog extends JFrame
{
   /** Base class for EditLive! */
   private ELJBean eljbean;
   /** Map class will hold attributes of a custom tag */
   private Map attributes;

   private JLabel headingLabel = new JLabel("Enter Employee Details");

   private JPanel bodyPanel = new JPanel(new BorderLayout());

   private JPanel labelPanel;
   private JPanel componentPanel = new JPanel(new GridLayout(4, 1));

   private JPanel lowerPanel = new JPanel(new GridLayout(1, 2));
   private JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
   private JPanel blankPanel = new JPanel(new FlowLayout());

   private JLabel nameLabel = new JLabel("Name:");
   private JLabel positionLabel = new JLabel("Position:");
   private JLabel ageLabel = new JLabel("Age:");
   private JLabel genderLabel = new JLabel("Sex:");

   private JTextField nameField;
   private JTextField positionField;
   private JTextField ageField;

   private JPanel genderPanel = new JPanel(new GridLayout(1, 2));
   private JRadioButton maleRadio;
   private JRadioButton femaleRadio;
   private ButtonGroup genderGroup = new ButtonGroup();

   /** JButton used to send contents of Java swing components back into custom tag double clicked originally */
   private JButton okButton = new JButton("OK");
   /** JButton used to send contents of Java swing components back into custom tag double clicked originally */
   private JButton cancelButton = new JButton("Cancel");

   /**
    * Constructor. Constructs JFrame to display Java swing fields to allow a user to enter new values
    * for the attributes of the custom tag double clicked. The default contents of these Java swing fields
    * will match the current attributes of the double clicked custom tag by using the Map variable sent
    * through the constructor. Upon confirming new attributes specified, the contents of the Java swing
    * fields are verified.
    *
    * @param elj_ Instance of EditLive! containing the custom tag instance originally double clicked
    * @param map contains the attributes of the custom tag double clicked in the _elj parameter
    */
   ViewDialog(ELJBean _elj, Map map)
   {
     super("Employee Details");

     // specifying variables passed to constructor
     eljbean = _elj;
     attributes = map;

     // specifying component values
     headingLabel.setFont(new java.awt.Font("Default", 1, 12));

     okButton.addActionListener
          (new java.awt.event.ActionListener()
          {
            public void actionPerformed(ActionEvent e)
            {
			   int errorType = 0;

               String ageContents = ageField.getText();
               try
               {
                  Integer ageInteger = new Integer(ageContents);

                  // if no exception thrown, can continue to
                  // assign values to Map.

                  String nameContents = nameField.getText();
                  if(nameContents.equals(""))
                  {
					  errorType = 1;
					  throw new Exception();
				  }

                  String positionContents = positionField.getText();
                  if(positionContents.equals(""))
                  {
					  errorType = 2;
					  throw new Exception();
				  }

                  String genderContents = new String("");
                  if(maleRadio.isSelected() || femaleRadio.isSelected())
                  {
                     if(maleRadio.isSelected())
                     {
				        genderContents = new String("male");
					 }
					 else
					 {
						genderContents = new String("female");
					 }

				  }
				  else
				  {
				     errorType = 3;
					 throw new Exception();
				  }

				  attributes.put("name", nameContents);
				  attributes.put("age", ageContents);
				  attributes.put("position", positionContents);
			      attributes.put("gender", genderContents);

				  // Fire a set properties custom action.
				  eljbean.getEventBroadcaster().broadcastEvent(new TextEvent(this, TextEvent.CUSTOM_ACTION, attributes,
				     TextEvent.CustomAction.SET_PROPERTIES));
			   }
			   catch(Exception ex)
			   {
				  if(errorType == 0)
				  {
                     JOptionPane.showMessageDialog(null,
                        "Incorrect input for Age. Value must be an integer.",
                        "Error In Age Value",
                           JOptionPane.ERROR_MESSAGE);
			  	  }
			  	  if(errorType == 1)
			  	  {
                     JOptionPane.showMessageDialog(null,
                        "A name for the employee must be entered.",
                        "Error In Name Entry",
                           JOptionPane.ERROR_MESSAGE);
				  }
				  if(errorType == 2)
				  {
                     JOptionPane.showMessageDialog(null,
                        "A position for the employee must be entered.",
                        "Error In Position Entry",
                           JOptionPane.ERROR_MESSAGE);
				  }
				  if(errorType == 3)
				  {
                     JOptionPane.showMessageDialog(null,
                        "A gender for the employee must be selected.",
                        "Error In Gender Selection",
                           JOptionPane.ERROR_MESSAGE);
				  }

			   }
            }
          });

     cancelButton.addActionListener
          (new java.awt.event.ActionListener()
          {
            public void actionPerformed(ActionEvent e)
            {
			   // close window for employee details
               dispose();
            }
          });


     // assigning values to components based in map attributes
     String name = (String)attributes.get("name");
     String age = (String)attributes.get("age");
     String position = (String)attributes.get("position");
     String gender = (String)attributes.get("gender");

     nameField = new JTextField(name);
     positionField = new JTextField(position);
     ageField = new JTextField(age);

     String maleString = new String("male");
     String femaleString = new String("female");

     if(maleString.equals(gender))
     {
		 maleRadio = new JRadioButton("Male", true);
		 femaleRadio = new JRadioButton("Female");
     }
     else if(femaleString.equals(gender))
     {
		 maleRadio = new JRadioButton("Male");
		 femaleRadio = new JRadioButton("Female", true);
	 }
	 else
	 {
		 maleRadio = new JRadioButton("Male");
		 femaleRadio = new JRadioButton("Female");
	 }

     genderGroup.add(maleRadio);
     genderGroup.add(femaleRadio);

     // adding components to JPanels
     labelPanel = new JPanel(new GridLayout(4, 1));

     labelPanel.add(nameLabel);
     labelPanel.add(positionLabel);
     labelPanel.add(ageLabel);
     labelPanel.add(genderLabel);

     componentPanel.add(nameField);
     componentPanel.add(positionField);
     componentPanel.add(ageField);
     componentPanel.add(genderPanel);

     genderPanel.add(maleRadio);
     genderPanel.add(femaleRadio);

     bodyPanel.add(labelPanel, BorderLayout.WEST);
     bodyPanel.add(componentPanel, BorderLayout.CENTER);

     lowerPanel.add(blankPanel);
     lowerPanel.add(buttonPanel);
     buttonPanel.add(okButton);
     buttonPanel.add(cancelButton);

     // add components to view
     this.getContentPane().setLayout(new BorderLayout());

     this.getContentPane().add(headingLabel, BorderLayout.NORTH);
     this.getContentPane().add(bodyPanel, BorderLayout.CENTER);
     this.getContentPane().add(lowerPanel, BorderLayout.SOUTH);

     setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
     setSize(290, 175);
   }
}