/*
 * Copyright (c) 2013 Ephox Corporation.
 */

import com.ephox.editlive.*;
import com.ephox.editlive.common.*;
import javax.swing.JOptionPane;

/**
 * Class retrieves an instance of EditLive! through it's constructor. EventListener
 * is then registered with the EditLive! instance. Upon any event raised by
 * EditLive! the event is checked to see if it matches a custom menu created
 * in the sample_eljconfig.xml file. If event matched custom menu, a Java swing dialog
 * appears displaying the current EditLive! version.
 */
public class BasicELJ implements EventListener {

	ELJBean _bean;

    /** Constructor. Takes an instance of EditLive! and registeres an EventListener
     *
     * @param bean instance of EditLive! to be implemented in webpage
     */
	public BasicELJ(ELJBean bean) {
		_bean= bean;
		System.err.println("Created with bean: " + bean);
		bean.getEventBroadcaster().registerBeanEditorListener(this);
	}

    /** raiseEvent method, called upon any EditLive! event. */
	public void raiseEvent(TextEvent e) {
		if (e.getActionCommand() == TextEvent.CUSTOM_ACTION) {
			if (e.getExtraInt() == TextEvent.CustomAction.RAISE_EVENT) {
				if (e.getExtraString().equals("displayVersion")) {
					e.setHandled(true);

					String versionString = "EditLive! version: " + _bean.getVersion();

					JOptionPane.showMessageDialog(null, versionString);
				}
			}
		}
	}
}