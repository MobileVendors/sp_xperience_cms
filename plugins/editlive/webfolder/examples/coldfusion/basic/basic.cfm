<!---
******************************************************
 basic.cfm -- Basic Coldfusion example

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->

<!--- Load the configuration file on the server to speed up loading --->
<cfset configpath=ExpandPath("sample_eljconfig.xml")>
<cffile action="read" file="#configpath#" variable="xmlConfig">

<cfset contents="<p>This is the initial source</p>">

<HTML>

<HEAD>
<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8">
<TITLE>Sample EditLive! Coldfusion Integration</TITLE>

<!---
These script files initialize the EditLive! API so that the EditLive!
properties and methods may be set to customise EditLive!.
--->
<script src="../../../redistributables/editlivejava/editlivejava.js"></script>

</HEAD>

<BODY>

	<h1>Ephox EditLive! Basic Coldfusion Sample</h1>
	<p>This is a basic example of EditLive! in a web page </p>

<cfoutput>
<script type='text/javascript'>
var ELJApplet1_js;
ELJApplet1_js = new EditLiveJava("ELJApplet1", "700", "600");

ELJApplet1_js.setConfigurationText("#URLEncodedFormat(xmlConfig)#");
ELJApplet1_js.setBody("#URLEncodedFormat(contents)#");

ELJApplet1_js.setAutoSubmit(true);
ELJApplet1_js.setDebugLevel("info");
ELJApplet1_js.show();
</script>
</cfoutput>
	<P style="FONT-SIZE: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</P>
</FORM>

</BODY>

</HTML>
