<!---
******************************************************
 add.cfm -- create a new article

 End users can enter content using a web form and EditLive!
 The web form is submitted to the page xt_add.asp

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->

<!--- Load the configuration file on the server to speed up loading --->
<cfset configpath=ExpandPath("db_config.xml")>
<cffile action="read" file="#configpath#" variable="xmlConfig">

<cfset contents="<p>This is the initial source</p>">

<HTML>

<HEAD>
<TITLE>Create a New Article</TITLE>
<LINK rel="stylesheet" href="sample.css">

<!---
These script files initialize the EditLive! API so that the EditLive!
properties and methods may be set to customise EditLive!.
--->
<script src="../../../redistributables/editlivejava/editlivejava.js"></script>

</HEAD>

<BODY>

<H1>Create a New Article</H1>


<!---
This form contains EditLive!, a text area for the article title,
a submit button and a cancel button.
--->
<FORM action="xt_add.cfm" method="POST" name="articleForm">


  <!--- Article title --->
  <P>Title: <INPUT type="text" name="article_title" size="50"></P>
  <P>Body:<BR>

<cfoutput>
<script type='text/javascript'>
var ELJApplet1_js;
ELJApplet1_js = new EditLiveJava("ELJApplet1", "700", "600");

ELJApplet1_js.setConfigurationText("#URLEncodedFormat(xmlConfig)#");
ELJApplet1_js.setBody("#URLEncodedFormat(contents)#");

ELJApplet1_js.setAutoSubmit(true);
ELJApplet1_js.setDebugLevel("info");
ELJApplet1_js.show();
</script>
</cfoutput>
  </P>
  <P><INPUT type="submit" value="Save" name="Add Article"> <INPUT type="button" value="Cancel" name="Cancel" onclick="javascript:history.back();"></P>
</FORM>

</BODY>

</HTML>
