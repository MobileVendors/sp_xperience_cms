<!---
******************************************************

 edit.cfm -- update an existing article

 End users can enter content using a web form and EditLive!
 The web form is submitted to the page xt_edit.asp

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->

<!--- throw an error if article_id was not specified --->
<cfparam name="article_id">

<!--- Load the configuration file on the server to speed up loading --->
<cfset configpath=ExpandPath("db_config.xml")>
<cffile action="read" file="#configpath#" variable="xmlConfig">

<!---
Retrieve the specified article details from the database
Use <cfqueryparam> to escape the content for SQL.
--->
<CFQUERY DATASOURCE="ELContent" NAME="article_content">
SELECT *
FROM articles
WHERE article_id=<cfqueryparam value="#article_id#" cfsqltype="cf_sql_integer">
</CFQUERY>

<HEAD>
<CFOUTPUT>
<TITLE>Editing Article ID #article_content.article_id#</TITLE>
</CFOUTPUT>
<LINK rel="stylesheet" href="sample.css">

<!--
These script files initialize the EditLive! API so that the EditLive!
properties and methods may be set to customise EditLive!.
-->
<script src="../../../redistributables/editlivejava/editlivejava.js"></script>

</HEAD>

<BODY>

<H1>Edit Document</H1>


<!---
This form contains EditLive!, a text area for the article title,
a submit button and a cancel button.
--->
<FORM action="xt_edit.cfm" method="POST" name="articleForm">

<cfoutput>
  <!--- Hidden field for identifying the article --->
  <INPUT type="hidden" name="article_id" value="#article_content.article_id#">

  <!--- Article title --->
  <P>Title: <INPUT type="text" name="article_title" value="#HTMLEditFormat(article_content.article_title)#" size="40"></P>
  <P>Body:<BR>

<script type='text/javascript'>
var ELJApplet1_js;
ELJApplet1_js = new EditLiveJava("ELJApplet1", "700", "600");

ELJApplet1_js.setConfigurationText("#URLEncodedFormat(xmlConfig)#");
ELJApplet1_js.setBody("#URLEncodedFormat(toString(article_content.article_body))#");
ELJApplet1_js.setStyles("#URLEncodedFormat(toString(article_content.article_body))#");

ELJApplet1_js.setAutoSubmit(true);
ELJApplet1_js.setDebugLevel("info");
ELJApplet1_js.show();
</script>
</cfoutput>

  <P><INPUT type="submit" value="Save"> <INPUT type="button" value="Cancel" onclick="javascript:history.back();"></P>
</FORM>

</BODY>

</HTML>



