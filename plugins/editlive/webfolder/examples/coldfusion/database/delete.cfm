<!---
******************************************************

 delete.cfm -- confirm the user wants to delete an article

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->

<!--- throw an error if article_id was not specified --->
<cfparam name="article_id">

<!--- Retrieve the specified article details from the database --->
<CFQUERY DATASOURCE="ELContent" NAME="article_content">
SELECT *
FROM articles
WHERE article_id=<cfqueryparam value="#article_id#" cfsqltype="cf_sql_integer">
</CFQUERY>

<HTML>
<HEAD>

<CFOUTPUT>
<TITLE>Delete Article ID #article_content.article_id#?</TITLE>
</CFOUTPUT>
<LINK rel="stylesheet" href="sample.css">

</HEAD>

<BODY>

<H1>Delete a Document</H1>
<P>Are you sure you wish to delete this article?</P>

<!---
 This table contains the article ID and article title for deletion confirmation.
--->

<TABLE>
<CFOUTPUT>
  <!--- Article ID --->
  <TR>
    <TD>Article ID:</TD>
    <TD>#article_content.article_id#</TD>
  </TR>


  <!--- Article title --->
  <TR>
    <TD>Title:</TD>
    <TD>#article_content.article_title#</TD>
  </TR>
</TABLE>


<!--- Form for deleting the article --->
<FORM action="xt_delete.cfm" method="POST">
  <INPUT type="hidden" name="article_id" value="#article_content.article_id#">
  <P><INPUT type="submit" value="Delete"> <INPUT type="button" value="Cancel" onclick="javascript:history.back();"></P>
</FORM>
</CFOUTPUT>

</BODY>

</HTML>
