<!---
Update the article record values in the database with the values from the form objects.
Use <cfqueryparam> to escape the content for SQL.
 --->
<CFQUERY datasource="ELContent" name="add_article">
	INSERT INTO articles (article_title, article_body, article_styleElementText)
	VALUES(<cfqueryparam value="#FORM.article_title#" cfsqltype="cf_sql_varchar">,
		<cfqueryparam value="#FORM.ELJApplet1#" cfsqltype="cf_sql_varchar">,
		<cfqueryparam value="#FORM.ELJApplet1_styles#" cfsqltype="cf_sql_varchar">)
</CFQUERY>

<!--- Go back to the start page --->
<CFLOCATION URL="start.cfm">