<!---
******************************************************

 view.asp -- template for displaying an article

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->

<!--- throw an error if article_id was not specified --->
<cfparam name="article_id">

<!--- Retrieve the specified article details from the database --->
<CFQUERY DATASOURCE="ELContent" NAME="article_content">
SELECT *
FROM articles
WHERE article_id=<cfqueryparam value="#article_id#" cfsqltype="cf_sql_integer">
</CFQUERY>

<HTML>
<HEAD>

<!--- Display the article content --->
<CFOUTPUT>
<TITLE>#article_content.article_title#</TITLE>
<STYLE>
#toString(article_content.article_styleElementText)#
</STYLE>
</HEAD>
<BODY>


<H1>#article_content.article_title#</H1>

#toString(article_content.article_body)#
</CFOUTPUT>

</BODY>
</HTML>
