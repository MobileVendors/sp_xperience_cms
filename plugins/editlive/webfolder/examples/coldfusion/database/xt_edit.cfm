<!---
Update the article record values in the database with the values from the form objects.
Use <cfqueryparam> to escape the content for SQL.
 --->
<CFQUERY DATASOURCE="ELContent">
UPDATE articles
SET article_title=<cfqueryparam value="#FORM.article_title#" cfsqltype="cf_sql_varchar">,
	article_body=<cfqueryparam value="#FORM.ELJApplet1#" cfsqltype="cf_sql_varchar">,
	article_styleElementText=<cfqueryparam value="#FORM.ELJApplet1_styles#" cfsqltype="cf_sql_varchar">
WHERE article_id=<cfqueryparam value="#FORM.article_id#" cfsqltype="cf_sql_integer">
</CFQUERY>

<!--- Go back to the start page --->
<CFLOCATION URL="start.cfm">