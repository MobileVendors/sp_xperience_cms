<!---
******************************************************

 start.cfm -- the home page for the application

 Lists the articles stored in the database and provides
 links for creating a new article, editing existing articles
 and deleting existing articles

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
--->
<HTML>

<HEAD>
<TITLE>Sample Database Application for Coldfusion</TITLE>
<LINK rel="stylesheet" href="sample.css">
</HEAD>

<BODY>

<H1>Sample Database Application for Coldfusion</H1>

<P><A href="add.cfm">Create a new article</A></P>

<!---Create a query to retrieve a list of articles from the database--->
<CFQUERY DATASOURCE="ELContent" NAME="article_list">
SELECT * FROM articles ORDER BY article_id
</CFQUERY>

<!---Check if there are records in the database.--->
<CFIF article_list.RecordCount IS "0">

<!---There are no records.  Tell the user.--->

<p>There are no records in the database. Click <STRONG>Add an Article</STRONG>
    to add a record to the database.</p>

<CFELSE>
<!---
There are records in the database.  Loop
through all the records in the query
and write out a table row for each record.
Include links to view the article, edit the
article, or delete the article.
--->


<TABLE cellpadding=3 cellspacing=0 border=0>
   <TR>
      <TH>ID</TH>
      <TH width="250">Title</TH>
      <TH>Available Actions</TH>
   </TR>

<CFOUTPUT query="article_list">
  <TR>
    <TD>#article_id#</TD>
    <TD>#article_title#</TD>
    <TD><A href="view.cfm?article_id=#article_id#">View</A> |
    <A href="edit.cfm?article_id=#article_id#">Edit</A> |
    <A href="delete.cfm?article_id=#article_id#">Delete</A></TD>
  </TR>

</CFOUTPUT>
</TABLE>

</CFIF>



</BODY>
</HTML>