<?

/******************************************************

 delete.php -- confirm the user wants to delete an article

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/

// include the database wrapper functions
require_once("./i_database.php");

// get the article id
$articleID = $HTTP_GET_VARS["article_id"];

// Attempt to establish a connection with the database
if (DBConnect() == FALSE) {
	print DBError();
} else {
   //SQL query to get required document from the database
   DBQuery("SELECT * FROM articles WHERE article_id = " . escape_string($articleID) . ";");
   if (DBFetchRow()) {

//Display the article information in a form asking for delete confirmation
?>

<HTML>
<HEAD>
<TITLE>Delete Article ID <?=htmlspecialchars($articleID)?>?</TITLE>
<LINK rel="stylesheet" href="sample.css">

</HEAD>
<BODY>


<H1>Delete a Document</H1>

<P>Are you sure you wish to delete this article?</P>

<TABLE>
<TR>
<TD>Article ID:</TD>
<TD><?=htmlspecialchars($articleID)?></TD>
</TR>
<TR>
<TD>Title:</TD>
<TD><?=htmlspecialchars(DBResult("article_title"))?></TD>
</TR>
</TABLE>

<FORM action="xt_delete.php" method="GET">
<INPUT type="hidden" name="article_id" value="<?=htmlspecialchars($articleID)?>">
<P><INPUT type="submit" value="Delete"> <INPUT type="button" value="Cancel" onclick="javascript:history.back();"></P>
</FORM>
</BODY>
</HTML>
<?
   } else {
      //There are no records matching this article_id
      //TODO: Handle and display meaningful error
   }
}
DBDisconnect();
?>
