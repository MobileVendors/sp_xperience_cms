-- MySQL dump 10.9
--
-- Host: localhost    Database: elcontent
-- ------------------------------------------------------
-- Server version	4.1.11-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `elcontent`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `elcontent` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `elcontent`;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL auto_increment,
  `article_title` varchar(255) collate utf8_unicode_ci NOT NULL default '',
  `article_styleElementText` blob NOT NULL,
  `article_body` blob NOT NULL,
  PRIMARY KEY  (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--


/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
LOCK TABLES `articles` WRITE;
INSERT INTO `articles` VALUES (1,'MyCo Management Team','','<h2>Mike MyCo, MyCo CEO</h2>\r\n\r\n<p>Mike MyCo is the founder of MyCo. Mike brings to the company his leadership, his vision and his name. Mike has also been involved in MyInc, MySite and MyComm. He has much experience in the industry and strongly believes that MyCo products are the best in the world.</p>\r\n\r\n<p>Email: <a href=\"mailto:mikemyco@myco.com\">mikemyco@myco.com</a></p>\r\n\r\n<h2>Sue Saleswoman, MyCo Sales Manager</h2>\r\n\r\n<p>Sue Saleswoman has been with MyCo as its Sales Manager since the company\'s inception in January 2002. Sue has a strong dedication to the customers she serves and with her experience in the industry she can help a customer no matter what their needs may be. If you need a MyCo product then you need to talk to Sue.</p>\r\n\r\n<p>Email: <a href=\"mailto:suesaleswoman@myco.com\">suesaleswoman@myco.com</a></p>\r\n\r\n<h2>Sam Support, MyCo Support Manager</h2>\r\n\r\n<p>Sam Support has also been with MyCo since it was first founded and has been the Support Manager ever since. Sam\'s dedication to customer support is peerless. He constantly strives to help the customer in anyway he can. He has a strong belief in the personal service when it comes to the support of MyCo products. If you need a helping hand at MyCo talk to Sam.</p>\r\n\r\n<p>Email: <a href=\"mailto:samsupport@myco.com\">samsupport@myco.com</a></p>\r\n\r\n<h2>Fred Jones, MyCo News Site and Publicity Manager</h2>\r\n\r\n<p>Fred Jones is the final founding member of MyCo. Fred has been the News Site and Publicity Manager at MyCo since January 2002. He strives to keep the MyCo Site up to date with product news, press releases and seminar information.</p>\r\n\r\n<p>Email: <a href=\"mailto:fredjones@myco.com\">fredjones@myco.com</a></p>'),(2,'MyCo Sprocket 3.2 Integration Seminar','','<h1><font face=\"Courier\">Sprocket 3.2 Seminar&nbsp;</font></h1>\r\n\r\n<p>Due to the overwhelming response from developers to MyCo\'s new Sprocket 3.2 product MyCo is pleased to announce that it will be holding a seminar on how to integrate Sprocket 3.2 into your current architecture.</p>\r\n\r\n<p>Sam Support and Rebecca Repair will be present at the seminar to answer any questions about the integration of Sprocket 3.2 into any possible situation. For more information on the seminar please contact <a href=\"mailto:fredjones@myco.com\">Fred Jones</a>.</p>'),(3,'MyCo Spanner Works','','<h1>MyCo Spanner Works</h1>\r\n\r\n<p>MyCo is pleased to announce the addition of the MyCo Spanner Works into the MyCo suite of programs for the system administrator. MyCo Spanner Works has been designed to assist system administrators in recovering from system crashes. MyCo Spanner Works backs up critical data and settings and even works on Apple servers.</p>\r\n\r\n<p>MyCo Spanner Works. It\'s perfect for when someone throws a spanner into the works of your system.</p>\r\n\r\n<p>&nbsp;<img title=\"iMac\" alt=\"iMac Computer\" src=\"images/newimac.gif\" /></p>'),(4,'Sprocket 3.2','','<p><font color=\"#800080\" size=\"6\">MyCo Releases Sprocket 3.2</font></p>\r\n\r\n<p>MyCo has just released its new piece of software; Sprocket 3.2</p>\r\n\r\n<p>Taking advantage of the large number of programmers at their disposal and the Java language MyCo is happy to announce that it has just released the latest version of its great Sprocket technology. Sprocket 3.2 has vastly improved on everything that Sprocket 2.0 and even Sprocket 3.2 had to offer.</p>\r\n\r\n<p>The Sprocket technology is now more versatile than ever. Sprocket 3.2 contains new customization technologies to allow it to be adapted into almost any situation. Just see the Support article on Customising Sprocket 3.2.</p>\r\n\r\n<p>Sprocket 3.2 also has comprehensive integration guides, and if that is not enough to get the developer started then our full support team is available for you to contact.</p>\r\n\r\n<p>Sprocket 3.2 is the way of the future, fully customizable, easily integrated and with the full support of MyCo\'s support team behind it. You just cannot go wrong with MyCo and Sprocket 3.2.</p>'),(5,'INFO: Building Custom Add-ons for Sprocket 3.2','','<h2><font color=\"#ff0000\">Sprocket 3.2</font></h2>\r\n\r\n<p>Sprocket 3.2 is one of the most exciting technologies to emerge from MyCo to date. It may be customized and added to to make it fit into almost any situation. Just see the table below:</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" width=\"100%\" align=\"none\" cellspacing=\"1\">\r\n<tr>\r\n<td width=\"50%\"><font size=\"5\" face=\"Courier New\" color=\"#008080\"><b>Situation</b></font></td>\r\n<td width=\"50%\"><font size=\"5\" face=\"Courier New\" color=\"#008080\"><i>Customisation</i></font></td>\r\n</tr>\r\n\r\n<tr>\r\n<td width=\"50%\">Sprocket 3.2 on the desktop (standalone)</td>\r\n<td width=\"50%\">Simply use the installer interface to remove the multi user information from the Sprocket 3.2 Settings.&nbsp;</td>\r\n</tr>\r\n\r\n<tr>\r\n<td width=\"50%\">Sprocket 3.2 on the network</td>\r\n<td width=\"50%\">Use the installer interface to add all the network functionality back into Sprocket 3.2 if it has been removed previously.</td>\r\n</tr>\r\n\r\n<tr>\r\n<td width=\"50%\">Sprocket 3.2 as a paperweight</td>\r\n<td width=\"50%\">Place the heavy Sprocket 3.2 manual on your desk on top of a pile of papers to stop them blowing away.</td>\r\n</tr>\r\n</table>\r\n\r\n<p><br />\r\nFor more information on Sprocket 3.2 functionality please speak to Sam Support at MyCo. For information on purchasing Sprocket 3.2 please speak to Sue Saleswoman at MyCo.</p>');
UNLOCK TABLES;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

