<?

/******************************************************

 view.php -- template for displaying an article

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/

// include the database wrapper functions
require_once("./i_database.php");

// get the article id
$articleID = $HTTP_GET_VARS["article_id"];

// Attempt to establish a connection with the database
if (DBConnect() == FALSE) {
	print DBError();
} else {
   //SQL query to get required document from the database
   DBQuery("SELECT * FROM articles WHERE article_id = " . escape_string($articleID) . ";");
   if (DBFetchRow()) {
?>
<HTML>
<HEAD>
<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8">
<TITLE><?=DBResult("article_title")?></TITLE>
<STYLE>
<?=DBResult("article_styleElementText")?>
</STYLE>
</HEAD>
<BODY>

<H1><?=DBResult("article_title")?></H1>

<?=DBResult("article_body")?>

</BODY>
</HTML>
<?
   } else {
      //There are no records matching this article_id
      //TODO: Handle and display meaningful error
   }
}
DBDisconnect();
?>
