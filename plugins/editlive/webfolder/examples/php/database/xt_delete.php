<?

/******************************************************

 xt_delete.php -- delete the specified article from the database

 article_id passed in through QueryString

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement
******************************************************/

// include the database wrapper functions
require_once("./i_database.php");

// get the article id
$articleID = $HTTP_GET_VARS["article_id"];

// redirect variable, we don't want to redirect if we have errors
$redirect = TRUE;

//Attempt to establish a connection with the database
if (!DBConnect()) {
	print DBError();
	$redirect = FALSE;
} else {
   DBQuery("DELETE FROM articles WHERE article_id = " . escape_string($articleID) . ";");
   if (DBError() != "") {
   	print DBError();
   	$redirect = FALSE;
   }

   // Disconnect is required when updating
   DBDisconnect();
}

// redirect if no errors
if($redirect){
    Header("Location: start.php");
}
?>