<?

/******************************************************

 edit.php -- update an existing article

 End users can enter content using a web form and EditLive!
 The web form is submitted to the page xt_edit.php

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/

//load the XML file into the string "$xmlConfig"
//  this helps to speed up the ELJ load time
$filename = "db_config.xml";
$fd = fopen($filename,"r");
$xmlConfig = fread ($fd, filesize($filename));
fclose ($fd);


// include the database wrapper functions
require_once("./i_database.php");

// get the article id
$articleID = $HTTP_GET_VARS["article_id"];

// Attempt to establish a connection with the database
if (DBConnect() == FALSE) {
	print DBError();
} else {
?>

<HTML>
<HEAD>
<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8">
<TITLE>Editing Article ID <?=$articleID?></TITLE>
<LINK rel="stylesheet" href="sample.css">

</HEAD>
<BODY>

<?
   //SQL query to get required document from the database
   DBQuery("SELECT * FROM articles WHERE article_id = " . escape_string($articleID) . ";");
   if (DBFetchRow()) {
// This form contains EditLive!, a text area for the article title,
// a submit button and a cancel button.
?>

<H1>Edit Document</H1>


<FORM name="form1" method="post" action="xt_edit.php">

<?// Hidden field for identifying the article?>
<INPUT type="hidden" name="article_id" value="<?=DBResult("article_id")?>">

<?// Article title?>
<P>Title: <INPUT type="text" name="article_title" value="<?=htmlspecialchars(DBResult("article_title"))?>" size="40"></P>


<P>Body:
<script src="../../../redistributables/editlivejava/editlivejava.js"></script>
<script type='text/javascript'>
var ELJApplet1_js;
ELJApplet1_js = new EditLiveJava("ELJApplet1", "700", "600");

ELJApplet1_js.setConfigurationText("<?=rawurlencode($xmlConfig)?>");
ELJApplet1_js.setBody("<?=rawurlencode(DBResult("article_body"))?>");
ELJApplet1_js.setStyles("<?=rawurlencode(DBResult("article_styleElementText"))?>");

ELJApplet1_js.setAutoSubmit(true);
ELJApplet1_js.setDebugLevel("info");
ELJApplet1_js.show();
</script>

<P><INPUT type="submit" value="Save"> <INPUT type="button" value="Cancel" onclick="javascript:history.back();"></P>

</FORM>
<?
   } else {
   	  echo "No record found";
      //There are no records matching this article_id
      //TODO: Handle and display meaningful error
   }
}
DBDisconnect();
?>
</BODY>
</HTML>
