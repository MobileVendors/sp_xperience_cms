<?php
/****************************************************************************************
 *
 * i_database.php
 *
 * This file contains functions to connect to and retrieve content from
 *  the sample database.
 *
 ***************************************************************************************/
$DBLink = null;
$DBResult = null;
$i = 0;

// Connect to the defined database, see the documentation for how to set "ELContent" up
function DBConnect() {
	global $DBLink;

	$DBName = "ELContent";
	$hostname = "localhost";
	/*****************************************************************
	 * Change these lines to set your database username and password *
	 *****************************************************************/
	$username = "";
	$password = "";


	// Use persistent connect as this reduces latency and increases overall efficiency
	$DBLink = mysql_pconnect($hostname,$username,$password);
	if ($DBLink) {
		return(mysql_select_db($DBName));
	}
	return false;
}

// Disconnect from the database, only used when updating as we are connecting with pconnect
//  which leaves the connection open even after this is called
function DBDisconnect() {
	global $DBLink;

	mysql_close($DBLink);
}

// Execute an SQL query on the database
//  it is assumed this string is safe, using the escape_string before passing here
function DBQuery($SQLQuery) {
	global $DBLink;
	global $DBResult;
	global $i;

	$DBResult = mysql_query($SQLQuery);
	$i = 0;
	return($DBResult);
}

// Fetch the next row, returning true if it exists otherwise false
function DBFetchRow() {
	global $DBResult;
	global $i;


	if ($i < mysql_num_rows($DBResult)) {
		$success = mysql_data_seek($DBResult,$i);
		$i++;
	} else {
		$success = false;
	}

	return $success;
}

// Retrieve a column (specified by number or name) from the current row
function DBResult($column) {
	global $DBResult;
	global $i;

	return(mysql_result($DBResult, $i - 1, $column));
}

// Retrieve the last Database Error, returns an empty string otherwise
function DBError() {
	return(mysql_error());
}

// escape the string for MySQL usage
function escape_string($string) {
	global $DBLink;

	// make sure it is not escaped already
	$string = stripslashes($string);

	if(version_compare(phpversion(),"4.3.0")=="-1") {
		return(mysql_escape_string($string));
	} elseif($DBLink) {
		return(mysql_real_escape_string($string, $DBLink));
	}
}
?>
