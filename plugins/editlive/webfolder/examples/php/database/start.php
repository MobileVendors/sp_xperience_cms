<?php
/******************************************************

 start.php -- the home page for the application

 Lists the articles stored in the database and provides
 links for creating a new article, editing existing articles
 and deleting existing articles

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/

// include the database wrapper functions
require_once("./i_database.php");
?>
<HTML>

<HEAD>
<TITLE>Sample Database Application for PHP</TITLE>
<LINK rel="stylesheet" href="sample.css">
</HEAD>

<BODY>

<H1>Sample Database Application for PHP</H1>

<P><A href="add.php">Create a new article</A></P>

<?
//Attempt to establish a connection with the database
if (!DBConnect()) {
	print DBError();
} else {
   DBQuery("select * from articles");
   if (DBFetchRow()) {
?>

<TABLE cellpadding=3 cellspacing=0 border=0>
   <TR>
      <TH>ID</TH>
      <TH width="250">Title</TH>
      <TH>Available Actions</TH>
   </TR>
<?
   	// There are records. Loop through all the records in the
   	// recordset and write out a table row for each record
   	do {
?>
   <TR>
      <TD><?=DBResult("article_id")?></TD>
      <TD><?=DBResult("article_title")?></TD>
      <TD><A href="view.php?article_id=<?=DBResult("article_id")?>">View</A> |
         <A href="edit.php?article_id=<?=DBResult("article_id")?>">Edit</A> |
         <A href="delete.php?article_id=<?=DBResult("article_id")?>">Delete</A>
      </TD>
   </TR>
<?
   	} while(DBFetchRow())
?>
</TABLE>

<?
   } else {
?><P>There are no records in the database. Click <STRONG>Create a new article</STRONG>
to add a record to the database.</P>
<?
   }
}
DBDisconnect();
?>

</BODY>

</HTML>