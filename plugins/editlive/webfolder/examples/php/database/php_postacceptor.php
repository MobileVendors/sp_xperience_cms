<?php

	/*********************************************
	 * Change this line to set the upload folder *
	 *********************************************/
 	$imageFolder = "images/";

	reset ($_FILES);
	$temp = current($_FILES);

	if (is_uploaded_file($temp['tmp_name'])){
		$filetowrite = $imageFolder . $temp['name'];
		move_uploaded_file($temp['tmp_name'], $filetowrite);
	} else {
		// Notify EditLive! that the upload failed
		header("HTTP/1.0 500 Server Error");
	}
?>
