<?
/******************************************************

 xt_edit.php -- add the article to the database

 Receives content from edit.asp

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/
// include the database wrapper functions
require_once("./i_database.php");

// redirect variable, we don't want to redirect if we have errors
$redirect = TRUE;

//Attempt to establish a connection with the database
if (!DBConnect()) {
	print DBError();
	$redirect = FALSE;
} else {
   // Get the POSTed data from the form and escape it for SQL using the function in i_database
   $articleID = escape_string($HTTP_POST_VARS["article_id"]);
   $articleTitle = escape_string($HTTP_POST_VARS["article_title"]);
   $articleStyleElementText = escape_string($HTTP_POST_VARS["ELJApplet1_styles"]);
   $articleBody = escape_string($HTTP_POST_VARS["ELJApplet1"]);

   // Insert the POSTed data into the database
   $query = "UPDATE articles SET article_title='$articleTitle', article_styleElementText='$articleStyleElementText',"
          . " article_body='$articleBody' WHERE article_id=$articleID";
   DBQuery($query);
   if (DBError() != "") {
   	$redirect = FALSE;
   }

   // Disconnect is required when updating
   DBDisconnect();
}

// redirect if no errors
if($redirect){
    Header("Location: start.php");
}
?>