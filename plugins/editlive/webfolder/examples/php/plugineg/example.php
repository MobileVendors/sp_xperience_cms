<?php
/******************************************************

 example.php -- A standard javascript implementation of EditLive!
 				A call is made to the addPlugins PHP method to
 				add plugins to the EditLive! instance.

 This is an example of how a PHP script can be used to easily
 add plugins from a specified directory to EditLive!.

 Copyright (c) 2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************/

require_once("./pluginLoader.php");
?>
<html>
	<head>
		<title>Automated Plugin Loading Example - PHP</title>
		<link rel="stylesheet" href="stylesheet.css">
		<!--
		Include the EditLive! JavaScript Library
		-->
		<script src="../../../redistributables/editlivejava/editlivejava.js" language="JavaScript"></script>
	</head>
	<body>

		<h1>Automated Plugin Loading Example</h1>

		<p>This example depicts how a PHP script can be used to add all of the plugins located
		in a specific to an EditLive! instance.</p>
		<!--
		The instance of EditLive!
		-->
		<script type='text/javascript'>
			// Create a new EditLive! instance with the name "ELApplet", a height of 400 pixels and a width of 700 pixels.
			var editlive = new EditLiveJava("ELApplet", 700, 400);

			// This sets a relative or absolute path to the XML configuration file to use
			editlive.setConfigurationFile("../../redistributables/editlivejava/sample_eljconfig.xml");

			<?php
				$protocol = 'http';
				if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') {
					$protocol = 'https';
				}

				$hostname = $protocol.'://'.$_SERVER['HTTP_HOST'];
				$dirName = $_SERVER['PHP_SELF'];
				$dirName = substr($dirName , 0, strlen($dirName )-24)."examplePlugins/";
				$hostname = $hostname.$dirName;

				$pluginPath = substr(__FILE__, 0, strlen(__FILE__)-24)."examplePlugins";

				loadPlugins("editlive", $pluginPath, $hostname);
			?>

			// .show is the final call and instructs the JavaScript library (editlivejava.js) to insert a new EditLive! instance
			//  at the this location.
			editlive.show();
		</script>
	</body>
</html>
