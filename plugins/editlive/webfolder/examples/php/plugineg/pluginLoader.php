<?php
	function loadPlugins($jsVariableName, $pluginDir, $pluginUrl) {
	    if (is_dir($pluginDir) && $dh = opendir($pluginDir)) {
	        while (($file = readdir($dh)) !== false) {
	           $extension = substr($file , strlen($file) - 4, strlen($file));

	            if(strcmp(".xml", $extension) == 0) {;
	            	$filename = $pluginDir . '/' . $file;
	            	$content = file_get_contents($filename);
	            	if ($content) {
	            		$content = rawurlencode($content);
		                echo "$jsVariableName.addPluginAsText('$content', '$pluginUrl');\n";
		            }
	            }
	        }
	        closedir($dh);
	    }
	}
?>
