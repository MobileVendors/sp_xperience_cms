<?php

/******************************************************

basic.php -- Basic PHP example

Copyright (c) Ephox Corporation. All rights reserved.
See license.txt for license agreement

 ******************************************************/

//load the XML file into the string "$xmlConfig"
//  this helps to speed up the ELJ load time
$filename = "sample_eljconfig.xml";
$fd = fopen($filename, "r");
$xmlConfig = fread($fd, filesize($filename));
fclose($fd);


//the initial content to load into ELJ
$contents = "<p>This is the initial source</p>"

?>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>Sample EditLive! for Java PHP Integration</title>
	<link rel="stylesheet" href="sample.css" />

	<?php
		// This script file initializes the EditLive! API so that the EditLive!
		// properties and methods may be set to customise EditLive!.
	?>
	<script type="text/javascript" src="../../redistributables/editlivejava/editlivejava.js"></script>
</head>
<body>
	<h1>Ephox EditLive! for Java Basic PHP Sample</h1>
	<p>This is a basic example of EditLive! for Java in a web page </p>

	<script type='text/javascript'>
		var elj = new EditLiveJava("ELJApplet1", "700", "600");

		elj.setConfigurationText("<?php echo rawurlencode($xmlConfig)?>");
		elj.setBody("<?php echo rawurlencode($contents)?>");

		elj.setAutoSubmit(true);
		elj.setDebugLevel("info");
		elj.show();
	</script>
	<p style="font-size: 8pt">Copyright &copy; Ephox Corporation. All rights reserved.</p>

</body>
</html>
