<%@ language="VBScript" %>
<html>
<head>
<title>Content Display Page</title>
<style type="text/css">
	body {
		font-family: Verdana, Arial;
		font-size: 90%;
	}
	h1 {
	   font-family: Tahoma, Arial;
	   font-size: 24pt;
	   font-weight: normal;
	   color: #003366;
	   border-bottom: solid 1px #003366;
	}
	h2 {
		font-family: Tahoma, Arial;
		font-size: 19pt;
		font-weight: normal;
		color: #003366;
	}
	p.byline {
		font-style: italic;
		text-align: right;
	}
	p.quotation {
		border-left: solid 1px blue;
		margin-left: 20px;
		font-size: 80%;
	}
	p.note {
		border: dotted 1px green;
		margin-left: 20px;
		color: green;
		background-color: #CCFFCC;
		font-style: italic;
	}
	p.warning {
		border: solid 1px #D00000;
		background-color: #fff0f0;
		margin-left: 20px;
	}
	p.fineprint{
		font-size: 8pt;
		text-align: center;
	}
	p.code {
		font-family: Courier New;
		color: green;
		border: dotted 1px green;
		margin-left: 20px;
		padding: 10px;
	}
	span.code {
		font-family: Courier New;
	}
	span.comment {
		border: solid 1px #FFFF00;
		background-color: #FFFFCC;
	}
	span.reference {
		font-style: italic:
	}
</style>

</head>
<body>

<h1>Content from Multiple Instances of Ephox EditLive!</h1>

<p>Content from First Instance:</p>

<div style="border: 1px black solid; padding: 20px">
<%
Response.Write(Request.Form("ELJOne"))
%>
</div>

<p>Content from Second Instance:</p>

<div style="border: 1px black solid; padding: 20px">
<%
Response.Write(Request.Form("ELJTwo"))
%>
</div>

</body>
</html>