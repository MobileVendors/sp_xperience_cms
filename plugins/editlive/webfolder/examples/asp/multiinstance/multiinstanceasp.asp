<%@ language="VBScript" %>
<%
'******************************************************
'
' multipleinstance.asp -- an example of multiple instances of EditLive!
'						  for Java in the one page
'
' This sample shows a page with more than one instance of
' EditLive! for Java
'
' Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************
%>
<html>
	<head>
		<title>EditLive! for Java Multiple Instance Sample</title>
		<link rel="stylesheet" href="stylesheet.css">

		<!--***
		Include the EditLive! for Java VBScript Library
		***-->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->

		<%
		strContent1 = "<h2>Instance One</h2><p>This is the EditLive! Java Multiple Instance Sample</p>"
		strContent2 = "<h2>Instance Two</h2><p>This is the EditLive! Java Multiple Instance Sample</p>"
		%>
	</head>
	<body>

		<h1>Ephox EditLive! for Java Multiple Instance Sample</h1>

		<p>This is an example of multiple EditLive! for Java applets in a web page.</p>

		<form name="form1" method="POST" action="previewpage.asp">

			<%
			' Declare a global EditLive! for Java object
			Dim eljglobal
			Set eljglobal = New EditLiveForJavaGlobal

			' Initialise the EditLive! for Java global object
			eljglobal.Init()
			%>


			<p>EditLive! for Java Applet Number 1:</p>
			<%
			' Declare a new EditLive! for Java instance
			Dim editlivejava1
			Set editlivejava1 = New EditLiveForJava

			' Set the properties for the EditLive! for Java instance
			' Set the name property (this property should be unique)
			editlivejava1.Name = "ELJOne"
			editlivejava1.Width = 700
			editlivejava1.Height = 400

			' Specify the location of the XML configuration file for EditLive! for Java
			editlivejava1.ConfigurationFile = "sample_eljconfig.xml"

			' Specify the initial content for EditLive! for Java
			editlivejava1.Body = strContent1

			' Show the editor applet
			editlivejava1.Show()
			%>


			<p>EditLive! for Java Applet Number 2:</p>
			<%
			' Declare a new EditLive! for Java instance
			Dim editlivejava2
			Set editlivejava2 = New EditLiveForJava

			' Set the properties for the EditLive! for Java instance
			' Set the name property (this property should be unique)
			editlivejava2.Name = "ELJTwo"
			editlivejava2.Width = 700
			editlivejava2.Height = 400

			' Specify the location of the XML configuration file for EditLive! for Java
			editlivejava2.ConfigurationFile = "sample_eljconfig.xml"

			' Specify the initial content for EditLive! for Java
			editlivejava2.Body = strContent2

			' Show the editor applet
			editlivejava2.Show()
			%>

            <hr>

            <input type="submit" value="Display Content">

		</form>

		<p style="font-size: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</p>
	</body>
</html>
