<%@ language="VBScript" %>
<%
'******************************************************
'
' customtagsample.asp -- a basic example of EditLive! for Java integration
'
' This is an example of how to use custom tags in EditLive! for Java.
'
' Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************
%>
<html>
	<head>
		<title>EditLive! for Java Custom and Scripting Tags Sample</title>
		<link rel="stylesheet" href="stylesheet.css">
		<!-- ****
		  Include VBScript Library
		*** -->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->
	</head>
	<body>
		<h1>Ephox EditLive! for Java Custom and Scripting Tags Example</h1>
		<p>This is an example of using custom and scripting tags in EditLive! for Java</p>
		<form name="form1" method="POST">
		<%
			' Declare a global EditLive! for Java object
			Dim eljglobal
			Set eljglobal = New EditLiveForJavaGlobal

			' Initialise the EditLive! for Java global object
			eljglobal.Init()

			' Declare a new EditLive! for Java instance
			Dim editlivejava1
			Set editlivejava1 = New EditLiveForJava

			' Set the properties for the EditLive! for Java instance
			' Set the name property (this property should be unique)
			editlivejava1.Name = "ELJApplet1"
			editlivejava1.Width = 700
			editlivejava1.Height = 500

			' Specify the location of the XML configuration file for EditLive! for Java
			editlivejava1.ConfigurationFile = "customtag_eljconfig.xml"

			strContent = "<h1>Custom Tag Demo</h1><p>This document includes multiple custom XML tags and scripting elements.  The custom tag support displayed here demonstrates support for both registered and unknown custom tags.</p><h2>Registered custom tag with no attributes and a body</h2><h4>Example</h4><p class='indent'><customTag>This is inside a registered XML style tag</customTag></p><h4>Code</h4><p class='code'>&lt;customTag&gt;This is inside a registered XML style tag&lt;/customTag&gt;</p><p>&nbsp;</p><h2>Unknown custom tag with no body and one attribute</h2><h4>Example</h4><p class='indent'><tagWithAttrib attrib='test'/></p><h4>Code</h4><p class='code'>&lt;tagWithAttrib attrib='unknownTag'/&gt;</p><p>&nbsp;</p><h2>Registered custom tag with attributes and a body</h2><h4>Example</h4><p class='indent'><customTag attrib='test'>This is inside a registered XML style tag</customTag></p><h4>Code</h4><p class='code'>&lt;customTag attrib='test'&gt;This is inside a registered XML style tag&lt;/customTag&gt;</p><p>&nbsp;</p><h2>HTML comment tag</h2><h4>Example</h4><p class='indent'><!--This is a HTML comment tag--></p><h4>Code</h4><p class='code'>&lt;!--This is a HTML comment tag--&gt;</p><p>&nbsp;</p><h2>ASP or JSP style scripting</h2><h4>Example</h4><p class='indent'><"+"%example.scripting%"+"></p><h4>Code</h4><p class='code'>&lt;%example.scripting%&gt;</p><p>&nbsp;</p><h2>PHP style scripting</h2><h4>Example</h4><p class='indent'><?example scripting?></p><h4>Code</h4><p class='code'>&lt;?example scripting?&gt;</p><p>&nbsp;</p><h2>JSTE style scripting</h2><h4>Example</h4><p class='indent'><#example scripting#></p><h4>Code</h4><p class='code'>&lt;#example scripting#&gt;</p><p>&nbsp;</p>"

			' Specify content for EditLive! for Java that contains custom and scripting tags
			editlivejava1.Document = strContent

			strStyles = "customTag {display: inline; ephox-label: Custom Tag; ephox-start-icon:url(icons/opentag.gif); ephox-end-icon:url(icons/closetag.gif);}body {font-family: Verdana, Arial;font-size: 90%;}h1 {font-family: Tahoma, Arial;font-size: 24pt;font-weight: normal;color: #003366;border-bottom: solid 1px #003366;}h2 {font-family: Tahoma, Arial;font-size: 19pt;font-weight: normal;color: #003366;}  p.code {font-family: Courier New;color: green;margin-left: 20px;}p.indent {margin-left: 20px;}span.code {font-family: Courier New;}span.comment {border: solid 1px #FFFF00;background-color: #FFFFCC;}"

            ' Specify styles for EditLive! for Java registering custom scripting tags
            editlivejava1.Styles = strStyles

			' Show the editor applet
			editlivejava1.Show()
		%>
		</form>

		<hr>

		<p style="FONT-SIZE: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</p>
	</body>
</html>
