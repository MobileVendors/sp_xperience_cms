<%@ language="VBScript" %>
<%
'******************************************************
'
' equation.asp -- example allowing users to create mathematical
'                 equations using the Equation Editor
'
' This is a basic example of how to instantiate the Equation
' Editor in EditLive!
' Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************


%>
<html>
	<head>
		<title>EditLive! Sample Form</title>
		<link rel="stylesheet" href="stylesheet.css">

		<!--
		 Include the EditLive! ASP VBScript Library
		-->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->

	</head>
	<body>

		<h1>Ephox EditLive! Basic ASP Sample</h1>

		<p>This is a basic example of loading EditLive! in a web page. This example has been configured
		to output XHTML use a<br>range of cascading style sheet features.</p>

		<form name="MyForm">

			<%

			' Declare a global EditLive! object
			Dim eljglobal
			Set eljglobal = New EditLiveForJavaGlobal

			' Initialise the EditLive! global object
			eljglobal.Init()

			' Declare a new EditLive! instance
			Dim editlivejava1
			Set editlivejava1 = New EditLiveForJava

			' Set the properties for the EditLive! instance
			' Set the name property (this property should be unique)
			editlivejava1.Name = "ELJApplet1"
			editlivejava1.Width = 690
			editlivejava1.Height = 500

			' Specify the location of the XML configuration file for EditLive!
			editlivejava1.ConfigurationFile = "sample_eljconfig.xml"

			' Specify the initial content for EditLive!
			editlivejava1.Body = "<h1 class='sample'>Ephox EditLive! - Equation Editing</h1><p class='byline'>By Ephox Corporation</p><p>Add the WebEQ equation editor to <a href='http://www.ephox.com/products/editliveforjava/'>EditLive!</a> and&nbsp;enable content contributors to create and edit mathematical and scientific equations.&nbsp; The add-on allows authors to generate equations using the mathematical markup language, MathML. &nbsp;The quadratic equation below has been embedded using the EditLive! WebEQ Equation Editor add-on.</p><p align='center'><math xmlns='http://www.w3.org/1998/Math/MathML'> <mrow>       <mi>x</mi>       <mo>=</mo>       <mfrac>          <mrow>             <mo>&#x2212;</mo>             <mi>b</mi>             <mo>&#x00b1;</mo>             <msqrt>               <mrow>                  <msup>                    <mi>b</mi>                    <mn>2</mn>                  </msup>                  <mo>&#x2212;</mo>                  <mrow>                     <mn>4</mn>                     <mo>&#x2062;</mo>                     <mi>a</mi>                     <mo>&#x2062;</mo>                     <mi>c</mi>                  </mrow>               </mrow>             </msqrt>          </mrow>          <mrow>             <mn>2</mn>             <mo>&#x2062;</mo>             <mi>a</mi>          </mrow>       </mfrac>    </mrow> </math></p>"

			' Show the editor applet
			editlivejava1.Show()

			%>

		</form>

		<p style="font-size: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</p>
	</body>
</html>
