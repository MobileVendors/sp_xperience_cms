<%@ language="VBScript" %>
<%
'******************************************************
'
' basicaspsample.asp -- a basic example of EditLive! integration
'
' This is a basic example of how to integrate EditLive!
' into a Web page.
'
' Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************


%>
<html>
	<head>
		<title>EditLive! Sample Form</title>
		<link rel="stylesheet" href="stylesheet.css">

		<!--
		 Include the EditLive! ASP VBScript Library
		-->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->

	</head>
	<body>

		<h1>Ephox EditLive! Basic ASP Sample</h1>

		<p>This is a basic example of loading EditLive! in a web page. This example has been configured
		to output XHTML use a<br>range of cascading style sheet features.</p>

		<form name="MyForm">

			<%

			' Declare a global EditLive! object
			Dim eljglobal
			Set eljglobal = New EditLiveForJavaGlobal

			' Initialise the EditLive! global object
			eljglobal.Init()

			' Declare a new EditLive! instance
			Dim editlivejava1
			Set editlivejava1 = New EditLiveForJava

			' Set the properties for the EditLive! instance
			' Set the name property (this property should be unique)
			editlivejava1.Name = "ELJApplet1"
			editlivejava1.Width = 690
			editlivejava1.Height = 500

			' Specify the location of the XML configuration file for EditLive!
			editlivejava1.ConfigurationFile = "sample_eljconfig.xml"


			' Specify the initial content for EditLive!
			editlivejava1.Document = "<html><head><title>Ephox Solutions</title></head><body><h1><img alt='Powered by Ephox EditLive! Logo' align='right' title='Powered by Ephox EditLive! Logo' src='images/poweredByELlogo.jpg' />Ephox Solutions</h1><p>Unleash the power of your content system with the world's leading authoring solutions.</p><h2><a href='online_rich_text.html'>Online Rich Text Authoring</a></h2><p>Ephox's EditLive! solutions deliver the world's leading browser-based rich text authoring functionality for any online application.</p><h2>EditLive! Productivity Pack</h2><p>With EditLive! and the EditLive! Productivity Pack your users can create content more quickly, easily and collaboratively than ever before. &#160;The EditLive! Productivity Pack includes:</p><ul><li>Track changes</li><li>Thesaurus</li><li>Equation Editor</li></ul><table cellspacing='0' border='1' width='100%' cellpadding='0'><tr align='center'><th align='center' width='85%' colspan='2' bgcolor='#b0c9ff'><span style=' color: #000000; font-size: small;'>Features</span></th></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>Spell check</th><td width='75%' bgcolor='#e6f4ff'>EditLive! incorporates a real time spell checking engine</td></tr><tr><th align='left' width='25%' bgcolor='#bfe4ff'>Interface</th><td width='75%' bgcolor='#bfe4ff'>Rich menus and toolbars for usability</td></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>Source code</th><td width='75%' bgcolor='#e6f4ff'>Color coded source code editing</td></tr><tr><th align='left' width='25%' bgcolor='#bfe4ff'>Inline resizing</th><td width='75%' bgcolor='#bfe4ff'>Resize tables and images inline</td></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>Track changes</th><td width='75%' bgcolor='#e6f4ff'>Collaborate more effectively with track changes</td></tr><tr><th align='left' width='25%' bgcolor='#bfe4ff'>Word import</th><td width='75%' bgcolor='#bfe4ff'>Import filtered content from Microsoft Word</td></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>Full screen</th><td width='75%' bgcolor='#e6f4ff'>Break out of the web form with full screen authoring</td></tr><tr><th align='left' width='25%' bgcolor='#bfe4ff'>Rich content</th><td width='75%' bgcolor='#bfe4ff'>Work with tables, images, multimedia, lists and more.</td></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>XHTML</th><td width='75%' bgcolor='#e6f4ff'>Guaranteed 100% XHTML compliance.</td></tr><tr><th align='left' width='25%' bgcolor='#bfe4ff'>CSS</th><td width='75%' bgcolor='#bfe4ff'>Ensure compliance with your corporate style sheet</td></tr><tr><th align='left' width='25%' bgcolor='#e6f4ff'>Accessibility</th><td width='75%' bgcolor='#e6f4ff'>Check for accessibility compliance</td></tr></table></body></html>"
			' Show the editor applet
			editlivejava1.Show()

			%>

		</form>

		<p style="font-size: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</p>
	</body>
</html>
