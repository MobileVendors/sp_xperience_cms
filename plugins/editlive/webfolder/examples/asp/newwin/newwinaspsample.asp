<%@ language="VBScript" %>
<%
'******************************************************
'
' newwinaspsample.asp -- a sample of EditLive! that
'						submits to its parent frame
'
' A simple example of Ephox EditLive! that submits
' its content to its parent frame
'
' Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************
%>

<html>
	<head>
		<title>Ephox EditLive! New Window Demonstration</title>
		<link rel="stylesheet" href="stylesheet.css">

		<!-- ***
		Include the EditLive! VBScript Library
		*** -->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->

		<script type='text/javascript'>

		//Submits the content to the parent and closes the window
		function submitToParent() {
			var parentWindow = window.opener;
			parentWindow.document.parentForm.elj_text.value = document.MyForm.ELJApplet1.value;
			window.close();
			return false;
		}

		</script>
	</head>
	<body>

		<h1>Ephox EditLive! New Window Demonstration</h1>

		<form name="MyForm" onsubmit='return submitToParent();'>
			<%

				' Declare a global EditLive! object
				Dim eljglobal
				Set eljglobal = New EditLiveForJavaGlobal

				' Initialise the EditLive! global object
				eljglobal.Init()

				' Declare a new EditLive! instance
				Dim editlivejava1
				Set editlivejava1 = New EditLiveForJava

				' Set the properties for the EditLive! instance
				' Set the name property (this property should be unique)
				editlivejava1.Name = "ELJApplet1"
				editlivejava1.Width = 700
				editlivejava1.Height = 400

				' Specify the location of the XML configuration file for EditLive!
				editlivejava1.ConfigurationFile = "sample_eljconfig.xml"

				' Specify the initial content for EditLive!
				editlivejava1.Document = "<html><head></head><body><p>Document contents of Ephox EditLive!</p></body></html>"

				' Show the editor applet
				editlivejava1.Show()
			%>
            <hr>
			<p><input type="submit" value="Submit"></p>
		</form>
	</body>
</html>
