<%@ language="VBScript" %>
<%
'******************************************************
'
' formsaspsample.asp -- a basic example of EditLive! for Java integration
'
' This is a basic example of how to integrate EditLive! for Java
' into a Web page. The forms menu is included to allow HTML forms
' and form controls into the document.
'
' Copyright (c) 2013 Ephox Corporation. All rights reserved.
' See license.txt for license agreement
'
'******************************************************


%>
<html>
	<head>
		<title>EditLive! for Java Sample Form</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<link rel="stylesheet" href="stylesheet.css">

		<!--
		 Include the EditLive! for Java ASP VBScript Library
		-->
		<!-- #include file="../../../redistributables/editlivejava/editlivejava.asp" -->

	</head>
	<body>

		<h1>Ephox EditLive! for Java HTML Forms ASP Sample</h1>

		<p>This is a basic example of using the forms menu in EditLive! for Java to insert HTML forms
		and HTML form controls into a document.</p>

		<form name="MyForm">

			<%

			' Declare a global EditLive! for Java object
			Dim eljglobal
			Set eljglobal = New EditLiveForJavaGlobal

			' Initialise the EditLive! for Java global object
			eljglobal.Init()

			' Declare a new EditLive! for Java instance
			Dim editlivejava1
			Set editlivejava1 = New EditLiveForJava

			' Set the properties for the EditLive! for Java instance
			' Set the name property (this property should be unique)
			editlivejava1.Name = "ELJApplet1"
			editlivejava1.Width = 690
			editlivejava1.Height = 500

			' Specify the location of the XML configuration file for EditLive! for Java
			editlivejava1.ConfigurationFile = "forms_sample_eljconfig.xml"


			' Specify the initial content for EditLive! for Java
			editlivejava1.Document = "<html><head><title>Using HTML forms in EditLive! for Java 5.0</title><style type='text/css'>body {    font-family: Verdana, Arial;    font-size: 90%;}h1 {   font-family: Tahoma, Arial;   font-size: 24pt;   font-weight: normal;   color: #003366;   border-bottom: solid 1px #003366;}h2 { font-family: Tahoma, Arial; font-size: 19pt;    font-weight: normal;    color: #003366;}p.byline {  font-style: italic; text-align: right;}p.quotation {    border-left: solid 1px blue;    margin-left: 20px;  font-size: 80%;}p.note {    border: dotted 1px green;   margin-left: 20px;  color: green;   background-color: #CCFFCC;  font-style: italic;}p.warning { border: solid 1px #D00000;  background-color: #fff0f0;  margin-left: 20px;}p.fineprint{ font-size: 8pt; text-align: center;}p.code {    font-family: Courier New;   color: green;   border: dotted 1px green;   margin-left: 20px;  padding: 10px;}span.code {  font-family: Courier New;}span.comment {    border: solid 1px #FFFF00;  background-color: #FFFFCC;}span.reference { font-style: italic:}hr {    height:1px; color:grey}</style></head><body><h1>Customer Satisfaction Survey</h1><br/><form method='post'><p><strong>Personal Details</strong></p><p>First Name <input name='firstName' size='20' type='text' />&nbsp;Last Name <input name='lastName' size='20' type='text' /></p><p>Sex <input type='radio' name='gender' value='male' />Male <input type='radio' name='gender' value='female'/>Female</p><p><strong>Address</strong></p><p>Address <input name='address' size='20' type='text' /></p><p>Suburb/Town <input name='suburb' size='20' type='text' />&nbsp;State <input name='state' size='5' type='text' />&nbsp;Postcode <input name='postcode' size='5' type='text' /></p><p><strong>Feedback</strong></p><p>How would you rate our product (1 being poor, 7 being excellent)?</p><p><input type='radio' name='productrate' value='1'/>1&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='2'/>2&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='3'/>3&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='4'/>4&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='5'/>5&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='6'/>6&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='productrate' value='7'/>7</p><p>How would you rate our support(1 being poor, 7 being excellent) ?</p><p><input type='radio' name='supportrate' value='1'/>1&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='2'/>2&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='3'/>3&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='4'/>4&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='5'/>5&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='6'/>6&nbsp;&nbsp;&nbsp;&nbsp;<input type='radio' name='supportrate' value='7'/>7</p><p>Enter any additional comments you have</p><p><textarea name='comments' cols='60' rows='8'></textarea></p><p>Would you like a copy of our weekly online newsletter? <input type='checkbox' checked='true'> Yes</p><p><input type='submit' value='Submit'> <input type='submit' value='Cancel'> <input type='reset'></form></body></html>"

			' Show the editor applet
			editlivejava1.Show()

			%>

		</form>

		<p style="font-size: 8pt">Copyright &copy; 2013 Ephox Corporation. All rights reserved.</p>
	</body>
</html>
