﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MultipleEditorsExample.aspx.cs" Inherits="multiple_MultipleEditorsExample" ValidateRequest="false" %>

<%@ Register assembly="EditLiveJavaControl" namespace="EditLiveJavaControl" tagprefix="elj" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multiple Editors Example</title>
</head>
<body>
    <form id="form1" runat="server">

    <elj:EditLiveJava ID="EditLiveJava1" runat="server" Height="167px" DownloadDirectory="../../../redistributables/editlivejava" /><br />
    <elj:EditLiveJava ID="EditLiveJava2" runat="server" Height="167px" DownloadDirectory="../../../redistributables/editlivejava" /><br />
    
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" /><br />
    
    <asp:TextBox ID="TextBox1" runat="server" Height="123px" TextMode="MultiLine" Width="316px"></asp:TextBox><br />
    <asp:TextBox ID="TextBox2" runat="server" Height="123px" TextMode="MultiLine" Width="316px"></asp:TextBox><br />

    </form>
</body>
</html>
