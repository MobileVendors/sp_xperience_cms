﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BasicSample.aspx.cs" Inherits="basic_BasicSample" ValidateRequest="false" %>
<%@ Register assembly="EditLiveJavaControl" namespace="EditLiveJavaControl" tagprefix="elj" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Basic Example</title>
</head>
<body>
    <form id="form1" runat="server">
        <elj:EditLiveJava ID="EditLiveJava1" runat="server" 
            DownloadDirectory="../../../redistributables/editlivejava"
            Content="&lt;p&gt;Default editor content&lt;/p&gt;&lt;p&gt;More content&lt;/p&gt;"
            />
        <br />
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
        <br />
        <asp:TextBox ID="TextBox1" runat="server" Height="96px" TextMode="MultiLine" Width="252px" />
        <br />
    </form>
</body>
</html>
