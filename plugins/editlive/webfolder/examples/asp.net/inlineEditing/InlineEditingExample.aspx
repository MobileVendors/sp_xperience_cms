﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InlineEditingExample.aspx.cs" Inherits="inlineEditing_InlineEditingExample" ValidateRequest="false" %>
<%@ Register assembly="EditLiveJavaControl" namespace="EditLiveJavaControl" tagprefix="elj" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Inline Editing Example</title>
</head>
<body>
    <form id="form1" runat="server" method="post">
        <elj:EditLiveJava ID="EditLiveJava1" runat="server" InlineEditing="true" DownloadDirectory="../../../redistributables/editlivejava" />
        <elj:EditableSection ID="EditableSection1" Content="&lt;p&gt;content 1&lt;/p&gt;" runat="server" Height="200px" /><br />
        <elj:EditableSection ID="EditableSection2" Content="&lt;p&gt;content 2&lt;/p&gt;" runat="server" Height="200px" /><br />        
        
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" /><br />

        <asp:TextBox ID="TextBox1" runat="server" Height="91px" TextMode="MultiLine" Width="256px" /><br />
        <asp:TextBox ID="TextBox2" runat="server" Height="88px" TextMode="MultiLine" Width="255px" />

    </form>
</body>
</html>
