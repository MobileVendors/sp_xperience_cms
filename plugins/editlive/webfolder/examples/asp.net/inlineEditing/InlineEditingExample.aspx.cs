﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class inlineEditing_InlineEditingExample : System.Web.UI.Page {
    protected void Button1_Click(object sender, EventArgs e) {
        TextBox1.Text = EditableSection1.Content;
        TextBox2.Text = EditableSection2.Content;
    }
}
