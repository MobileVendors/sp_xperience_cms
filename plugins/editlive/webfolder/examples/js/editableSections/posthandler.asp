<%@ language="VBScript" %>
<!--
******************************************************

 postHandler.asp 

 This simple script is designed to display the contents of the editable
 sections passed from a HTTP form submission.

 Copyright (c) 2001-2013 Ephox Corporation. All rights reserved.
 See license.txt for license agreement

******************************************************
-->
<html>
	<head>
		<title>Inline Editing Sections Content</title>
		<link rel="stylesheet" href="stylesheet.css">
	</head>
	<body>
		<p>Content from Inline Editing Section 1:</p>

		<div style="overflow: auto; border: 1px dotted gray; white-space:normal;">
		<%
			Response.Write(Request.Form("content1"))
		%>
		</div>

		<p>Content from Inline Editing Section 2:</p>

		<div style="overflow: auto; border: 1px dotted gray; white-space:normal;">
		<%
			Response.Write(Request.Form("content2"))
		%>
		</div>
	</body>
</html>
