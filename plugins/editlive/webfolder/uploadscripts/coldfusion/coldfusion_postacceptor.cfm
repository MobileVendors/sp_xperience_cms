<CFSETTING EnableCFOutputOnly="Yes">
<!--- The above ensures only the <cfoutput> tag causes any output;
	ELJ includes extra whitespace as part of the file name.
--->

<!---*********************************************
	 * Change this line to set the upload folder *
	 *********************************************--->
<cfset uploadFolder="images">

<cfset imageFolder=expandPath("#uploadFolder#")>

<!---Save the uploaded file--->
<cffile action = "upload"
	fileField = "image"
	destination = "#imageFolder#"
	nameConflict = "MakeUnique">

<!---Notify ELJ of the filename--->
<cfoutput>#uploadFolder#/#serverFile#</cfoutput>