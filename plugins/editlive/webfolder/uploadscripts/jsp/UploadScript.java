/**
 * UploadScript.java
 *
 * @author Jonathan Hall
 * @version 1.0
 * @see org.apache.commons.fileupload
 */

import java.io.*;
import java.util.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.fileupload.*;

public class UploadScript extends HttpServlet {

	private static ResourceBundle bundle = ResourceBundle.getBundle("FILEUPLOAD");

	/** The path uploaded images will be place in */
    public static final String fileDir = bundle.getString("FILEDIR");
    /** The list of file extensions which will be accepted by this upload servlet */
    public static final String [] fileExtensions = bundle.getString("FILEEXT").split(",");

	static {
  		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException {

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		try {
			// Create a new file upload handler
			DiskFileUpload upload = new DiskFileUpload();
			String tmpPath = System.getProperty("java.io.tmpdir",".");

			// Set upload parameters
			upload.setSizeMax(5 * 1024 * 1024);  // 5Mb max upload size
			upload.setSizeThreshold(1024 * 512); // 500k max in memory cache size
			upload.setRepositoryPath(tmpPath);   // System Dependent temp folder
			                                     // for files larger then SizeThreshold

			// Parse the request
			List items = upload.parseRequest(req);

			// Process the uploaded fields
			Iterator iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();

				if (!item.isFormField()) {
					// Get complete file String from local disk
					String fileName = item.getName();


					// Writing tokens:
					StringTokenizer tokenizer;
					int amount;

					tokenizer = new StringTokenizer(fileName, "\\:/");
					amount = tokenizer.countTokens();

					// Loop through and discard tokens until 1 token remains
					for (int i = 0; i < amount -1; i++) {
						tokenizer.nextToken();
					}

					// The last token will be the file name
					String currentFile = tokenizer.nextToken();


					// Writing tokens:
					tokenizer = new StringTokenizer(currentFile, ".");
					amount = tokenizer.countTokens();

					// file must have an extention
					if (amount > 1) {
						// Loop through and discard tokens until 1 token remains
						for (int i = 0; i < amount - 1; i++) {
							tokenizer.nextToken();
						}

						// The last token will be the file extention
						String currentExtention = tokenizer.nextToken().toLowerCase();

						for(int fileExtIndex = 0; fileExtIndex < fileExtensions.length; fileExtIndex++){
							if(currentExtention.equalsIgnoreCase(fileExtensions[fileExtIndex])){
								//Write the file:
								File tempUploadFile = new File(fileDir + currentFile);
								item.write(tempUploadFile);
								//Uncomment and adjust this line if dynamic naming is required
								//Replace "virtualDirectory" with the relevant URL for your server

								//out.print("virtualDirectory"+tempUploadFile.getName());
							} else {
								// ignore the file.
							}
						}
					}

					//Delete the temp file if required
					item.delete();
				}
			}
		} catch(Exception e) {
			out.println(e.getMessage());
		}
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException {
		processRequest(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException {
		processRequest(req, res);
	}
}