<?php
if(isset($_GET['url'])) {
	
	$url = $_GET['url'];
	$default_size = 150;
	
	$width	= (isset($_GET['width'])) ? $_GET['width'] : $default_size;
	$height = (isset($_GET['height'])) ? $_GET['height'] : $default_size;
	
	$ext = strrchr($url, '.');
	$ext = strtolower($ext);

	if ($ext == '.jpeg' || $ext == '.jpg') {
		$imgfile = imagecreatefromjpeg($url);
	} elseif ($ext == '.gif') {
		$imgfile = imagecreatefromgif($url);
	} elseif ($ext == '.png'){
		$imgfile = imagecreatefrompng($url);
	} else {
		$error[] = "The linked file must be a .jpeg, .jpg, .gif, or .png";
	}
	//Resize the image
	$old_width	= imagesx($imgfile);
	$old_height	= imagesy($imgfile);
	
	//Keep the resolution of the image the same
	if($old_width > $old_height) {
		$ratio = $old_height / $height;
	} else {
		$ratio = $old_width / $width;
	}
	
	$new_width	= ceil($old_width / $ratio);
	$new_height	= ceil($old_height / $ratio);
	
	$new_image	= imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($new_image, $imgfile, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
	$imgfile	= $new_image;
	
	// Display the image
	header('Content-type: image/jpeg');
	imagejpeg($imgfile, NULL, 100);
	imagedestroy($imgfile);
}
?>