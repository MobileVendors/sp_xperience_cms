<?php
include('../inc/conn.php');
header("Content-Type: text/xml"); 
// Category listing for both schools and articles.
$id		= clean($_GET['category']);
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<magList category="'.$id.'">
';

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

$mag = sql_query("SELECT magazine, modified FROM fyp_magcatlink WHERE category = '".$id."'");
while($run = sql_fetch($mag)) {
	if(xml_is_show_data($timestamp, $run['modified']))
		$isUpdated = 1;
	
	$sql = sql_query("SELECT id, published, name, descr, picture, modified FROM fyp_mags WHERE id = '".$run['magazine']."' ORDER BY name");
	while ($magxml = sql_fetch($sql)) {
		if(xml_is_show_data($timestamp, $magxml['modified']))
			$isUpdated = 1;
		
		$xmlContent .= '
			<mag id="'.$magxml['id'].'">
				<title>'.$magxml['name'].'</title>
				<pubDate>'.$magxml['published'].'</pubDate>
				<image>
				<![CDATA[
					'.$url.''.$magxml['picture'].'
				]]>
				</image>
				<description>
				<![CDATA[
					'.$magxml['descr'].'
				]]>
				</description>
			</mag>
		';
	}
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</magList>
';
echo $xml;
?>