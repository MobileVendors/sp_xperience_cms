<?php
include('../inc/conn.php');
header("Content-Type: text/xml"); 
// Category listing for both schools and articles.
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<magCategory>
';
$sch = sql_query("SELECT id, name FROM fyp_magcats ORDER BY id");
while ($schxml = sql_fetch($sch)) {
	$xml .= '
			<category title="'.$schxml['name'].'" id="'.$schxml['id'].'" />
	';
}
$xml .= '
	</magCategory>
';
echo $xml;
?>