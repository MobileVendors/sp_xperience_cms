<?php
if(isset($_GET['url'])) {
	$url = $_GET['url'];
	$default_size = 150;
	
	$width	= (isset($_GET['width'])) ? $_GET['width'] : $default_size;
	$height = (isset($_GET['height'])) ? $_GET['height'] : $default_size;
	
	$ext = strrchr($url, '.');
	$ext = strtolower($ext);

	if ($ext == '.jpeg' || $ext == '.jpg') {
		$imgfile = imagecreatefromjpeg($url);
	} elseif ($ext == '.gif') {
		$imgfile = imagecreatefromgif($url);
	} elseif ($ext == '.png'){
		$imgfile = imagecreatefrompng($url);
	} else {
		$error[] = "The linked file must be a .jpeg, .jpg, .gif, or .png";
    }
    
    list($fname, $fext) = explode('.', end(explode('/', $url)));
    $imageName = "$fname-$width-by-$height.$fext";
    $cache = getcwd().'/cache/';
    $imageFile = $cache.$imageName;
    $tsstring='';$etag='';
    
    // if file already generated
    if(file_exists($imageFile)) {
        $timestamp = filemtime($imageFile);
        $filesize = filesize($imageFile);
        $etag = md5("$imageFile-$filesize-$timestamp");
        $tsstring = gmdate('D, d M Y H:i:s ', $timestamp) . 'GMT';
    } 
    
    $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false;
    $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : false;
    
    // check the sameness of the file requested
    // and tell to get the cache
    if ((($if_none_match && $if_none_match == $etag) || (!$if_none_match)) && 
            ($if_modified_since && $if_modified_since == $tsstring)) 
    { 
        header('HTTP/1.1 304 Not Modified');
        exit();
    }


    /**** no cache yet routine below ****/


	//Resize the image
	$old_width	= imagesx($imgfile);
	$old_height	= imagesy($imgfile);
	
	//Keep the resolution of the image the same
	if($old_width > $old_height) {
		$ratio = $old_height / $height;
	} else {
		$ratio = $old_width / $width;
	}
	
	$new_width	= ceil($old_width / $ratio);
	$new_height	= ceil($old_height / $ratio);
	
	$new_image	= imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($new_image, $imgfile, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
	$imgfile	= $new_image;
    
    if(!file_exists($imageFile)) 
    {
    // generate the image and cache
	    imagejpeg($imgfile, $imageFile, 100);
        imagedestroy($imgfile);
    }

    $timestamp = filemtime($imageFile);
    $filesize = filesize($imageFile);
    $etag = md5("$imageFile-$filesize-$timestamp");
    
    $tsstring = gmdate('D, d M Y H:i:s ', $timestamp) . 'GMT';
    header("Last-Modified: $tsstring");
    header("ETag: $etag ");
    header('Content-Length: ' . $filesize);
	header('Content-type: image/jpeg');
    
    readfile($imageFile);
}
?>
