<?php
include('../inc/conn.php');
header("Content-Type: text/xml");
$id		= clean($_GET['id']);
$xml 	= '<?xml version="1.0" encoding="UTF-8"?>
<magazine id="'.$id.'">
  ';

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

$mag	= sql_query("SELECT pageid, modified FROM fyp_magpages WHERE magid = '".$id."'");
$magr	= sql_fetch($mag);
$page 	= sql_query("SELECT id, name, template, modified FROM fyp_pages WHERE parent = '".$id."' ORDER BY pagenum");
while($pagexm = sql_fetch($page)) {
	if(xml_is_show_data($timestamp, $pagexm['modified']))
		$isUpdated = 1;
	
	$t = sql_query("SELECT name, modified FROM fyp_templates WHERE id = '".$pagexm['template']."'");
	$txml = sql_fetch($t);
	
	if(xml_is_show_data($timestamp, $txml['modified']))
		$isUpdated = 1;
	
	$xmlContent	.= '<page name="'.$pagexm['name'].'" template="'.$txml['name'].'" id="'.$pagexm['id'].'">
    ';

$art 	= sql_query("SELECT articleid, articleslot, modified FROM fyp_articlepages WHERE pageid = '".$pagexm['id']."' ORDER BY id");
while($artxm	= sql_fetch($art)) {
	if(xml_is_show_data($timestamp, $artxm['modified']))
		$isUpdated = 1;
	
$type 	= sql_query("SELECT name, subname, sharelink, articletype, modified FROM fyp_articles WHERE id = '".$artxm['articleid']."'");
$typexm = sql_fetch($type);

if(xml_is_show_data($timestamp, $typexm['modified']))
	$isUpdated = 1;

$catxm = sql_query("SELECT diploma, category, modified FROM fyp_articlecats WHERE article = '".$artxm['articleid']."'");
	$xmlContent 	.= '<article slot="'.$artxm['articleslot'].'" id="'.$artxm['articleid'].'" type="'.$typexm['articletype'].'">
      ';
	

	$dxml = '';
	$cxml = '';
	while ($cat = sql_fetch($catxm)) {
		if(xml_is_show_data($timestamp, $cat['modified']))
			$isUpdated = 1;
		
		if ($cat['diploma'] == 0) {
			$dxml .= '<cat id="'.$cat['category'].'" />';
		} else {
			$cxml .= '<cat id="'.$cat['category'].'" />';
		}
	}

	$xmlContent	.= '
	<school>
		'.$dxml.'
	</school>
        ';
	$xmlContent	.= '
	<category>
		'.$cxml.'
	</category>
        ';

	if (!empty($typexm['sharelink'])) {$share = $typexm['sharelink'];} else {$share = $url.'article.php?id='.$artxm['articleid'].'';}
	$xmlContent	.= '
      <url>'.$share.'</url>';
	
	$xmlContent	.= '
      <title><![CDATA['.xml_encode_data($typexm['name']).']]></title>';
	  
	$xmlContent	.= '
      <subtitle><![CDATA['.xml_encode_data($typexm['subname']).']]></subtitle>';

	$media	= sql_query("SELECT contenttitle, contenttype, contentvalue, defaultimg, modified FROM fyp_content WHERE contentparent = '".$artxm['articleid']."' AND contenttype != 3 ORDER BY contentorder ASC");
	$xmlContent	.= '
      <media>';
	while ($mrow = sql_fetch($media)) {
		
		if(xml_is_show_data($timestamp, $mrow['modified']))
			$isUpdated = 1;
		
		if ($mrow['contenttype'] == 1) {
			$link = $url.''.str_replace('&', '&amp;', $mrow['contentvalue']);
			$link = str_replace('adm/', '', $link);
		} else {
			$link = $mrow['contentvalue'];
		}
		
		if($mrow['contenttype'] == 2)
			$mrow['contenttitle'] = urldecode($mrow['contenttitle']);
		
		$xmlContent .= '
        <item caption="'.str_replace('"', '&quot;', $mrow['contenttitle']).'" url="'.$link.'" type="'.$mrow['contenttype'].'" primary="'.$mrow['defaultimg'].'" />';
	}
	$xmlContent	.= '
      </media>';

	$content	= sql_query("SELECT textexcerpt, contentvalue, modified FROM fyp_content WHERE contentparent = '".$artxm['articleid']."' AND contenttype = 3");
	$crow		= sql_fetch($content);
	
	if(xml_is_show_data($timestamp, $crow['modified']))
		$isUpdated = 1;
	
	$xmlContent	.= '
      <shortContent>
        <![CDATA['.xml_encode_data($crow['textexcerpt']).']]>
      </shortContent>
      <content>
        <![CDATA['.xml_encode_data($crow['contentvalue']).']]>
      </content>
    </article>';
}
$xmlContent	.= '
  </page>
  ';
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
</magazine>';

echo $xml;
?>