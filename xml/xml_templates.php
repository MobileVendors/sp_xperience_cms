<?php
include('../inc/conn.php');
header("Content-Type: text/xml"); 
// Category listing for both schools and articles.
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<Templates>
';

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

$t = sql_query("SELECT id, name, slots, modified  FROM fyp_templates ORDER BY id");
while ($txml = sql_fetch($t)) {
	if(xml_is_show_data($timestamp, $txml['modified']))
		$isUpdated = 1;
	
	$xmlContent .= '
			<template id="'.$txml['id'].'" name="'.$txml['name'].'" slots="'.$txml['slots'].'" />
	';
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</Templates>
';
echo $xml;
?>