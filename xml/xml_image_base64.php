<?php
include('../inc/conn.php');
$url = isset($_GET['url']) && !empty($_GET['url']) ? $_GET['url'] : false;
'plain'==getFormat() ? header("Content-Type: text/plain") : header("Content-Type: text/xml"); 

if(!$url) 
{
    errorXml("'url' parameter is required");
}

$file = ROOT.str_replace(HOST_URL, '', $url);
if(!file_exists($file))
{
    errorXml('Image file does not exists');
}

$image = file_get_contents($file);
successXml(base64_encode($image));

function getFormat()
{
    return isset($_GET['format']) && !empty($_GET['format']) ? $_GET['format'] : 'xml';
}

function successXml($base64)
{
    returnIfPlainFormat($base64);

    $writer = writerOpen();
    $writer->startElement('Image');
        $writer->startElement('base64');
            $writer->writeCdata($base64);
        $writer->endElement();
    $writer->endElement();
    writerClose($writer);
}

function writerOpen()
{
    $writer = new XMLWriter();
    $writer->openURI('php://output');  
    $writer->startDocument('1.0','UTF-8');  
    $writer->setIndent(4);
    return $writer;
}

function writerClose($writer)
{
    $writer->endDocument();
    $writer->flush();
    exit;
}

function returnIfPlainFormat($response)
{
    if('plain'==getFormat())
    {
        echo $response;
        exit;
    }
}

function errorXml($message)
{
    returnIfPlainFormat($message);

    $writer = writerOpen();
    $writer->startElement('Error');
        $writer->startElement('message');
            $writer->writeCdata($message);
        $writer->endElement();
    $writer->endElement();
    writerClose($writer);
    
}

?>
