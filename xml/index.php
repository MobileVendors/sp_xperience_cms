<!DOCTYPE html>

<html>
<head>
    <title>XML List.</title>
</head>

<body>
<h1>Just a simple list of the xml files and their description</h1>
<p>Most of the fields are pretty much self explanatory.</p>
<ul>
    <li><a href="#xml_cats">xml_cats.php</a></li>
    <li><a href="#xml_mags">xml_mags.php</a></li>
    <li><a href="#xml_singlecat">xml_singlecat.php</a></li>
    <li><a href="#xml_singlemag">xml_singlemag.php</a></li>
</ul>
<h2 id="xml_cats">xml_cats.php</h2>
<p>This xml feed displays all the schools/diplomas and article categories.</p>
<h3>Sample XML feed. | <a href="xml_cats.php">Live XML Feed.</a></h3>
<p>The "id" field for both schools/diplomas and categories can be the same.</p>
<pre>
<?php echo htmlentities('<Category>
    <School>
        <category title="School or Diploma" email="email@sp.edu.sg" id="1" />
    </School>
    <Article>
        <category title="Article Category" id="1" />
    </Article>
</Category>'); ?>
</pre>
<p>id's would be linked to articles in the xml file: <a href="#xml_singlemag">xml_singlemag.php</a>. For example;</p>
<pre>
<?php echo htmlentities('<category>
    <cat id="1" /> [This is linked to category with the id 1]
</category>
<school>
    <cat id="1" /> [This is linked to school/diploma with the id 1]
</school>'); ?>
</pre>
<p>Would match up with the school/diploma category named "School or Diploma" and the article category named "Article Category".</p>
<h2 id="xml_mags">xml_mags.php</h2>
<p>This xml feed displays all the magazine categories.</p>
<h3>Sample XML feed. | <a href="xml_mags.php">Live XML Feed.</a></h3>
<p>The id in this case is just the identifier for <a href="#xml_singlecat">xml_singlecat.php</a>. In this case, xml_singlecat.php?category=1 Would list all the magazines with the category id of 1</p>
<pre>
<?php echo htmlentities('<magCategory>
    <category title="Magazine Category" id="1"/>
</magCategory>'); ?>
</pre>
<h2 id="xml_singlecat">xml_singlecat.php</h2>
<p>This xml feed displays all the magazines in a particular magazine category.</p>
<h3>Sample XML feed. | <a href="xml_singlecat.php?category=3">Live XML Feed.</a></h3>
<p>&lt;magList&gt; category is a unique identifier, called by <a href="#xml_mags">xml_mags.php</a>.</p>
<p>&lt;mag&gt; id is the unique identifier for the particular magazine.</p>
<pre>
<?php echo htmlentities('<magList category="3">
    <mag id="19">
        <title>Title of the Magazine</title>
        <pubDate>2011-08-24</pubDate>
        <image>http://direct-link-to-image.com/image.jpg</image>
        <description>Description of the magazine</description>
    </mag>
</magList>'); ?>
</pre>
<h2 id="xml_singlemag">xml_singlemag.php</h2>
<p>This xml feed displays all the pages/articles in a single magazine.</p>
<h3>Sample XML feed. | <a href="xml_singlemag.php?id=81">Live XML Feed.</a></h3>
<p>Each magazine can have as many pages as required, and each page can have up to 9 articles (this may change in future)</p>
<p>&lt;page&gt;, &lt;article&gt;, &lt;url&gt; and &lt;media&gt; is used for the internal app.</p>
<p>&lt;category&gt; and &lt;school&gt; is explained at <a href="#xml_cats">xml_cats.php</a>.</p>
<p></p>
<pre>
<?php echo htmlentities('
<magazine id="1">
    <page name="This is page 1" template="0" id="1">
        <article slot="1" id="1" type="1">
            <category>
            </category>
            <school>
                <cat id="1" />
            </school>
            <url>http://direct-link-to-article.com/title-for-this-article</url>
            <title><![CDATA[Title for this article.]]></title>
            <subtitle><![CDATA[Subtitle for this article.]]></subtitle>
            <media>
                <item caption="Image number 1" url="http://direct-link-to-image.com/image.jpg" type="1" primary="1" />
                <item caption="Image number 2" url="http://direct-link-to-image.com/image.jpg" type="1" primary="0" />
            </media>
            <shortContent>
            <![CDATA[Short content, usually a summary of the whole article. Partial html formatting allowed.]]>
            </shortContent>
            <content>
            <![CDATA[Full content, full html formatting allowed.]]>
            </content>
        </article>
    </page>
    <page name="This is page 2" template="0" id="1">
        <article slot="1" id="1" type="1">
            <category>
                 <cat id="1" />
           </category>
            <school>
            </school>
            <url>http://direct-link-to-article.com/title-for-this-article</url>
            <title><![CDATA[Title for this article.]]></title>
            <subtitle><![CDATA[Subtitle for this article.]]></subtitle>
            <media>
                <item caption="Image number 1" url="http://direct-link-to-image.com/image.jpg" type="1" primary="1" />
                <item caption="Image number 2" url="http://direct-link-to-image.com/image.jpg" type="1" primary="0" />
            </media>
            <shortContent>
            <![CDATA[Short content, usually a summary of the whole article. Partial html formatting allowed.]]>
            </shortContent>
            <content>
            <![CDATA[Full content, full html formatting allowed.]]>
            </content>
        </article>
        <article slot="2" id="1" type="1">
            <category>
            </category>
            <school>
                <cat id="1" />
            </school>
            <url>http://direct-link-to-article.com/title-for-this-article</url>
            <title><![CDATA[Title for this article.]]></title>
            <subtitle><![CDATA[Subtitle for this article.]]></subtitle>
            <media>
                <item caption="Image number 1" url="http://direct-link-to-image.com/image.jpg" type="1" primary="1" />
                <item caption="Image number 2" url="http://direct-link-to-image.com/image.jpg" type="1" primary="0" />
            </media>
            <shortContent>
            <![CDATA[Short content, usually a summary of the whole article. Partial html formatting allowed.]]>
            </shortContent>
            <content>
            <![CDATA[Full content, full html formatting allowed.]]>
            </content>
        </article>
    </page>
</magazine>
'); ?>
</pre>
</body>
</html>
