<?php
include('../inc/conn.php');
header("Content-Type: text/xml"); 
// Category listing for both schools and articles.
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<Category>';

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '
		<School>
';

$sch = sql_query("SELECT id, name, email, modified FROM fyp_schdips ORDER BY name");
while ($schxml = sql_fetch($sch)) {
	if(xml_is_show_data($timestamp, $schxml['modified']))
		$isUpdated = 1;
	
	$xmlContent .= '
			<category title="'.$schxml['name'].'" email="'.$schxml['email'].'" id="'.$schxml['id'].'" />
	';
}
$xmlContent .= '
		</School>
		<Article>
';
$art = sql_query("SELECT id, name, modified FROM fyp_cats ORDER BY id");
while ($artxml = sql_fetch($art)) {
	if(xml_is_show_data($timestamp, $artxml['modified']))
		$isUpdated = 1;
	
	$xmlContent .= '
			<category title="'.$artxml['name'].'" id="'.$artxml['id'].'" />
	';
}

$xmlContent .= '
		</Article>';

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</Category>
';
echo $xml;
?>