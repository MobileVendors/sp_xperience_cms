<?php
include('../inc/conn.php');
header("Content-Type: text/xml");
$id		= clean($_GET['id']);
	$xml 	= '<?xml version="1.0" encoding="UTF-8"?>
<magazine id="'.$id.'">
  ';

$mag	= sql_query("SELECT pageid FROM fyp_magpages WHERE magid = '".$id."'");
$magr	= sql_fetch($mag);
$page 	= sql_query("SELECT id, name, template FROM fyp_pages WHERE parent = '".$id."' ORDER BY pagenum");
while($pagexm = sql_fetch($page)) {
	$t = sql_query("SELECT name FROM fyp_templates WHERE id = '".$pagexm['template']."'");
	$txml = sql_fetch($t);
	$xml	.= '<page name="'.$pagexm['name'].'" template="'.$txml['name'].'" id="'.$pagexm['id'].'">
    ';

$art 	= sql_query("SELECT articleid, articleslot FROM fyp_articlepages WHERE pageid = '".$pagexm['id']."' ORDER BY id");
while($artxm	= sql_fetch($art)) {
$type 	= sql_query("SELECT name, subname, sharelink, articletype FROM fyp_articles WHERE id = '".$artxm['articleid']."'");
$typexm = sql_fetch($type);
$catxm = sql_query("SELECT diploma, category FROM fyp_articlecats WHERE article = '".$artxm['articleid']."'");
	$xml 	.= '<article slot="'.$artxm['articleslot'].'" id="'.$artxm['articleid'].'" type="'.$typexm['articletype'].'">
      ';
	
	while ($cat = sql_fetch($catxm)) {
		$cxml = '';
		$dxml = '';
		if ($cat['diploma'] == 0) {
			$cxml .= '<cat id="'.$cat['category'].'" />';
		} else {
			$dxml .= '<cat id="'.$cat['category'].'" />';
		}
	}
	$xml	.= '<category>
		'.$cxml.'
		</category>
        ';
	$xml	.= '<school>
		'.$dxml.'
		</school>
        ';

	if (!empty($typexm['sharelink'])) {$share = $typexm['sharelink'];} else {$share = $url.'article.php?id='.$artxm['articleid'].'';}
	$xml	.= '
      <url>'.$share.'</url>';
	
	$xml	.= '
      <title><![CDATA['.html_entity_decode($typexm['name']).']]></title>';
	  
	$xml	.= '
      <subtitle><![CDATA['.html_entity_decode($typexm['subname']).']]></subtitle>';

	$media	= sql_query("SELECT contenttitle, contenttype, contentvalue, defaultimg FROM fyp_content WHERE contentparent = '".$artxm['articleid']."' AND contenttype != 3");
	$xml	.= '
      <media>';
	while ($mrow = sql_fetch($media)) {
		if ($mrow['contenttype'] == 1) {
			$link = $url.''.str_replace('&', '&amp;', $mrow['contentvalue']);
		} else {
			$link = $mrow['contentvalue'];
		}
		$xml .= '
        <item caption="'.str_replace('"', '&quot;', $mrow['contenttitle']).'" url="'.$link.'" type="'.$mrow['contenttype'].'" primary="'.$mrow['defaultimg'].'" />';
	}
	$xml	.= '
      </media>';

	$content	= sql_query("SELECT textexcerpt, contentvalue FROM fyp_content WHERE contentparent = '".$artxm['articleid']."' AND contenttype = 3");
	$crow		= sql_fetch($content);
	$xml	.= '
      <shortContent>
        <![CDATA['.html_entity_decode($crow['textexcerpt']).']]>
      </shortContent>
      <content>
        <![CDATA['.html_entity_decode($crow['contentvalue']).']]>
      </content>
    </article>';
}
$xml	.= '
  </page>
  ';
}
$xml .= '
</magazine>';

echo $xml;
?>