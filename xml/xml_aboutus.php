<?php
include('../inc/conn.php');
header("Content-Type: text/xml");
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<aboutus>
';
$sql = sql_query("SELECT * FROM fyp_aboutus WHERE id = 1");

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

while ($aboutxml = sql_fetch($sql)) {
	
	if(xml_is_show_data($timestamp, $aboutxml['modified']))
		$isUpdated = 1;
	
	$xmlContent .= '
		<media>
		<![CDATA[
			'.$url.''.$aboutxml['media'].'
		]]>
		</media>
		<info>
		<![CDATA[
			'.stripcslashes($aboutxml['info']).'
		]]>
		</info>
	';
	
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</aboutus>
';
echo $xml;
?>