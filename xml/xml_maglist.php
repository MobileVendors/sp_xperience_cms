<?php
include('../inc/conn.php');
header("Content-Type: text/xml"); 
// Category listing for both schools and articles.
$id  = clean($_GET['category']);
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<magList category="'.$id.'">
';

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

$sch = sql_query("SELECT id, name FROM fyp_magcats ORDER BY id");
while ($schxml = sql_fetch($sch)) {
	if(xml_is_show_data($timestamp, $schxml['modified']))
		$isUpdated = 1;
	
	$xmlContent .= '
			<category title="'.$schxml['name'].'" id="'.$schxml['id'].'" />
	';
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</magList>
';
echo $xml;
?>