<?php
include('../inc/conn.php');
header("Content-Type: text/xml");
$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<bannerList>
';
$sql = sql_query("SELECT * FROM fyp_banners WHERE image IS NOT NULL OR url IS NOT NULL");

$timestamp = xml_get_timestamp_param();
$isUpdated = 0;
$xmlContent = '';

while ($banxml = sql_fetch($sql)) {
	if(xml_is_show_data($timestamp, $banxml['modified']))
		$isUpdated = 1;
		
	$xmlContent .= '
		<banner id="'.$banxml['id'].'">
			<image>
			<![CDATA[
				'.$url.''.$banxml['image'].'
			]]>
			</image>
			<url>
			<![CDATA[
				'.$banxml['url'].'
			]]>
			</url>
		</banner>
	';
}

if($isUpdated)
	$xml .= $xmlContent;

if(isset($_GET['timestamp']))
	$xml .= xml_get_server_time($isUpdated);

$xml .= '
	</bannerList>
';
echo $xml;
?>