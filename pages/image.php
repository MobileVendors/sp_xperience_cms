<?php
include('../inc/conn.php');
$id = clean($_GET['id']);
$eqry = sql_query("SELECT name FROM fyp_articles WHERE id = '".$id."'");
$erow = sql_fetch($eqry);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload an image for article "<?php echo $erow['name']; ?>"</title>
<link href="../css/global.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/forms.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.10.2.min.js"></script> 
<script language="javascript">
function add_content(){
    $("#moar").append('<input type="text" class="caption" name="caption[]" placeholder="Caption for image below" /><input type="file" class="file" name="file[]" />');
}
</script>
</head>

<body>
<?php
if (isset($_GET['go'])) {
        $destpath = '../media/';
	while(list($key, $value) = each($_FILES['file']['name'])) {
        $filesize = @filesize($_FILES['file']['tmp_name']);
        if (!empty($value)) {
            if ($filesize > 1512000 || $_FILES['file']['error'][$key] > 0) {
                $content = '
                <h2>Image Uploaded</h2>
                Upload failed. Either you did not select a file, or the file was too large. Images cannot be bigger than 1.5 megabytes.
                ';
            } else {
                $contenttype 	= 1;
                $type			= explode('/', $_FILES['file']['type'][$key]);
                if($type[0] 	== 'image') {
                    $source 	= $_FILES['file']['tmp_name'][$key];
                    $x = str_replace(' ', '_', $_FILES['file']['name'][$key]);
                    $x = str_replace('+', '_', $x);
					$x = str_replace('%20', '_', $x);
                    $filename 	= rand(1,5).time().'-'.$x;
                    $filetype 	= $_FILES['file']['type'][$key];
                    move_uploaded_file($source, $destpath.$filename);
                    thumbify($destpath.$filename);
                    if (!empty($_POST['caption'][$key])) {
                        $caption 	= clean($_POST['caption'][$key]);
                    } else {
                        $caption	= $filename;
                    }
                    $name	= $destpath.$filename;
                    sql_query("INSERT INTO fyp_content (contentparent, contenttitle, contenttype, textexcerpt, contentvalue, defaultimg, modified) VALUES 
                    ('".$id."', '".$caption."', '".$contenttype."', '0', '".''.substr($name, 2)."', '0', '".date('Y-m-d H:i:s')."')");
                }
                $content = '
                <h2>Image Uploaded</h2>
                This window will close in 5 seconds..
                <script language="javascript" type="text/javascript">
                setTimeout(window.close, 5000)
                </script>
                ';
            }
        }
	}
} else {
	$content = '
		<h2>Upload an image</h2>
		<form action="?id='.$id.'&amp;go" method="post" enctype="multipart/form-data">
		<a href="javascript:add_content();" style="color:#fffaaa">Click here to add more files</a>
		<input type="text" class="caption" name="caption[]" placeholder="Caption for image below" /> 
		<input type="file" class="file" name="file[]" />
		<div id="moar"></div>
		<button type="submit" style="clear: both;">Upload Image(s)</button>
		</form>
		';
}
echo $content;
?>
</body>
</html>
