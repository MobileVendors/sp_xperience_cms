<?php
//include('pages/secure.php');
if ($srow['magazines'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
			$assignedMedia	= clean($_POST['assignedMedia']);
			$url	= clean($_POST['url']);
			
			$errorFlag = false;
			
			if(!empty($url)){
				if(!filter_var($url, FILTER_VALIDATE_URL)){
					$errorFlag = true;
					$content = '
					Invalid URL
				';
				}
			}
			
			if(!$errorFlag){
				if(!empty($_FILES['file']['name']) || $assignedMedia != 'Select image from gallery'){
					if($assignedMedia == 'Select image from gallery'){
						$filesize = filesize($_FILES['file']['tmp_name']);
				
						$type	= explode('/', $_FILES['file']['type']);
						
						list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
						if($type[0] == 'image'){
						if ($width <= 800 && $height <= 500 && $filesize <= 512000){
							$destpath = 'covers/';
							$source = $_FILES['file']['tmp_name'];
							$filename = rand(1,5).time().'-'.$_FILES['file']['name'];
							$filetype = $_FILES['file']['type'];
							move_uploaded_file($source, $destpath.$filename);
							thumbify($destpath.$filename);
								
							$qry = sql_query("INSERT INTO fyp_banners (image, url, modified) VALUES ('".$destpath.$filename."', '".mysqli_real_escape_string($dbLink, $url)."', '".date('Y-m-d H:i:s')."')");
									
								$last = sql_last_id('fyp_banners');
								log_action('Added Banner: '.$last.'');
									
								header('location: ?page=banners');
						} else {
							$content = '
								Image must not exceed maximum dimension of 800x500 and 512Kb filesize.
							';
						}
						} else
							$content = 'Filetype invalid. Please upload image format.';
							
					} else {
						//use the original if exists
						$assignedMediaTemp = str_replace('thumbs/', '', $assignedMedia);
						if(file_exists($assignedMediaTemp))
							$assignedMedia = $assignedMediaTemp;
				
						$qry = sql_query("INSERT INTO fyp_banners (image, url, modified) VALUES ('".$assignedMedia."', '".mysqli_real_escape_string($dbLink, $url)."', '".date('Y-m-d H:i:s')."')");
						
					    $last = sql_last_id('fyp_banners');
						log_action('Added Banner: '.$last.'');
				
						header('location: ?page=banners');
					}
					
				} else {
					$content = '
					Please select image
				';
				}
			}
			
		} else {
			$qry	= sql_query("SELECT * FROM fyp_banners");
			if (sql_num_rows($qry) < 4) {
				$content = '
			<h2>Adding a Banner</h2>
			<form action="?page=banners&amp;add&amp;go" method="post" enctype="multipart/form-data">
			<div class="upload">
				<table>
					<tr>
						<td style="width: 200px;">
				<label for="file">Image</label>
						</td>
						<td>
				<input type="file" name="file" id="file" class="realupload" onchange="this.form.fakeupload.value = this.value;" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
				<a href="javascript:popup(\'pages/media_files_popup.php?show=images\')"><img src="images/photo_album.png" title="Assign image/video from gallery" /></a>
				<input type="text" name="assignedMedia" id="assignedMedia" value="Select image from gallery" readonly style="display: inline"/>
							<a href="javascript:clearMediaField(\'assignedMedia\', \'Select image from gallery\')"><img src="images/delete.png" title="Clear" /></a>
						</td>
					</tr>
				</table>
			</div>
            <label for="name">URL</label>
			<input type="text" name="url" id="url" />
            <button type="submit" style="clear: both; display: block;">Add Banner</button>
			</form>
			';
			} else {
				$content = '
					Banner count reached maximum. Only 4 banners allowed.
			';
			}
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_POST['publish']) || isset($_POST['save']) || isset($_POST['test'])) {
			$name	= clean($_POST['url']);
			$assignedMedia	= clean($_POST['assignedMedia']);
			
			$name = mysqli_real_escape_string($dbLink, $name);
			sql_query("UPDATE fyp_banners SET url = '".$name."', modified = '".date('Y-m-d H:i:s')."' WHERE id=$id");
			log_action('Updated Banner: '.$id.'');
			
			if(!empty($_FILES['file']['name']) || $assignedMedia != 'Select image from gallery'){
				if($assignedMedia == 'Select image from gallery'){
					$filesize = filesize($_FILES['file']['tmp_name']);
					
					$isVideo = false;
					if(strpos($_FILES['file']['type'], 'video') !== false)
						$isVideo = true;
						
					$saveFlag = false;
					if($isVideo){
						if ($filesize <= 10000000)
							$saveFlag = true;
					} else {
						list($width, $height, $type) = getimagesize($_FILES['file']['tmp_name']);
						if ($width <= 800 && $height <= 500 && $filesize <= 512000)
							$saveFlag = true;
					}
						
					if($saveFlag){
						$destpath = 'videos/';
						if(!$isVideo)
							$destpath = 'covers/';
						$type	= explode('/', $_FILES['file']['type']);
						$source = $_FILES['file']['tmp_name'];
						$filename = rand(1,5).time().'-'.$_FILES['file']['name'];
						$filetype = $_FILES['file']['type'];
						move_uploaded_file($source, $destpath.$filename);
					
						if(!$isVideo)
							thumbify($destpath.$filename);
					
						$qry = sql_query("UPDATE fyp_banners SET image = '".$destpath.$filename."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
					}
				} else {
					//use the original if exists
					$assignedMediaTemp = str_replace('thumbs/', '', $assignedMedia);
					if(file_exists($assignedMediaTemp))
						$assignedMedia = $assignedMediaTemp;
						
					$qry = sql_query("UPDATE fyp_banners SET image = '".$assignedMedia."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
				}
			}
			header('location: ?page=banners');
		} else {
			$qry = sql_query("SELECT * FROM fyp_banners WHERE id = '".$id."' ");
            $erow = sql_fetch($qry);
            
            $img = $erow['image'];
            chmod(ROOT.$img, 0777);
            $url = "/plugins/phpimageeditor/index.php?imagesrc=$img";
            if($mode != 'development') {
            	$url = "/$prefix/plugins/phpimageeditor/index.php?imagesrc=$img";
            }
            
            $editImg = '<a href="javascript:popupeEditor(\''.$url.'\')"><img src="images/edit.png" title="Edit Banner Image" /></a>';
            
			$content = '
			<h2>Editing Banner</h2>
			<form action="?page=banners&amp;edit='.$id.'&amp;go" method="post" enctype="multipart/form-data">
			<div class="upload">
				<table>
					<tr>
						<td style="width: 200px;">
							<label for="file">Image</label>
						</td>
						<td>
							'.$editImg.'
							<a href="javascript:popup(\''.$erow['image'].'\')"><img src="images/view.png" title="View Banner" /></a></label>
				<input type="file" name="file" id="file" class="realupload" onchange="this.form.fakeupload.value = this.value;" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
				<a href="javascript:popup(\'pages/media_files_popup.php?show=images\')"><img src="images/photo_album.png" title="Assign image/video from gallery" /></a>
				<input type="text" name="assignedMedia" id="assignedMedia" value="Select image from gallery" readonly style="display: inline"/>
							<a href="javascript:clearMediaField(\'assignedMedia\', \'Select image from gallery\')"><img src="images/delete.png" title="Clear" /></a>
						</td>
					</tr>
				</table>
			</div>
            <label for="name">URL</label>
			<input type="text" name="url" id="url" value="'.html_entity_decode(stripslashes($erow['url'])).'" />
            <button type="submit" name="publish" class="normal">Save Banner</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$qry = sql_query("DELETE FROM fyp_banners WHERE id = '".$id."' ");
			$qry = sql_query("UPDATE fyp_banners SET modified = '".date('Y-m-d H:i:s')."' ");
			log_action('Deleted Banner: '.$id.'');
			header('location: ?page=banners');
		} else {
			$qry = sql_query("SELECT id FROM fyp_banners WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Banner "'.$row['id'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=banners&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=banners">Cancel</a>
			<p>
			';
		}
	} else {
		$qry	= sql_query("SELECT * FROM fyp_banners");
		$addbannertext = '';
		if (sql_num_rows($qry) < 4)
			$addbannertext = '<a href="?page=banners&amp;add" class="fr">Add Banner</a>';
			
		$subcontent = '
		<h2>'.$addbannertext.'Banners</h2>
			<table>
			<tr>
				<th id="table_main">Image</th>
				<th>Image Name</th>
				<th>URL</th>
				<th>Actions</th>
			</tr>
		';
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="4">There are currently no Banners on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			
			while($row = sql_fetch($qry)) {
				$img = $row['image'];
				$url = "/plugins/phpimageeditor/index.php?imagesrc=$img";
				if($mode != 'development') {
					$url = "/$prefix/plugins/phpimageeditor/index.php?imagesrc=$img";
				}
				$editAction = '<a href="javascript:popupeEditor(\''.$url.'\')"><img src="images/edit.png" title="Edit" /></a>';
				if(!$thumb = thumbify($img))
				{
					$thumb = HOST_URL."images".DS."no_available_image.png";
					$editAction = '';
				}
			
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td><img src="'.$thumb.'" width=64 height=64 /></td>    
                        <td>'.$img.'</td>
                        <td>'.$row['url'].'</td>
						<td class="td-actions"> 
							<a href="?page=banners&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Banner" /></a> 
							<a href="?page=banners&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Banner" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
<script type="text/javascript">
function popupeEditor(url){
    leftPos = (screen.width / 2) - 501
    topPos = (screen.height / 2) - 222
    mywindow = window.open(url, "_blank", "location=0, status=0, scrollbars=1, width=1024, height=800");
    mywindow.moveTo(leftPos, topPos);
}

function HandleAssignedMediaFromPopupResult(result){
	$('#assignedMedia').val(result);
	var control = $("#file");
	control.replaceWith( control = control.clone( true ) );
}

function clearMediaField(fieldName, defaultValue){
	var r = confirm("Do you want to reset image?");
	if (r == true){
		$('#'+fieldName).val(defaultValue);
		var control = $("#file");
		control.replaceWith( control = control.clone( true ) );
	}
}

$("document").ready(function(){
    $("#file").change(function() {
    	$('#assignedMedia').val('Select image from gallery');
    });
});
</script>
<style>
div.upload table td { 
	border: 0px solid white !important;
}
div.upload table td img {
	vertical-align: middle
}
</style>
