<?php
include('inc/conn.php');
if (file_exists('brb.php')) {
	include('brb.php');
	die();
}
include('pages/secure.php');
if (isset($_GET['page']) && file_exists('pages/'.$_GET['page'].'.php')) {
	$page = $_GET['page'];
} else {
	$page = 'dashboard';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project: Issues</title>
<link href="css/global.css" rel="stylesheet" type="text/css" />
<link href="css/forms.css" rel="stylesheet" type="text/css" />
<link href="css/tables.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>
<script src='js/colorpicker/spectrum.js'></script>
<script src='js/colorpicker/tinycolor.js'></script>
<link rel='stylesheet' href='js/colorpicker/spectrum.css' />
<script charset="utf-8" src="inc/kindeditor-min.js"></script>
<script charset="utf-8" src="inc/preview.js"></script>
<script>
KE.show({
        id : 'txtareaexcerpt',
        width : '100%',
        height : '100px',
		items : 
				['bold', 'italic', 'underline', 'source', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
		resizeMode : 0
});
KE.show({
        id : 'txtarea',
        width : '100%',
        height : '300px',
		items : 
				['bold', 'italic', 'underline', 'strikethrough', 'justifyleft', 'justifycenter', 'justifyright',
				'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
				'superscript', 'title', 'fontname', 'fontsize', 'link', 'unlink', '|', 'selectall', 'source', '', '', '', '', '', '', '', '', ''],
		resizeMode : 0
});
//<![CDATA[
$(function() {
// Return a helper with preserved width of cells
$("#sortable tbody.sorting").sortable({
	axis: 'y',
    placeholder: "td-placeholder",
	revert: true,
	handle: ".handle"
});
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};
 
$("#sortable tbody.sorting").sortable({
    helper: fixHelper
}).disableSelection();
});
function popup(url){
	leftPos = (screen.width / 2) - 251
	topPos = (screen.height / 2) - 162
    mywindow = window.open(url, "element", "location=0, status=0, scrollbars=1, width=502, height=325");
    mywindow.moveTo(leftPos, topPos);
}
//]]> 
</script>
</head>

<body>
<div id="wrap">
<?php
	if (isset($_SESSION['hash'])) { ?>
	<div id="betamessage">As this is a beta version, I would appreciate it if you could send <a href="http://issues.idea.informer.com/" target="_blank">feedback</a> if you encounter anything out of the ordinary. Your work will not be interrupted.</div>
<?php
	}
	?>
<div id="menuwrap">
	<div id="header">
		ISSUESv2
	</div>
	<div id="userbox">
		<?php 
			if (isset($_SESSION['hash'])) {
				echo '
				<strong>'.$_SESSION['name'].'</strong><a href="?page=dashboard">Dashboard</a> | <a href="?page=profile">Profile</a> | <a href="?page=logout">Logout</a>
				';
			}
		?>
	</div>
	<div id="menu">
    <?php 
	if (isset($srow)) {
	if ($srow['schdips'] == 1) {
		echo '
    <strong>Schools / Diplomas</strong>
    <ul> 
		<li><a href="?page=schdip&amp;list">List Schools / Diplomas</a></li>
		<li><a href="?page=schdip&amp;add">Add School / Diploma</a></li>
	</ul>
	';
	}
	if ($srow['magazines'] == 1) {
		echo '
    <strong>Magazines</strong>
    <ul> 
		<li><a href="?page=magazines&amp;list">List Magazines</a></li>
		<li><a href="?page=magazines&amp;add">Add Magazine</a></li>
		<!--<li><a href="?page=magcategories&amp;list">Magazine Categories</a></li>-->
		<li><a href="?page=magcategories2&amp;list">Magazine Categories</a></li>
	</ul>
	';
	}
	if ($srow['articles'] == 1) {
		echo '
    <strong>Articles</strong>
    <ul> 
		<li><a href="?page=articles&amp;list">List Articles</a></li>
		<li><a href="?page=articles&amp;add">Add Article</a></li>
		<li><a href="?page=categories&amp;list">Article Categories</a></li>
	</ul>
	';
	}
	if ($srow['users'] == 1) {
		echo '
    <strong>Users</strong>
    <ul> 
		<li><a href="?page=users&amp;list">List Users</a></li>
		<li><a href="?page=users&amp;add">Add User</a></li>
	</ul>
	';
	}
    if ($srow['banners'] == 1) {
    	echo '
    <strong>Banners</strong>
    <ul>
		<li><a href="?page=banners&amp;list">List Banners</a></li>
	</ul>
	';
    }
    if ($srow['users'] == 1) {
    	echo '
    <strong>About Us</strong>
    <ul>
		<li><a href="?page=aboutus&amp;view">View About Us</a></li>
	</ul>
    ';
    }
	}
	?>
	</div>
</div>
<div id="content">
<!--<div id="warning">Please note that any data other than user info MAY be periodically wiped. Please keep backups.</div>-->
<?php
	include('pages/'.$page.'.php');
?>
</div>
</div>
<div id="footer">PROJECT: ISSUES</div>
</body>
</html>
