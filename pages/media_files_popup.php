<?php
include('../inc/conn.php');
//include('pages/secure.php');
if(isset($_GET['show'])){
	//20131127.get all images
	
	$show = clean($_GET['show']);
	$files = array();
	
	if(in_array($show, array('all', 'images'))){
		$files = getMediaFiles('covers/thumbs/', $files);
		$files = getMediaFiles('media/thumbs/', $files);
	}
	
	if(in_array($show, array('all', 'videos'))){
		$files = getMediaFiles('videos/', $files);
	}
	//end: get all images
		
	$content = '
			<div id="fileTree" class="demo">';
	foreach($files as $file){
		$content .= '
					<a style="margin: 10px 5px;" onclick="javascript: setAssignedMedia(\''.$file['name'].'\');">
				';
		if($file['type'] == 'video'){
			$content .= '
						<video style="height: 40px; width: 40px; margin-top: 5px">
						  <source src="'.$url.$file['name'].'" />
						  Your browser does not support the video tag.
						</video>
					';
		} else {
			$content .= '
						<img width=40 height=40 style="margin-top: 5px" src="'.$url.$file['name'].'" /></a>
					';
		}
		$content .= '
					</a>
				';
	}
	$content .= '
			</div>
			';	
}			
?>
<?php
echo $content;?>
<style>
<!--
a:hover {
background-color: red;
}
-->
</style>
<script type="text/javascript">
function setAssignedMedia(name){
	var r = confirm("Do you want to use this image/video?");
	if (r == true){
		try {
	        window.opener.HandleAssignedMediaFromPopupResult(name);
	    }
	    catch (err) {}
	    window.close();
	    return false;
	}
}
</script>
<?php function getMediaFiles($folder, $filesArray = null){
	$dir = str_replace('pages', '', getcwd().$folder);
	$dh = opendir($dir);
	if ($dh) {
		while($file = readdir($dh)) {
			//if (!in_array($file, array('.', '..', 'empty', '.gitignore', 'thumbs.db', 'IpsThumb.db'))) {
			if (is_file($dir.$file)){
				$filetype = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $dir.$file);
				$filetype = strstr($filetype, '/', true);
				$filesArray[] = array('name'=>$folder.$file, 'type'=>$filetype);
			}
			//}
		}
	}
	closedir($dh);
	
	return $filesArray;
}?>