<?php
//include('pages/secure.php');
if ($srow['categories'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
		$name = clean($_POST['name']);
		sql_query("INSERT INTO fyp_cats (name, modified) VALUES ('".$name."', '".date('Y-m-d H:i:s')."')");
		log_action('Added Article Category: '.$name.'');
		header('location: ?page=categories');
		} else {
		$content = '
		<h2>Add a Article Category</h2>
		<form action="?page=categories&amp;add&amp;go" method="post">
		<input type="text" name="name" id="name" placeholder="Article Category Name (255 Characters)" />
		<button type="submit">Add Article Category</button>
		</form>
		';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$descr = clean($_POST['descr']);
			$exc = sql_query("SELECT id, name FROM fyp_cats WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_cats SET name = '".$name."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
			log_action('Updated Article Category: '.$old['name'].' to '.$name.'');
			header('location: ?page=categories');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_cats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Edit a Article Category</h2>
			<form action="?page=categories&amp;edit='.$id.'&amp;go" method="post">
			<input type="text" name="name" id="name" placeholder="Article Category Name (255 Characters)" value="'.$row['name'].'" />
			<button type="submit">Update Article Category</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$exc = sql_query("SELECT id, name FROM fyp_cats WHERE id = '".$id."' ");
			$run = sql_fetch($exc);
			sql_query("UPDATE fyp_cats SET modified = '".date('Y-m-d H:i:s')."' ");
			$qry = sql_query("DELETE FROM fyp_cats WHERE id = '".$id."' ");
			log_action('Deleted Article Category: '.$run['name'].'');
			header('location: ?page=categories');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_cats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Article Category "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=categories&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=category">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2><span class="fr"><a href="?page=categories&amp;add">Add Category</a></span>Article Categories</h2>
			<table>
			<tr>
				<th id="table_main">Article Category Name</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, name FROM fyp_cats ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Categories on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['name'].'</td>
						<td class="td-actions">
							<a href="?page=categories&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Article Category" /></a> 
							<a href="?page=categories&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Article Category" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
