<?php
//include('pages/secure.php');
if ($srow['groups'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
        if (isset($_GET['go'])) {
            $name	= clean($_POST['group_name']);
            if(empty($name)) {
                setFlashAndRedirectTo('Group name must not be blank', '?page=groups&add');
            }
            $qry = sql_query("SELECT name FROM fyp_schools WHERE name = '$name'");
            $row = sql_fetch($qry);
            if($row['name']) {
                setFlashAndRedirectTo('Groups already exist', '?page=groups&add');
            }
            $description	= clean($_POST['group_description']);
            $created_by = $_SESSION['userid'];
            sql_query("INSERT INTO fyp_schools (name, description, created_by, created_at) 
                VALUES('$name', '$description', $created_by, NOW())");
			log_action('Added Group: '.$name.'');
			redirectTo('?page=groups&list');
        } else {
			$content = '
            <h2>Add a Group</h2>
            <span id="flash" style="color:red">'.showFlash().'</span>
			<form action="?page=groups&amp;add&amp;go" method="post">
			<label for="name">Name</label><input type="text" name="group_name" id="group_name" placeholder="255 Characters maximum" />
			<label for="description">Description</label><input type="text" name="group_description" id="group_description" placeholder="255 Characters maximum" />
			<button type="submit">Add Group</button>
			</form>
			';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
        if (isset($_GET['go'])) {
			$name	= clean($_POST['group_name']);
            $description	= clean($_POST['group_description']);
            if(empty($name)) {
                setFlashAndRedirectTo('Group name must not be blank', "?page=groups&edit=$id");
            }
            $qry = sql_query("SELECT name FROM fyp_schools WHERE name = '$name' AND id != $id");
            $row = sql_fetch($qry);
            if($row['name']) {
                setFlashAndRedirectTo('Groups already exist', "?page=groups&edit=$id");
            }
            $qry = sql_query("UPDATE fyp_schools SET 
                name = '$name', description = '$description' WHERE id = $id");
			log_action("Updated Group: $username  (Group# $id)");
			redirectTo('?page=groups&list');
		} else {
			$qry = sql_query("SELECT name, description FROM fyp_schools WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
            <h2>Edit a Group</h2>
            <span id="flash" style="color:red">'.showFlash().'</span>
			<form action="?page=groups&amp;edit='.$id.'&amp;go" method="post">
			<label for="name">Name</label><input type="text" name="group_name" id="group_name" placeholder="255 Characters maximum" value="'.$row['name'].'" />
			<label for="description">Description</label><input type="text" name="group_description" id="group_description" placeholder="255 Characters maximum" value="'.$row['description'].'" />
			'.$opt.'
			<button type="submit">Edit Group</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$exc = sql_query("SELECT id, name FROM fyp_schools WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("DELETE FROM fyp_schools WHERE id = '".$id."' ");
            log_action('Deleted Group: '.$old['name'].' (Group#'.$old['id'].')');
			redirectTo('?page=groups&list');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_schools WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Group "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=groups&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=groups&list">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2>List Groups</h2>
			<table>
			<tr>
                <th>Name</th>
                <th>Description</th>
				<th>Actions</th>
			</tr>
		';
        $qry	= sql_query("SELECT * FROM fyp_schools");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Groups on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
                        <td>'.$row['name'].'</td>
                        <td>'.$row['description'].'</td>
						<td class="td-actions">
							<a href="?page=groups&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Group" /></a> 
							<a href="?page=groups&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Group" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
	echo $content;
?>
</div>
