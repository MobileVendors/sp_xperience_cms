<?php
if ($srow['magazines'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
		$name = clean($_POST['name']);
		$color = clean($_POST['color_code']);
		sql_query("INSERT INTO fyp_magcats (name, color_code, modified) VALUES ('".$name."', '".$color."', '".date('Y-m-d H:i:s')."')");
		log_action('Added Magazine Category: '.$name.'');
		header('location: ?page=magcategories2');
		} else {
		$content = '
		<h2>Add a Magazine Category</h2>
		<form action="?page=magcategories2&amp;add&amp;go" method="post">
		<input type="text" name="name" id="name" placeholder="Magazine Category Name (255 Characters)" />
		<label for="color_code" style="padding: 0px 5px; margin-top: 2px;">Color Code <input type="text" id="color_code" name="color_code" style="width: 50px;display: inline;margin-top: 1px;float: right;" /></label>
		<input type="text" id="colorpicker" readonly/>
		<button type="submit">Add Magazine Category</button>
		</form>
		';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$color = clean($_POST['color_code']);
			$descr = clean($_POST['descr']);
			$exc = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_magcats SET name = '".$name."', color_code = '".$color."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
			log_action('Updated Magazine Category: '.$old['name'].' to '.$name.'');
			header('location: ?page=magcategories2');
		} else {
			$qry = sql_query("SELECT id, name, color_code FROM fyp_magcats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Edit a Magazine Category</h2>
			<form action="?page=magcategories2&amp;edit='.$id.'&amp;go" method="post">
			<input type="text" name="name" id="name" placeholder="Magazine Category Name (255 Characters)" value="'.$row['name'].'" />
			<label for="color_code" style="padding: 0px 5px; margin-top: 2px;">Color Code <input type="text" id="color_code" name="color_code" value="'.$row['color_code'].'" style="width: 50px;display: inline;margin-top: 1px;float: right;" /></label>
			<input type="text" id="colorpicker" value="'.$row['color_code'].'" readonly/>
			<button type="submit">Update Magazine Category</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$exc = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_magcats SET modified = '".date('Y-m-d H:i:s')."' ");
			$qry = sql_query("DELETE FROM fyp_magcats WHERE id = '".$id."' ");
			log_action('Deleted Magazine Category: '.$old['name'].'');
			header('location: ?page=magcategories2');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Magazine Category "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=magcategories2&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=magcategories2">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2><a href="?page=magcategories2&amp;add" class="fr">Add Category</a>Magazine Categories</h2>
			<table>
			<tr>
				<th id="table_main">Magazine Category Name</th>
				<th>Color Code</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, name, color_code FROM fyp_magcats ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Categories on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['name'].'</td>
						<td style="padding: 5px; background-color: '.$row['color_code'].';"></td>
						<td class="td-actions">
							<a href="?page=magcategories2&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Magazine Category" /></a> 
							<a href="?page=magcategories2&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Magazine Category" /></a>
							<a href="xml/xml_singlecat.php?category='.$row['id'].'"><img src="images/star.png" title="View Category XML" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
<script type="text/javascript">
$("#colorpicker").spectrum({
    //showInitial: true,
    //showInput: true,
    allowEmpty: true,
    showButtons: false,
    clickoutFiresChange: true
});

$('#colorpicker').change(function(){
	$('#color_code').val('#'+$("#colorpicker").spectrum('get').toHex());
});

$("#color_code").change(function() {
	if($.trim($("#color_code").val()) != ""){
		if(isValidHexColor($("#color_code").val()))
	    	$("#colorpicker").spectrum("set", $("#color_code").val());
		else
			alert('Invalid Color');
	}
});

function isValidHexColor(str) {
    //return str.match(/^#[a-f0-9]{6}$/i) !== null;
    return str.match(/(^#[0-9A-F]{8}$)|(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i) !== null;
}
</script>