<?php
//include('pages/secure.php');
if ($srow['magazines'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
        if (!empty($_POST['name'])) {
			$name = clean($_POST['name']);
			$date = clean($_POST['inputyear'].'-'.$_POST['inputmonth'].'-'.$_POST['inputday']);
			$cat	= $_POST['cat'];			
			$c = array();
            $descr = clean($_POST['descr'], 1, 1);
			$status = clean($_POST['status']);
			$filesize = filesize($_FILES['file']['tmp_name']);
			list($width, $height, $type) = getimagesize($_FILES['file']['tmp_name']);
			if ($width != 500 || $height != 500) {
				$content = '
					Incorrect dimensions. Magazine covers need to be exactly 500*500 pixels.
				';
				// If file is bigger than 500KB..
			} elseif ($filesize > 512000) {
				$content = '
					File size is too big. Magazine covers cannot be bigger than 500 kilobytes.
				';
			} else {
				$destpath = 'covers/';
				$type	= explode('/', $_FILES['file']['type']);
				$source = $_FILES['file']['tmp_name'];
				$x = str_replace(' ', '_', $_FILES['file']['name']);
				$x = str_replace('+', '_', $x);
				$filename = rand(1,5).time().'-'.$x;
				$filetype = $_FILES['file']['type'];
				move_uploaded_file($source, $destpath.$filename);
				$qry = sql_query("INSERT INTO fyp_mags (published, category, name, descr, picture, status, numpages, modified) 
				VALUES ('".$date."', '0', '".$name."', '".$descr."', '".$destpath.$filename."', '".$status."', '0', '".date('Y-m-d H:i:s')."')");
				$last = sql_last_id('fyp_mags');
				$ccount	= clean($_POST['ccount']);
	
				for($i=0;$i<$ccount;$i++){
					if (isset($cat[$i])) {
						sql_query("INSERT INTO fyp_magcatlink (magazine, category, modified) VALUES ('".$last."', '".$cat[$i]."', '".date('Y-m-d H:i:s')."')");
					}
				}
				log_action('Added Magazine: '.$name.'');
				header('location: ?page=magazines&pages='.$last.'');
			}
		} else {
			$content = '
			<h2>Adding a Magazine</h2>
			<form action="?page=magazines&amp;add" method="post" enctype="multipart/form-data">
			<label for="name">Magazine Name</label>
			<input type="text" name="name" id="name" placeholder="255 Characters maximum" />
			<label for="status">Magazine Status</label>
			<select name="status" id="status">
				<option value="0">Draft</option>
				<option value="1">Published</option>
				<option value="2">Test</option>
			</select>
			<label for="date">Magazine Publish Date</label>
			'.date_picker().'
			<div class="upload">
				<label for="file">Magazine Cover</label>
				<input type="file" name="file" id="file" class="realupload" onchange="this.form.fakeupload.value = this.value;" />
			</div>
			<h3>Category</h3>
			<div class="wrapper">
			';
			$qry = sql_query("SELECT id, name FROM fyp_magcats ORDER BY name");
			$cnum = sql_num_rows($qry);
			while ($row = sql_fetch($qry)) {
				$content .= '
					<label class="normal"><input type="checkbox" name="cat[]" id="cat[]" value="'.$row['id'].'" /> '.$row['name'].'</label>
				';
			}
			$content .= '
			</div>
			<input type="hidden" name="ccount" id="ccount" value="'.$cnum.'" />
            <h3>Magazine Description</h3>
            <div id="txtmargin"><textarea id="txtarea" name="descr" placeholder="Description of Magazine"></textarea></div>
            <button type="submit" name="publish" class="normal">Save Magazine</button>
			</form>
			';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_POST['publish']) || isset($_POST['save']) || isset($_POST['test'])) {
			$name	= clean($_POST['name']);
			$date = clean($_POST['inputyear'].'-'.$_POST['inputmonth'].'-'.$_POST['inputday']);

			$cat	= $_POST['cat'];			
			$c = array();

			$descr	= clean($_POST['descr']);
			$status	= clean($_POST['status']);
			$qry = sql_query("UPDATE fyp_mags SET published = '".$date."', status = '".$status."', name = '".$name."', descr = '".$descr."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."'");
			log_action('Updated Magazine: '.$name.'');
			sql_query("UPDATE fyp_magcatlink SET modified = '".date('Y-m-d H:i:s')."'");
			sql_query("DELETE FROM fyp_magcatlink WHERE magazine = '".$id."'");
			$ccount	= clean($_POST['ccount']);

			for($i=0;$i<$ccount;$i++){
				if (isset($cat[$i])) {
					sql_query("INSERT INTO fyp_magcatlink (magazine, category, modified) VALUES ('".$id."', '".$cat[$i]."', '".date('Y-m-d H:i:s')."')");
				}
			}
			$filesize = filesize($_FILES['file']['tmp_name']);
			list($width, $height, $type) = getimagesize($_FILES['file']['tmp_name']);
			if ($width == 500 && $height == 500 && $filesize <= 512000) {
				$destpath = 'covers/';
				$type	= explode('/', $_FILES['file']['type']);
				$source = $_FILES['file']['tmp_name'];
				$filename = rand(1,5).time().'-'.$_FILES['file']['name'];
				$filetype = $_FILES['file']['type'];
                                move_uploaded_file($source, $destpath.$filename);
                                thumbify($destpath.$filename);
				$qry = sql_query("UPDATE fyp_mags SET picture = '".$destpath.$filename."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
			}
			header('location: ?page=magazines');
		} else {
			$qry = sql_query("SELECT id, status, category, name, descr, picture FROM fyp_mags WHERE id = '".$id."' ");
            $erow = sql_fetch($qry);
			$opt = '
				<select name="status" id="status">
			';
			for ($i = 0; $i < 3; $i++) {
				if ($i == $erow['status']) {$selected = ' selected="selected"';} else {$selected = '';}
				$status = array('Draft', 'Published', 'Test');
				$opt .= '
					<option value="'.$i.'"'.$selected.'>'.$status[$i].'</option>
				';
			}
			$opt .= '
				</select>
                                ';
                        $img = $erow['picture'];
                        chmod(ROOT.$img, 0777);
                        $url = "/plugins/phpimageeditor/index.php?imagesrc=$img";
                        if($mode != 'development') {
                            $url = "/$prefix/plugins/phpimageeditor/index.php?imagesrc=$img";
                        }
                        $editImg = '<a href="javascript:popupeEditor(\''.$url.'\')"><img src="images/edit.png" title="Edit Magazine Cover" /></a>';
			$content = '
			<h2>Editing a Magazine</h2>
			<form action="?page=magazines&amp;edit='.$id.'&amp;go" method="post" enctype="multipart/form-data">
			<label for="name">Magazine Name</label>
			<input type="text" name="name" id="name" placeholder="255 Characters maximum" value="'.$erow['name'].'" />
			<label for="status">Magazine Status</label>
			'.$opt.'
			<label for="date">Magazine Publish Date</label>
			'.date_picker($id).'
			<div class="upload">
				<label for="file">Magazine Cover '.$editImg.'
				<a href="javascript:popup(\''.$erow['picture'].'\')"><img src="images/view.png" title="View Magazine Cover" /></a></label>
				<input type="file" name="file" id="file" class="realupload" onchange="this.form.fakeupload.value = this.value;" />
			</div>
			<h3>Category</h3>
			<div class="wrapper">
			';
			$cat = sql_query("SELECT id, name FROM fyp_magcats");
			$cnum = sql_num_rows($cat);
            while ($ecat = sql_fetch($cat)) {
				$qry = sql_query("SELECT category FROM fyp_magcatlink WHERE magazine = '".$id."' AND category = '".$ecat['id']."'");
				$row = sql_fetch($qry);
					if (sql_num_rows($qry) == 1) {
						$checked = 'checked="checked" ';
					} else {
						$checked = '';
					}
					$content .= '
						<label class="normal"><input type="checkbox" name="cat[]" id="cat[]" value="'.$ecat['id'].'" '.$checked.'/> '.$ecat['name'].'</label>
					';
            }
			$content .= '
			</div>
			<input type="hidden" name="ccount" id="ccount" value="'.$cnum.'" />
            <h3>Magazine Description</h3>
            <div id="txtmargin"><textarea id="txtarea" name="descr" placeholder="Description of Magazine">'.$erow['descr'].'</textarea></div>
            <button type="submit" name="publish" class="normal">Save Magazine</button>
			</form>
			';
		}
	} elseif (isset($_GET['pages'])) {
		$id = clean($_GET['pages']);
		$mqry = sql_query("SELECT id, name, numpages FROM fyp_mags WHERE id = '".$id."' ");
		$mrow = sql_fetch($mqry);
		$content = '
		<h2><span class="fr"><a href="?page=pages&amp;add='.$id.'">Add page</a></span>Pages for '.$mrow['name'].'</h2>
		<form action="?page=reorder&amp;mag='.$id.'" method="post">
			<table id="sortable">
			<thead>
                        <tr>
				<th>Page Title</th>
				<th id="table_main">Page Name</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody class="sorting">
		';
		$qry = sql_query("SELECT id, pageid FROM fyp_magpages WHERE magid = '".$id."'");
		if (sql_num_rows($qry) == 0) {
			$content .= '
				<tr>
					<td colspan="3">There are currently no Pages for this Magazine</td>
				</tr>
			';
		} else {
			$mod = 0;
			$row = sql_fetch($qry);
				$eqry = sql_query("SELECT id, name, title, pagenum, template FROM fyp_pages WHERE parent = '".$id."' ORDER BY pagenum"); 
				while($erow = sql_fetch($eqry)) {
					$content .= '
                                            <tr'.($mod % 2 ? '' : ' class="table_alt"').'><td>'.$erow["title"].'</td>
							<td class="td-actions">'.$erow['name'].'<input type="hidden" name="pnum[]" value="'. $erow['id'] .'" /></td>
							<td class="td-actions">
								<span class="handle fr"><img src="images/move.png" title="Reorder Page" /></span>
								<a href="?page=pages&amp;title='.$erow['id'].'"><img src="images/title.png" title="Edit Page Name And Title" /></a> 
								<a href="?page=pages&amp;content='.$erow['id'].'"><img src="images/content_add.png" title="Manage Articles" /></a> 
								<a href="?page=pages&amp;edit='.$erow['id'].'"><img src="images/edit.png" title="Edit Page" /></a> 
								<a href="?page=pages&amp;delete='.$erow['id'].'&mag_id='.$id.'"><img src="images/delete.png" title="Delete Page" /></a>
							</td>
						</tr>
					';
				$mod++;
				}
		}
		$content	.= '
			</tbody>
			
			</table>
			<button type="submit">Reorder Pages</button>
			</form>
		';
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$exc = sql_query("SELECT id, name FROM fyp_mags WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_mags SET modified = '".date('Y-m-d H:i:s')."' ");
			$qry = sql_query("DELETE FROM fyp_mags WHERE id = '".$id."' ");
			log_action('Deleted Magazine: '.$old['name'].'');
			header('location: ?page=magazines');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_mags WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Magazine "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=magazines&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=magazines">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
		<h2>List Magazines</h2>
			<table>
			<tr>
				<th id="table_main">Magazine Name</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, name FROM fyp_mags ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Magazines on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['name'].'</td>
						<td class="td-actions">
							<a href="?page=magazines&amp;pages='.$row['id'].'"><img src="images/page_add.png" title="Add Pages to Magazine" /></a> 
							<a href="?page=magazines&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Magazine" /></a> 
							<a href="?page=magazines&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Magazine" /></a>
							<a href="xml/xml_singlemag.php?id='.$row['id'].'"><img src="images/star.png" title="View Magazine XML" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
<script type="text/javascript">
function popupeEditor(url){
    leftPos = (screen.width / 2) - 501
    topPos = (screen.height / 2) - 222
    mywindow = window.open(url, "_blank", "location=0, status=0, scrollbars=1, width=1024, height=800");
    mywindow.moveTo(leftPos, topPos);
}
</script>
