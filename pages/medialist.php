<?php
include('../inc/conn.php');
$id = clean($_GET['id']);
$qry = sql_query("SELECT name FROM fyp_articles WHERE id = '".$id."'");
$row = sql_fetch($qry);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List of media for "<?php echo $row['name']; ?>"</title>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="../css/global.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/forms.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//<![CDATA[ 
var order = [];
$(document).ready(function() {
    $("#sortable").sortable({
        stop: function(e, ui) {
            $.map($(this).find('li'), function(el) {
                order[$(el).index()] = el.id.split("-")[1];
            });
            //console.log(order);
        }
    });
    $('.delete').click(function() { 
        var id = $(this).attr('name');
        $('#confirm' + id).slideToggle('fast');
        return false; 
    });
    $('.editVideo').click(function() { 
        var id = $(this).attr('name');
        $('#editVideoSlider' + id).slideToggle('fast');
        return false; 
    });
});
function submitReorder() {
    $.ajax({
        'type': 'POST',
        'url': 'medialist.php',
        'data': {'ordering': order.toString()},
        'success': function(data) {
           alert('Reorder Saved Successfully') 
        }
    });
}
function editVideoSubmit(id){
	$('#editVideoForm'+id).submit();
}
function popupeEditor(url){
    leftPos = (screen.width / 2) - 501
    topPos = (screen.height / 2) - 222
    mywindow = window.open(url, "_blank", "location=0, status=0, scrollbars=1, width=1024, height=800");
    mywindow.moveTo(leftPos, topPos);
}

function popup(url){
	leftPos = (screen.width / 2) - 251
	topPos = (screen.height / 2) - 162
    mywindow = window.open(url, "element", "location=0, status=0, scrollbars=1, width=502, height=325");
    mywindow.moveTo(leftPos, topPos);
}
//]]> 
</script>
</head>

<body>
<?php
if (isset($_POST['ordering'])) {
    if(!empty($_POST['ordering'])) {
        $ordering = explode(",", clean($_POST['ordering']));
        foreach($ordering as $order => $id) {
            $sql = "UPDATE fyp_content SET contentorder = $order, modified = '".date('Y-m-d H:i:s')."' WHERE id = $id";
            sql_query($sql);
        }
    }
}

if (isset($_GET['del'])) {
	$del = clean($_GET['del']);
	sql_query("UPDATE fyp_content SET modified = '".date('Y-m-d H:i:s')."' ");
	sql_query("DELETE FROM fyp_content WHERE id = '".$del."'");
	header('location: ?id='.$id.'');
}
if (isset($_GET['edit'])) {
	$edit = clean($_GET['edit']);
	$contentvalue	= clean($_POST['editcvalue']);
	$caption		= clean($_POST['editcaption']);
	$caption		= urlencode($_POST['editcaption']);	
	
	if(strtolower(substr($contentvalue, 0, 4)) != 'http')
		$contentvalue = 'http://'.$contentvalue;
	
	$youtubepos = strpos(strtolower($contentvalue), 'youtube');
	$contentvaluepos = strpos($contentvalue, '#');
	if($youtubepos !== false){
		if($contentvaluepos !== false){
			if($youtubepos < $contentvaluepos)
				$contentvalue = substr($contentvalue, 0, $contentvaluepos);
		}
	}
	
	sql_query("UPDATE fyp_content SET contenttitle = '$caption', contentvalue = '$contentvalue', modified = '".date('Y-m-d H:i:s')."' WHERE id = '$edit'");
	header('location: ?id='.$id.'');
}
if (isset($_GET['primary'])) {
	$pri = clean($_GET['primary']);
	sql_query("UPDATE fyp_content SET defaultimg = 1, modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$pri."'");
	sql_query("UPDATE fyp_content SET defaultimg = 0, modified = '".date('Y-m-d H:i:s')."' WHERE contentparent = '".$id."' AND id != '".$pri."'");
	header('location: ?id='.$id.'');
}
$eqry = sql_query("SELECT id, contenttitle, contenttype, contentvalue, defaultimg FROM fyp_content WHERE contentparent = '".$id."' AND contenttype != 3 ORDER BY contentorder ASC");
$content = '<h2>Media List</h2>';
$content .= '<ul id="sortable">';
if (sql_num_rows($eqry) == 0) {
    $content .= '<li>No media for this article.</li>';
} else {
    while ($erow = sql_fetch($eqry)) {
    	
    	//$img = ltrim($erow['contentvalue'], '/');
    	$img = $erow['contentvalue'];
    	if('adm'==substr($img, 0, 3))
    		$img = substr($img, 3);
    	
    	if("/"==substr($img, 0, 1)) {
    		$img = ltrim($img, '/');
    	}
    	
        switch($erow['contenttype']) {
        	case 1:
            	$thumb = thumbify($img);
                break;
            case 2:
            	$thumb = HOST_URL.'images'.DS.'movies.png';
                break;
			default:
            	$thumb = HOST_URL.'images'.DS.'clipping_text.png';
        }
		$def = '';
		$css = '';
		
		if ($erow['defaultimg'] == 1) {
			$css = ' def';
			$def = 'Primary media.';
                } else {
                    if($erow['contenttype'] == 1){ chmod(ROOT.$img, 0777);
                    $editorUrl = "/plugins/phpimageeditor/index.php?imagesrc=$img";
                    if($mode != 'development') {
                        $editorUrl = "/$prefix/plugins/phpimageeditor/index.php?imagesrc=$img";
                    }
                        $def = '<a href="javascript:popupeEditor(\''.$editorUrl.'\')">Edit Image</a> | ';
					}
			$def .= '<a href="?id='.$id.'&amp;primary='.$erow['id'].'">Set as Primary</a>';
		}
		
		$nameUrl = $url.$img;
		$editVideoLink = "";
		$name	= $erow['contenttitle'];
		
		if($erow['contenttype'] == 2){
			$nameUrl = $img;
			$name = urldecode($erow['contenttitle']);
			$editVideoLink = '<a class="editVideo" name="'.$erow['id'].'">Edit</a> | ';
		} 
		
		
                $content .= '<li id="l-'.$erow['id'].'">
                    <div class="fauxlist'.$css.'"><span class="fr"> '.$editVideoLink.' '.$def.' | <a class="delete" name="'.$erow['id'].'">Delete</a></span>
		<a class="img-name" href="'.$nameUrl.'" target="_blank"><span class="thumb"><img src="'.$thumb.'" width="16" height="16" style="position: relative; left: -2px; top: 4px; margin-right: 5px" /></span>'.$name.'</a></div>
		<div class="hider" id="confirm'.$erow['id'].'">
		<span class="fr"><a href="?id='.$id.'&amp;del='.$erow['id'].'">Yes</a> | <a href="?id='.$id.'" class="no">No</a></span>
                Really delete this file?</div>
		<div class="hider" id="editVideoSlider'.$erow['id'].'">
		<span class="fr">
			<form id="editVideoForm'.$erow['id'].'" action="?id='.$id.'&amp;edit='.$erow['id'].'" method="post">
			<input type="text" id="editcaption" style="width: 288px" name="editcaption" value="'.urldecode($erow['contenttitle']).'" placeholder="Caption for this video" />
			<input type="text" id="editcvalue" style="width: 288px" name="editcvalue" value="'.$erow['contentvalue'].'" placeholder="Youtube Link" />
			<a onclick="javascript:editVideoSubmit('.$erow['id'].')">Update</a> | <a href="?id='.$id.'" class="no">Cancel</a>
			</form></div></li>';
	}
        $content .= '</ul>';
}
echo $content;
$imgSrc = $mode != 'development' ? "/$prefix/images/reorder_list.png" : '/images/reorder_list.png';
?>

    <img src="<?php echo $imgSrc ?>" onclick="submitReorder()" style="cursor:pointer;"/>
</body>
</html>
