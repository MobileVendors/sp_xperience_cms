<?php
if ($srow['articles'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['addcontent'])) {
		$id		= clean($_GET['addcontent']);
		$art	= clean($_GET['type']);
        if (isset($_GET['go'])) {
            $excerpt = clean($_POST['txtareaexcerpt'], 1, 1);
			$content = clean($_POST['cvalue'], 1);
			sql_query("UPDATE fyp_content SET textexcerpt = '".$excerpt."', contentvalue = '".$content."', modified = '".date('Y-m-d H:i:s')."' WHERE contentparent = '".$id."' AND contenttype = 3");
			header('location: ?page=articles');
		} else {
			$tool = '
			<span class="fr">
				<a href="?page=articles&amp;edit='.$id.'">Edit article</a>
			</span>
			<a href="javascript:popup(\'pages/image.php?id='.$id.'\')">Add an Image</a> | 
			<a href="javascript:popup(\'pages/video.php?id='.$id.'\')">Add a Youtube Video</a> |
			<a href="javascript:popup(\'pages/medialist.php?id='.$id.'\')">List of Media</a>
			';
			$eqry = sql_query("SELECT name FROM fyp_articles WHERE id = '".$id."'");
			$erow = sql_fetch($eqry);
			$content = '
			<h2>Add Content to Article: '.$erow['name'].'</h2>
			<form action="?page=articles&amp;addcontent='.$id.'&amp;type='.$art.'&amp;go" method="post">
			<div id="txtmargin"><textarea id="txtareaexcerpt" name="txtareaexcerpt" placeholder="Content Excerpt"></textarea></div>
            <p>
			'.$tool.'
			</p>
			<p style="color: #F00;">Note: Please do not paste content from MS Word, as it may cause the script to break down. If it does break down, I will have to take it down to fix it, meaning you won\'t get your work done, right? I appreciate your understanding. :)</p>
            <div id="txtmargin"><textarea id="txtarea" name="cvalue" placeholder="Content Text"></textarea></div>
            <div style="clear: both;"></div>
			<button type="submit">Add Content to Article</button>
			</form>
			';
		} 
	} elseif (isset($_GET['editcontent'])) {
		$id		= clean($_GET['editcontent']);
		if (isset($_GET['go'])) {
			$excerpt = clean($_POST['txtareaexcerpt'], 1, 1);
			$content = clean($_POST['cvalue'], 1);
			sql_query("UPDATE fyp_content SET textexcerpt = '".$excerpt."', contentvalue = '".$content."', modified = '".date('Y-m-d H:i:s')."' WHERE contentparent = '".$id."' AND contenttype = 3");
			header('location: ?page=articles');
		} else {
			$eqry = sql_query("SELECT name, articletype FROM fyp_articles WHERE id = '".$id."'");
			$erow = sql_fetch($eqry);
			
			$cqry = sql_query("SELECT textexcerpt, contentvalue FROM fyp_content WHERE contentparent = '".$id."' AND contenttype = 3");
			$crow = sql_fetch($cqry);
			$tool = '
			<span class="fr">
				<a href="?page=articles&amp;edit='.$id.'">Edit article</a>
			</span>
			<a href="javascript:popup(\'pages/image.php?id='.$id.'\')">Add an Image</a> | 
			<a href="javascript:popup(\'pages/video.php?id='.$id.'\')">Add a Youtube Video</a> |
			<a href="javascript:popup(\'pages/medialist.php?id='.$id.'\')">List of Media</a>
			';
			$content = '
			<h2>Updating Content for Article: '.$erow['name'].'</h2>
			<form action="?page=articles&amp;editcontent='.$id.'&amp;go" method="post">
            <div id="txtmargin"><textarea id="txtareaexcerpt" name="txtareaexcerpt" placeholder="Content Excerpt">'.$crow['textexcerpt'].'</textarea></div>
            <p>
			'.$tool.'
			</p>
			<p style="color: #F00;">Note: Please do not paste content from MS Word, as it may cause the script to break down. If it does break down, I will have to take it down to fix it, meaning you won\'t get your work done, right? I appreciate your understanding. :)</p>
			<div id="txtmargin"><textarea id="txtarea" name="cvalue" placeholder="Content Text">'.$crow['contentvalue'].'</textarea></div>
            <div style="clear: both;"></div>
			<button type="submit">Update Content for Article</button>
			</form>
			';
		}
	} elseif (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
			$name	= clean($_POST['name']);
			$sname	= clean($_POST['subname']);
			$link	= clean($_POST['sharelink']);
			$art	= clean($_POST['articletype']);
			$dcount	= clean($_POST['dcount']);
			$ccount	= clean($_POST['ccount']);

            // CatDip. Heh.
			$cat	= ($_POST['cat']);
			$dip 	= ($_POST['dip']);

			$qry = sql_query("INSERT INTO fyp_articles 
				(name, subname, sharelink, categoryid, articletype, diplomaid, modified) VALUES 
				('".$name."', '".$sname."', '".$link."', '0', '".$art."', '0', '".date('Y-m-d H:i:s')."')");
			$last = sql_last_id('fyp_articles');
            
			for($i=0;$i<$ccount;$i++){
				if (isset($cat[$i])) {
                    sql_query("INSERT INTO fyp_articlecats (diploma, article, category, modified) VALUES
                              ('0', '".$last."', '".$cat[$i]."', '".date('Y-m-d H:i:s')."')");
                }
			}
 			for($i=0;$i<$dcount;$i++){
				if (isset($dip[$i])) {
                    sql_query("INSERT INTO fyp_articlecats (diploma, article, category, modified) VALUES
                              ('1', '".$last."', '".$dip[$i]."', '".date('Y-m-d H:i:s')."')");
				}
			}
           										
			sql_query("INSERT INTO fyp_content (contentparent, contenttitle, contenttype, textexcerpt, contentvalue, defaultimg, modified) VALUES ('".$last."', '0', '3', '0', '0', '0', '".date('Y-m-d H:i:s')."')");
			log_action('Added Article: '.$name.'');
			header('location: ?page=articles&addcontent='.$last.'&type='.$art.'');
		} else {
			$content = '
			<h2>Adding an Article</h2>
			<form action="?page=articles&amp;add&amp;go" method="post">
			<label for="name">Article Title</label>
			<input type="text" name="name" id="name" placeholder="255 Characters maximum" />
			<label for="subname">Article Subtitle</label>
			<input type="text" name="subname" id="subname" placeholder="255 Characters maximum" />
			<label for="sharelink">Sharing Link</label>
			<input type="text" name="sharelink" id="sharelink" placeholder="Leave blank for default" />
			<label for="articletype">Article Type</label>
			<select name="articletype" id="articletype">
				<option value="1">Images/Text</option>
				<option value="2">Video/Text</option>
				<option value="3">Text Only</option>
			</select>
			<h3>Category</h3>
			<div class="wrapper">
			';
			$qry = sql_query("SELECT id, name FROM fyp_cats ORDER BY name");
			$cnum = sql_num_rows($qry);
			while ($row = sql_fetch($qry)) {
				$content .= '
					<label class="normal"><input type="checkbox" name="cat[]" id="cat[]" value="'.$row['id'].'" />'.$row['name'].'</label>
				';
			}
			$content .= '
			</div>
			<h3>School / Diploma Name</h3>
			<div class="wrapper">
			';
			$qry = sql_query("SELECT id, name, email FROM fyp_schdips ORDER BY id");
			$dnum = sql_num_rows($qry);
			while ($row = sql_fetch($qry)) {
				$content .= '
					<label class="normal"><input type="checkbox" name="dip[]" id="dip[]" value="'.$row['id'].'" />'.$row['name'].'</label>
				';
			}
			$content .= '
			</div>
			<input type="hidden" name="dcount" id="dcount" value="'.$dnum.'" />
			<input type="hidden" name="ccount" id="ccount" value="'.$cnum.'" />
			<button type="submit" style="clear: both; display: block;">Add Article</button>
			</form>
			';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$name	= clean($_POST['name']);
			$sname	= clean($_POST['subname']);
			$link	= clean(str_replace(' ', '%20', $_POST['sharelink']));
			$type 	= clean($_POST['articletype']);
			$dcount	= clean($_POST['dcount']);
			$ccount	= clean($_POST['ccount']);
			
			$cat	= $_POST['cat'];			
			$dip 	= $_POST['dip'];

			$exc = sql_query("SELECT name FROM fyp_articles WHERE id = '".$id."'");
			$old = sql_fetch($exc);
			sql_query("UPDATE fyp_articlecats SET modified = '".date('Y-m-d H:i:s')."' ");
            sql_query("DELETE FROM fyp_articlecats WHERE article = '".$id."'");
			for($i=0;$i<$ccount;$i++){
				if (isset($cat[$i])) {
                    sql_query("INSERT INTO fyp_articlecats (diploma, article, category, modified) VALUES
                              ('0', '".$id."', '".$cat[$i]."', '".date('Y-m-d H:i:s')."')");
                }
			}
 			for($i=0;$i<$dcount;$i++){
				if (isset($dip[$i])) {
                    sql_query("INSERT INTO fyp_articlecats (diploma, article, category, modified) VALUES
                              ('1', '".$id."', '".$dip[$i]."', '".date('Y-m-d H:i:s')."')");
				}
			}

			$qry = sql_query("UPDATE fyp_articles SET name = '".$name."', subname = '".$sname."', sharelink = '".$link."', articletype = '".$type."', modified = '".date('Y-m-d H:i:s')."' 
			WHERE id = '".$id."'");
			log_action('Updated Article: '.$old['name'].' to '.$name.'');
			header('location: ?page=articles&editcontent='.$id.'&type='.$type.'');
		} else {
			$eqry = sql_query("SELECT name, subname, sharelink, articletype FROM fyp_articles WHERE id = '".$id."' ");
			$erow = sql_fetch($eqry);
			$content = '
			<h2>Editing an Article</h2>
			<form action="?page=articles&amp;edit='.$id.'&amp;go" method="post">
			<label for="name">Article Title</label>
			<input type="text" name="name" id="name" value="'.$erow['name'].'" placeholder="255 Characters maximum" />
			<label for="subname">Article Subtitle</label>
			<input type="text" name="subname" id="subname" value="'.$erow['subname'].'" placeholder="255 Characters maximum" />
			<label for="sharelink">Sharing Link</label>
			<input type="text" name="sharelink" id="sharelink" value="'.$erow['sharelink'].'" placeholder="Leave blank for default" />
			';
			$type1 = '';
			$type2 = '';
			$type3 = '';
			if ($erow['articletype'] == 1) {
				$type1 = ' selected="selected"';
				$type2 = '';
				$type3 = '';
			} elseif ($erow['articletype'] == 2) {
				$type1 = '';
				$type2 = ' selected="selected"';
				$type3 = '';
			} elseif ($erow['articletype'] == 3) {
				$type1 = '';
				$type2 = '';
				$type3 = ' selected="selected"';
			}
			$content .= '
			<label for="articletype">Article Type</label>
			<select name="articletype" id="articletype">
				<option value="1"'.$type1.'>Images/Text</option>
				<option value="2"'.$type2.'>Video/Text</option>
				<option value="3"'.$type3.'>Text Only</option>
			</select>
			<h3>Category</h3>
			<div class="wrapper">
			';
			$qry = sql_query("SELECT id, name FROM fyp_cats ORDER BY name");
			$cnum = sql_num_rows($qry);
			while ($row = sql_fetch($qry)) {
                $cat = sql_query("SELECT category FROM fyp_articlecats WHERE article = '".$id."' AND category = '".$row['id']."' AND diploma = 0");
				if (sql_num_rows($cat) == 1) {
				$content .= '
					<label class="normal"><input type="checkbox" name="cat[]" id="cat[]" value="'.$row['id'].'" checked="checked" /> '.$row['id'].''.$row['name'].'</label>
				';
				} else {
				$content .= '
					<label class="normal"><input type="checkbox" name="cat[]" id="cat[]" value="'.$row['id'].'" /> '.$row['id'].''.$row['name'].'</label>
				';
				}
			}
			$content .= '
			</div>
			<h3>Diploma Name</h3>
			<div class="wrapper">
			';
			$qry = sql_query("SELECT id, name FROM fyp_schdips ORDER BY name");
			$dnum = sql_num_rows($qry);
			while ($row = sql_fetch($qry)) {
                $dip = sql_query("SELECT category FROM fyp_articlecats WHERE article = '".$id."' AND category = '".$row['id']."' AND diploma = 1");
				if (sql_num_rows($dip) == 1) {
				$content .= '
					<label class="normal"><input type="checkbox" name="dip[]" id="dip[]" value="'.$row['id'].'" checked="checked" /> '.$row['id'].''.$row['name'].'</label>
				';
				} else {
				$content .= '
					<label class="normal"><input type="checkbox" name="dip[]" id="dip[]" value="'.$row['id'].'" /> '.$row['id'].''.$row['name'].'</label>
				';
				}
			}
			$content .= '
			</div>
			<input type="hidden" name="dcount" id="dcount" value="'.$dnum.'" />
			<input type="hidden" name="ccount" id="ccount" value="'.$cnum.'" />
			<button type="submit" style="clear: both; display: block;">Edit Article</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$exc = sql_query("SELECT id, name FROM fyp_articles WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_articles SET modified = '".date('Y-m-d H:i:s')."' ");
			$qry = sql_query("DELETE FROM fyp_articles WHERE id = '".$id."' ");
			log_action('Deleted Article: '.$old['name'].'');
			header('location: ?page=articles');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_articles WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Article "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=articles&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=articles">Cancel</a>
			<p>
			';
		}
    } else {

        if(isset($_GET['filter'])) {
            $article_filter = $_SESSION['article_filter'] = !isset($_POST['article_filter']) ? 0 : $_POST['article_filter'];
            $article_count = $_SESSION['article_count'] = !isset($_POST['article_count']) ? 0 : $_POST['article_count'];
            $search_keyword = $_SESSION['search_keyword'] = !isset($_POST['search_keyword']) ? "" : $_POST['search_keyword'];
        }
        else {
            $article_filter = isset($_SESSION['article_filter']) ? $_SESSION['article_filter'] : 0;
            $article_count = isset($_SESSION['article_count']) ? $_SESSION['article_count'] : 0;
            $search_keyword = isset($_SESSION['search_keyword']) ? $_SESSION['search_keyword'] : '';
        }

        if(isset($_GET['page_change'])) {
            $page_number = !isset($_POST['page_number']) ? 1 : $_POST['page_number'];
        }
        else {
            $page_number = 1;
        }

        $cond = ""; 
        $filter_option = "
            <option value=0 selected>All Articles</option>
            <option value=1>Created Articles</option>
            <option value=2>Published Articles</option>";
        switch($article_filter){
            case 1:
                $filter_option = "
                    <option value=0>All Articles</option>
                    <option value=1 selected>Created Articles</option>
                    <option value=2>Published Articles</option>";
                $cond = "WHERE id NOT IN(SELECT DISTINCT(articleid) FROM fyp_articlepages)";
                break;
            case 2:       
                $filter_option = "
                    <option value=0>All Articles</option>
                    <option value=1>Created Articles</option>
                    <option value=2 selected>Published Articles</option>";
                $cond = "WHERE id IN(SELECT DISTINCT(articleid) FROM fyp_articlepages)";
                break;
        }
        
        if(!empty($search_keyword) && !empty($cond)) { 
            $cond .= " AND name LIKE '%$search_keyword%'";
        }
        else if(!empty($search_keyword) && empty($cond)) { 
            $cond .= "WHERE name LIKE '%$search_keyword%'";
        }
        $q = "SELECT id FROM fyp_articles $cond ORDER BY id DESC";
        $row_cnt = sql_num_rows(sql_query($q)); 
        
        $limit = 10;
        $count_option = "
            <option value=0 selected>10</option>
            <option value=1>20</option>
            <option value=2>50</option>
            <option value=3>All</option>";
        switch($article_count) {
            case 1:
                $count_option = "
                    <option value=0>10</option>
                    <option value=1 selected>20</option>
                    <option value=2>50</option>
                    <option value=3>All</option>";
                $limit = 20;
                break;
            case 2:
                $count_option = "
                    <option value=0>10</option>
                    <option value=1>20</option>
                    <option value=2 selected>50</option>
                    <option value=3>All</option>";
                $limit = 50;
                break;
            case 3:
                $count_option = "
                    <option value=0>10</option>
                    <option value=1>20</option>
                    <option value=2>50</option>
                    <option value=3 selected>All</option>";
                $limit = $row_cnt;
                break;
        }
        $offset = ($limit * $page_number) - $limit;
        $limit_sql = "LIMIT $offset, $limit";
        $pages_cnt = ceil($row_cnt/$limit);
        $page_number_opt = "";
        for($i=1; $i<=$pages_cnt; $i++) {
            $selected = $i==$page_number ? "selected" : "";
            $page_number_opt .= "<option value=$i $selected>$i</option>";
        }
        $paginator = <<<EOP
        <table>
            <tr>
                <td style="width:10px">Page </td>
                <td style="width:10px">
                    <form action="?page=articles&list&page_change" method="post">
                        <select onchange="this.form.submit()" style="width:50px" 
                            name="page_number" id="page_number">
                            $page_number_opt
                        </select>
                    </form>
                </td>
                <td>of $pages_cnt</td>
            </tr>
        </table>
EOP;
        $footer = <<<EOF
        <table class="tbl_header_footer">
            <tr><td>$paginator</td></tr>
        </table>
EOF;
        $filter= <<<EOS
        <form action="?page=articles&list&filter" method="post">
            <table class="tbl_filter">
                <tr>
                    <td>Filter</td>
                    <td>
                        <select name="article_filter" id="article_filter">
                        $filter_option
                        </select>
                    </td>
                    <td>Pages</td>
                    <td>
                        <select name="article_count" id="article_count">
                            $count_option
                        </select>
                    </td>
                    <td>Search</td>
                    <td>
                        <input type="text" style="width:200px" name="search_keyword" 
                        id="search_keyword" value="$search_keyword" />
                    </td>
                    <td><button>Go</button></td>
                </tr>
            </table>
        </form>
EOS;
        $header = <<<EOH
        <table class="tbl_header_footer">
            <tr>
                <td style="width:250px">$paginator</td>
                <td>$filter</td></tr>
        </table>
EOH;
		$subcontent = '
                    <h2>List Articles</h2>
		    '.$header.'
                    <table>
			<tr>
				<th>
                    Article Name</th>
                    <th>Used In</th>
				<th>Actions</th>
			</tr>
		';
        $q = "SELECT id FROM fyp_articles $cond ORDER BY id DESC $limit_sql";
        //print_r($q);
        $qry = sql_query($q);
        
        $article_id = array();
        while($row = sql_fetch($qry)) {
            $article_id[] = $row['id'];
        }
        $article_id = empty($article_id) ? 0 : implode(",", $article_id);
        $q= "SELECT fyp_articles.id, fyp_articles.name AS article_name, fyp_pages.name AS page_name,
                fyp_mags.name AS mag_name FROM fyp_articles LEFT JOIN fyp_articlepages ON
                fyp_articles.id = fyp_articlepages.articleid LEFT JOIN fyp_pages ON
                fyp_articlepages.pageid = fyp_pages.id LEFT JOIN fyp_magpages ON
                fyp_pages.id = fyp_magpages.pageid LEFT JOIN fyp_mags ON
                fyp_magpages.magid = fyp_mags.id WHERE fyp_articles.id IN ($article_id)
                ORDER BY fyp_articles.id DESC";
                //print_r($q);die;
                $qry = sql_query($q);
                if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Articles on record.</td>
				</tr>
			';
		} else {
                        $prev_id=0;$mod=0;$pages=null;$curr_id=0;$mags=null;
                        while($row = sql_fetch($qry)) {
                            if($prev_id==0) {
                                $prev_id = $row['id'];
                                $prev_row = $row;
                            }
                            $curr_id = $row['id'];
                            if($curr_id==$prev_id) {
                                $pages[] = $row['page_name'];
                                $mags[] = $row['mag_name'];
                            }
                            else {
                                $pages = implode(', ', array_unique($pages));
                                $mags = implode(', ', array_unique($mags));
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
                                        <td>'.$prev_row['article_name'].'</td>
                                        <td>
                                            <b><i>Pages: </i></b>'.$pages.'<br>
                                            <b><i>Magazines: </i></b>'.$mags.'
                                        </td>
						<td class="td-actions">
							<a href="?page=articles&amp;edit='.$prev_row['id'].'"><img src="images/edit.png" title="Edit Article" /></a> 
							<a href="?page=articles&amp;delete='.$prev_row['id'].'"><img src="images/delete.png" title="Delete Article" /></a>
						</td>
					</tr>
                                        ';
                                $prev_id = $curr_id;
                                $prev_row = $row;
                                $pages = array();
                                $mags = array();
                                $pages[] = $row['page_name'];
                                $mags[] = $row['mag_name'];
			        $mod++;
                            }
                        }
                        $pages = implode(', ', array_unique($pages));
                        $mags = implode(', ', array_unique($mags));
                        $subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
                                        <td>'.$prev_row['article_name'].'</td>
                                        <td>
                                            <b><i>Pages: </i></b>'.$pages.'<br>
                                            <b><i>Magazines: </i></b>'.$mags.'
                                        </td>
						<td class="td-actions">
							<a href="?page=articles&amp;edit='.$prev_row['id'].'"><img src="images/edit.png" title="Edit Article" /></a> 
							<a href="?page=articles&amp;delete='.$prev_row['id'].'"><img src="images/delete.png" title="Delete Article" /></a>
						</td>
					</tr>
                                        ';
		}
		$subcontent	.= '
                    </table>
                    '.$footer.'
		';
		$content = $subcontent;
	}
}
?>

<div id="subcontent"><?php
echo $content;?>
</div>
