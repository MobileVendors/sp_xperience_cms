<?php
include('../inc/conn.php');
$id = clean($_GET['id']);
$eqry = sql_query("SELECT name FROM fyp_articles WHERE id = '".$id."'");
$erow = sql_fetch($eqry);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Adding a video for article "<?php
echo $erow['name']; ?>"</title>
<link href="../css/global.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/forms.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php
if (isset($_GET['go'])) {
	$contentvalue	= clean($_POST['cvalue']);
	$caption		= clean($_POST['caption']);
	$caption		= urlencode($_POST['caption']);
	
	if(strtolower(substr($contentvalue, 0, 4)) != 'http')
		$contentvalue = 'http://'.$contentvalue;
	
	$youtubepos = strpos(strtolower($contentvalue), 'youtube');
	$contentvaluepos = strpos($contentvalue, '#');
	if($youtubepos !== false){
		if($contentvaluepos !== false){
			if($youtubepos < $contentvaluepos)
				$contentvalue = substr($contentvalue, 0, $contentvaluepos+1);
		}
	}
	
	sql_query("INSERT INTO fyp_content (contentparent, contenttitle, contenttype, contentvalue, textexcerpt, defaultimg, modified) VALUES ('".$id."', '".$caption."', '2', '".$contentvalue."', '0', '0', '".date('Y-m-d H:i:s')."')");
	$content = '
	<h2>Video link added</h2>
	This window will close in 5 seconds..
	<script language="javascript" type="text/javascript">
	setTimeout(window.close, 5000)
	</script>
	';
} else {
	$content = '
		<h2>Add a video</h2>
		<form action="?id='.$id.'&amp;go" method="post">
		<input type="text" name="caption"  placeholder="Caption for this video" />
		<input type="text" name="cvalue"  placeholder="Youtube Link" />
		<button type="submit">Add Video</button>
		</form>
		';
}
echo $content;
?>
</body>
</html>