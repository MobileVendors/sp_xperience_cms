<?php
//include('pages/secure.php');
if ($srow['diplomas'] == 0) {
	$content = '
	<h2>Hrm</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Super Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
		$diplomaname = clean($_POST['diplomaname']);
		sql_query("INSERT INTO fyp_diplomas (diplomaname) VALUES ('".$diplomaname."')");
		header('location: ?page=diplomas');
		} else {
		$content = '
		<h2>Add a Diploma</h2>
		<form action="?page=diplomas&amp;add&amp;go" method="post">
		<input type="text" name="diplomaname" id="diplomaname" placeholder="Diploma Name (255 Characters)" />
		<button type="submit">Add Diploma</button>
		</form>
		';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$diplomaname = clean($_POST['diplomaname']);
			$qry = sql_query("UPDATE fyp_diplomas SET diplomaname = '".$diplomaname."' WHERE id = '".$id."' ");
			header('location: ?page=diploma');
		} else {
			$qry = sql_query("SELECT id, diplomaname FROM fyp_diplomas WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Edit a Diploma</h2>
			<form action="?page=diplomas&amp;edit='.$id.'&amp;go" method="post">
			<input type="text" name="diplomaname" id="diplomaname" placeholder="Diploma Name (255 Characters)" value="'.$row['diplomaname'].'" />
			<button type="submit">Update Diploma</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$diplomaname = clean($_POST['diplomaname']);
			$qry = sql_query("DELETE FROM fyp_diplomas WHERE id = '".$id."' ");
			header('location: ?page=diplomas');
		} else {
			$qry = sql_query("SELECT id, diplomaname FROM fyp_diplomas WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Diploma "'.$row['diplomaname'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=diplomas&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=diplomas">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2>List Diplomas</h2>
			<table>
			<tr>
				<th id="table_main">Diploma Name</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, diplomaname FROM fyp_diplomas ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Diplomas on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['diplomaname'].'</td>
						<td class="td-actions">
							<a href="?page=diplomas&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Diploma" /></a> 
							<a href="?page=diplomas&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Diploma" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
