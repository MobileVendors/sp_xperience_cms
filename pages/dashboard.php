<div id="subcontent">
<h2>Dashboard</h2>
<p>Welcome to your dashboard, <strong><?php echo $_SESSION['name']; ?></strong>.</p>
<h3>Permissions</h3>
<p>You can:</p>
<ul>
<?php
	echo ($srow['schdips'] == 1 ? '<li>Manage Schools / Diplomas</li>' : '');
	echo ($srow['categories'] == 1 ? '<li>Manage Categories</li>' : '');
	echo ($srow['magazines'] == 1 ? '<li>Manage Magazines</li>' : '');
	echo ($srow['articles'] == 1 ? '<li>Manage Articles</li>' : '');
	echo ($srow['users'] == 1 ? '<li>Manage Users</li>' : '');
	echo ($srow['groups'] == 1 ? '<li>Manage Groups</li>' : '');
?>
</ul>
<h3>Tools</h3>
<ul>
<?php echo ($srow['users'] == 1 ? '<li><a href="?page=logging">View Administration Logs</a></li>' : ''); ?>
	<li><a href="xml/xml_mags.php">View Magazine Categories XML</a></li>
	<li><a href="xml/xml_cats.php">View All (Schools /  Categories) XML</a></li>
	<li><a href="xml/xml_templates.php">View All Templates XML</a></li>
</ul>
</div>
