<?php
if ($srow['magazines'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
		$name = clean($_POST['name']);
		sql_query("INSERT INTO fyp_magcats (name) VALUES ('".$name."')");
		log_action('Added Magazine Category: '.$name.'');
		header('location: ?page=magcategories');
		} else {
		$content = '
		<h2>Add a Magazine Category</h2>
		<form action="?page=magcategories&amp;add&amp;go" method="post">
		<input type="text" name="name" id="name" placeholder="Magazine Category Name (255 Characters)" />
		<button type="submit">Add Magazine Category</button>
		</form>
		';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$descr = clean($_POST['descr']);
			$exc = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_magcats SET name = '".$name."' WHERE id = '".$id."' ");
			log_action('Updated Magazine Category: '.$old['name'].' to '.$name.'');
			header('location: ?page=magcategories');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Edit a Magazine Category</h2>
			<form action="?page=magcategories&amp;edit='.$id.'&amp;go" method="post">
			<input type="text" name="name" id="name" placeholder="Magazine Category Name (255 Characters)" value="'.$row['name'].'" />
			<button type="submit">Update Magazine Category</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$exc = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("DELETE FROM fyp_magcats WHERE id = '".$id."' ");
			log_action('Deleted Magazine Category: '.$old['name'].'');
			header('location: ?page=magcategories');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_magcats WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete Magazine Category "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=magcategories&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=magcategories">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2><a href="?page=magcategories&amp;add" class="fr">Add Category</a>Magazine Categories</h2>
			<table>
			<tr>
				<th id="table_main">Magazine Category Name</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, name FROM fyp_magcats ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Categories on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['name'].'</td>
						<td class="td-actions">
							<a href="?page=magcategories&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit Magazine Category" /></a> 
							<a href="?page=magcategories&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete Magazine Category" /></a>
							<a href="xml/xml_singlecat.php?category='.$row['id'].'"><img src="images/star.png" title="View Category XML" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
