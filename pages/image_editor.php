<?php
include('../inc/conn.php');
if (file_exists('brb.php')) {
	include('brb.php');
	die();
}

if(isset($_GET['id'])) {
    $id = clean($_GET['id']);
    $q = sql_query("SELECT contentvalue FROM fyp_content WHERE id = $id");
    $row = sql_fetch($q);
    $image = HOST_URL . $row['contentvalue'];
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Image Editor</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
<script src="/plugins/editlive/webfolder/redistributables/editlivejava/editlivejava.js" language="JavaScript"></script>
</head>

<body>
 <script type="text/javascript">
    $(function() {
        initEditLive();
    });        
        function initEditLive() {
            editlive = new EditLiveJava("ELApplet", "100%", "100%");
            editlive.setOnInitComplete(loadImage);
            editlive.setConfigurationFile("/plugins/editlive/webfolder/redistributables/editlivejava/image_editing.xml");
            editlive.addEditableSection('edited_image');
        }

        function loadImage() {
            editlive.setBody("<img src='<?php echo $image?>' title='Click image to edit' />");
        }
</script>
    <form name='editor' method='POST' action="http://spmag.dev/pages/php_postacceptor.php">
        <div id="edited_image" style="height:400px"></div>
        <input type="submit" value="Save"></input>
    </form>
</body>
</html>
