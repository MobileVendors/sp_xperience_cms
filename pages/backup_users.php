<?php
//include('pages/secure.php');
if ($srow['users'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
			$username	= clean($_POST['username']);
			$usermail	= clean($_POST['usermail']);
			$userpass	= md5($_POST['userpass']);
			$schdip		= isset($_POST['schdip']) ? clean($_POST['schdip']) : '0';
			$categories	= isset($_POST['categories']) ? clean($_POST['categories']) : '0';
			$magazines	= isset($_POST['magazines']) ? clean($_POST['magazines']) : '0';
			$articles	= isset($_POST['articles']) ? clean($_POST['articles']) : '0';
			$users		= isset($_POST['articles']) ? clean($_POST['users']) : '0';
			sql_query("INSERT INTO fyp_users 
			(usergroup, username, usermail, userpass, userhash, schdips, categories, magazines, articles, users) VALUES 
			('1', '".$username."', '".$usermail."', '".$userpass."', '0', '".$schdip."', '".$categories."', '".$magazines."', '".$articles."', '".$users."')");
			log_action('Added User: '.$username.'');
			header('location: ?page=users');
		} else {
			$perm = array(
						'schdip',
						'categories',
						'magazines',
						'articles',
						'users'
						);
			$nice = array(
						'Schools / Diplomas',
						'Categories',
						'Magazines',
						'Articles',
						'Users'
						);
			$count = count($perm);
			$opt = '
			<h3>Permissions</h3>
			<div class="wrapper">
			';
			for ($i = 0; $i < $count; $i++) {
				$opt .= '
					<label class="normal"><input type="checkbox" name="'.$perm[$i].'" id="'.$perm[$i].'" value="1" /> Can Manage '.$nice[$i].'</label>
				';
			}
			$opt .= '
			</div>
			';
			$content = '
			<h2>Add a User</h2>
			<form action="?page=users&amp;add&amp;go" method="post">
			<label for="username">Username</label><input type="text" name="username" id="username" placeholder="255 Characters maximum" />
			<label for="usermail">Email Address</label><input type="text" name="usermail" id="usermail" placeholder="255 Characters maximum" />
			<label for="userpass">Password</label><input type="text" name="userpass" id="userpass" placeholder="255 Characters maximum" />
			'.$opt.'
			<button type="submit">Add User</button>
			</form>
			';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$username	= clean($_POST['username']);
			$usermail	= clean($_POST['usermail']);
			$userpass	= clean($_POST['userpass']);
			$schdip		= isset($_POST['schdip']) ? clean($_POST['schdip']) : '0';
			$categories	= isset($_POST['categories']) ? clean($_POST['categories']) : '0';
			$magazines	= isset($_POST['magazines']) ? clean($_POST['magazines']) : '0';
			$articles	= isset($_POST['articles']) ? clean($_POST['articles']) : '0';
			$users		= isset($_POST['articles']) ? clean($_POST['users']) : '0';
			if (empty($userpass)) {
				$userupdate = '';
			} else {
				$userupdate = "userpass = '".md5($userpass)."',";
			}
			$qry = sql_query("UPDATE fyp_users SET 
			schdips = '".$schdip."', 
			categories = '".$categories."', 
			magazines = '".$magazines."', 
			articles = '".$articles."',
			users = '".$users."',
			username = '".$username."', 
			".$userupdate." 
			usermail = '".$usermail."'
			WHERE id = '".$id."' ");
			log_action('Updated User: '.$username.' (User#'.$id.')');
			header('location: ?page=users');
		} else {
			$qry = sql_query("SELECT usergroup, username, usermail, userpass, schdips, categories, magazines, articles, users FROM fyp_users WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$perm = array(
						'schdips',
						'categories',
						'magazines',
						'articles',
						'users'
						);
			$nice = array(
						'Schools / Diplomas',
						'Categories',
						'Magazines',
						'Articles',
						'Users'
						);
			$count = count($perm);
			$opt = '
			<h3>Permissions</h3>
			<div class="wrapper">
			';
			for ($i = 0; $i < $count; $i++) {
				if ($row[$perm[$i]] == 1) {
						$tick = ' checked="checked"';
				} else {
						$tick = '';
				}
				$opt .= '
					<label class="normal"><input'.$tick.' type="checkbox" name="'.$perm[$i].'" id="'.$perm[$i].'" value="1" /> Can Manage '.$nice[$i].'</label>
				';
			}
			$opt .= '
			</div>
			';
			$content = '
			<h2>Edit a User</h2>
			<form action="?page=users&amp;edit='.$id.'&amp;go" method="post">
			<label for="username">Username</label><input type="text" name="username" id="username" placeholder="255 Characters maximum" value="'.$row['username'].'" />
			<label for="usermail">Email Address</label><input type="text" name="usermail" id="usermail" placeholder="255 Characters maximum" value="'.$row['usermail'].'" />
			<label for="userpass">Password</label><input type="text" name="userpass" id="userpass" placeholder="255 Characters maximum. Blank for no change." />
			'.$opt.'
			<button type="submit">Edit User</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$exc = sql_query("SELECT id, username FROM fyp_users WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("DELETE FROM fyp_users WHERE id = '".$id."' ");
			log_action('Deleted User: '.$old['username'].' (User#'.$old['id'].')');
			header('location: ?page=users');
		} else {
			$qry = sql_query("SELECT id, username FROM fyp_users WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete User "'.$row['username'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=users&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=users">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
			<h2>List Users</h2>
			<table>
			<tr>
				<th id="table_main">Username</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, usergroup, username FROM fyp_users WHERE username != '".$_SESSION['name']."' ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Users (Other than you) on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$gqry	= sql_query("SELECT name FROM fyp_groups WHERE id = '".$row['usergroup']."' ");
				$grow	= sql_fetch($gqry);
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td><span class="fr"><em>'.$grow['name'].'</em></span>'.$row['username'].'</td>
						<td class="td-actions">
							<a href="?page=users&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit User" /></a> 
							<a href="?page=users&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete User" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
	echo $content;
?>
</div>
