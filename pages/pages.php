<?php
//include('pages/secure.php');
if (isset($_GET['content'])) {
		$id 	= clean($_GET['content']);
		$qry	= sql_query("SELECT template FROM fyp_pages WHERE id = ".$id);
		$row	= sql_fetch($qry);
		$qry2	= sql_query("SELECT type, slots, preview FROM fyp_templates WHERE id = ".$row['template']);
		$num	= sql_fetch($qry2);
		$qry3	= sql_query("SELECT magid FROM fyp_magpages WHERE pageid = '".$id."'");
		$row3	= sql_fetch($qry3);
		$chk	= sql_query("SELECT id FROM fyp_articlepages WHERE pageid = '".$id."'");
		$cnum	= sql_num_rows($chk);
		
		if (isset($_GET['go'])) {

			// Template Slot
			for($i = 1; $i <= $num['slots']; $i++) {
				$article = clean($_POST['slot'.$i]);
				sql_query("UPDATE fyp_articlepages SET articleid = '".$article."', modified = '".date('Y-m-d H:i:s')."' WHERE pageid = '".$id."' AND articleslot = '".$i."'");
			}
			
			header('location: ?page=magazines&pages='.$row3['magid'].'');
		} else {
			$tmp = '';
            for($i = 1; $i <= $num['slots']; $i++) {

            	$cat_select = '<option value="" title="Select Category">Select Category</option>'; 
            	
                $cat_select .= '<optgroup label="Category">';
                $cat_list_run = sql_query("SELECT id, name FROM fyp_cats ORDER BY id ASC");
                while ($cat_list_row = sql_fetch($cat_list_run)) {
                    $cat_select .= '
                        <option value="cat_'.$cat_list_row['id'].'_'.$i.'" title="'.$cat_list_row['name'].'">'.$cat_list_row['name'].'</option>
                    ';
                }
                $cat_select .= '</optgtoup>';
                $sch_select = '<optgroup label="Schools / Diplomas">';
                $sch_list_run = sql_query("SELECT id, name FROM fyp_schdips ORDER BY id ASC");
                while ($sch_list_row = sql_fetch($sch_list_run)) {
                    $sch_select .= '
                        <option value="dip_'.$sch_list_row['id'].'_'.$i.'" title="'.$sch_list_row['name'].'">'.$sch_list_row['name'].'</option>
                    ';
                }
                $sch_select .= '</optgtoup>';
                $select = $cat_select.$sch_select;
                //
                $artquery = sql_query("SELECT articleid FROM fyp_articlepages WHERE pageid = '".$id."' AND articleslot = '".$i."'");
                if (sql_num_rows($artquery) == 1) {
                    $artrow = sql_fetch($artquery);
                    $namequery = sql_query("SELECT id, name FROM fyp_articles WHERE id = '".$artrow['articleid']."'");
                    $namerow = sql_fetch($namequery);
                    $artname = 'Current Article: '.$namerow['name'];
                    $xid    = $namerow['id'];
                } else {
                    $artname = 'Select Category from dropdown on the left.';
                    $xid    = '0';
                }
                $tmp .= '
                    <label class="select" for="slot'.$i.'">Article for Slot '.$i.'</label>
                    <select class="catselector catnumber_'.$i.'">
                    '.$select.'
                    </select>
                    <span class="article_'.$i.'">
                    '.snip($artname).'
                    <input type="hidden" value="'.$xid.'" name="slot'.$i.'" id="slot'.$i.'" />
                    </span>
                    <script>
                    $(document).ready(function() {
                        $(\'.catnumber_'.$i.'\').change(function() {
                            var category = $(this).val();
                        		
                        	if(category != ""){
	                        	$.ajax({
	                                type: \'GET\',
	                                url: \'pages/articlechooser.php\',
	                                data: \'cat=\' + category,
	                                dataType: \'html\',
	                                beforeSend: function() {
	                                    $(\'.article_'.$i.'\').html(\'...\');
	                                },
	                                success: function(response) {
	                                    $(\'.article_'.$i.'\').html(response);
	                                }
	                            });		
				            } else
	                                $(\'.article_'.$i.'\').html(\'Current Article: ...\');
                            
                        });
                    });
                    </script>
                    <br />
                ';
            }
			$content = '
			<h2><span class="fr"><a href="?page=articles&amp;add">Create a new Article</a></span>Manage Articles</h2>
			<img style="
			display: block;
			height: 200px;
			padding: 5px; 
			float: right;
			" 
			src="templates/'.$num['type'].'/'.$num['preview'].'" />
			<form action="?page=pages&amp;content='.$id.'&amp;go" method="post">
			'.$tmp.'
			<button type="submit">Update Magazine Articles</button>
			</form>
			';
		}
	} elseif (isset($_GET['add'])) {
		$id = clean($_GET['add']);
		if (isset($_GET['go'])) {
			// Page
			$n	= clean($_POST['name']);
			$p 	= clean($_POST['pagenum']);
			$t 	= clean($_POST['templateid']);
			$qry = sql_query("INSERT INTO fyp_pages (parent, name, template, pagenum, pageorder, modified) VALUES ('".$id."', '".$n."', '".$t."', '".$p."', '".$p."', '".date('Y-m-d H:i:s')."')");
			
			// Magpage
			$pageid = sql_last_id('fyp_pages');
			$qry = sql_query("INSERT INTO fyp_magpages (pageid, magid, modified) VALUES ('".$pageid."', '".$id."', '".date('Y-m-d H:i:s')."')");
			$qry = sql_query("UPDATE fyp_mags SET numpages = numpages + 1, modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."'");
			
			// Article Slots
			$qry2	= sql_query("SELECT slots FROM fyp_templates WHERE id = ".$t);
			$num	= sql_fetch($qry2);
						
			// Last
			for ($i = 1; $i <= $num['slots']; $i++) {
				$qry3	= sql_query("INSERT INTO fyp_articlepages (articleid, pageid, articleslot, modified) VALUES ('0', '".$pageid."', '".$i."', '".date('Y-m-d H:i:s')."')");
			}
			header('location: ?page=pages&content='.$pageid.'');
		} else {
			$qry	= sql_query("SELECT name, numpages FROM fyp_mags WHERE id = '".$id."'");
			$row	= sql_fetch($qry);
			$num	=  $row['numpages'] + 1;
			$content = '
			<h2>Add Page to '.$row['name'].'</h2>
			<form action="?page=pages&amp;add='.$id.'&amp;go" method="post">
			<label for="name">Page Name</label><input type="text" name="name" id="name" />
			<input type="hidden" name="pagenum" value="'.$num.'" />
			';
			$qry = sql_query("SELECT id, type, name, slots, preview FROM fyp_templates");
			while ($row = sql_fetch($qry)) {
				$content .= '
					<label title="templates/'.$row['type'].'/'.$row['preview'].'" class="template">
					<strong>'.$row['name'].'</strong>
					<img src="templates/'.$row['type'].'/'.$row['preview'].'" alt="" />
					<input type="radio" name="templateid" value="'.$row['id'].'" />
					</label>
				';
			}
			$content .= '
			<button type="submit" style="clear: both; display: block;">Add Page</button>
			</form>
			';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$n	= clean($_POST['name']);
			$t 	= clean($_POST['templateid']);

			$template_qry = sql_query("SELECT template FROM fyp_pages WHERE id = '".$id."' ");
			$template_row = sql_fetch($template_qry);
			
			$template_update = '';
			if($t != $template_row['template'] && !isset($_POST['no_template_change']))
				$template_update = ", template = '".$t."'";

			sql_query("UPDATE fyp_pages SET name = '".$n."' $template_update, modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."'");

			if($t != $template_row['template'] && !isset($_POST['no_template_change'])){
				sql_query("UPDATE fyp_articlepages SET modified = '".date('Y-m-d H:i:s')."' ");
				sql_query("DELETE FROM fyp_articlepages WHERE pageid = '".$id."'");
			
				// Article Slots
				$qry2	= sql_query("SELECT slots FROM fyp_templates WHERE id = ".$t);
				$num	= sql_fetch($qry2);
						
				// Last
				for ($i = 1; $i <= $num['slots']; $i++) {
					$qry3	= sql_query("INSERT INTO fyp_articlepages (articleid, pageid, articleslot, modified) VALUES ('0', '".$id."', '".$i."', '".date('Y-m-d H:i:s')."')");
				}
			}
			header('location: ?page=pages&content='.$id.'');
		} else {
			$eqry = sql_query("SELECT name, template FROM fyp_pages WHERE id = '".$id."' ");
			$erow = sql_fetch($eqry);
			$content = '
			<h2>Editing "'.$erow['name'].'"</h2>
			<form action="?page=pages&amp;edit='.$id.'&amp;go" method="post">
			<label for="name">Page Name</label><input type="text" name="name" id="name" value="'.$erow['name'].'" />
			
			';
			$qry = sql_query("SELECT id, type, name, slots, preview FROM fyp_templates");
			while ($row = sql_fetch($qry)) {
				if ($erow['template'] == $row['id']) {
					$checked = ' checked="checked"';
				} else {
					$checked = '';
				}
				$content .= '
					<label title="templates/'.$row['type'].'/'.$row['preview'].'" class="template">
					<strong>'.$row['name'].'</strong>
					<img src="templates/'.$row['type'].'/'.$row['preview'].'" alt="" />
					<input type="radio" name="templateid" value="'.$row['id'].'"'.$checked.' />
					</label>
				';
			}
			$content .= '
			<button type="submit" style="clear: both; display: inline-block;">Edit Page</button>
			<button type="submit" name="no_template_change" style="clear: both; display: inline-block;">No Template Change</button>
			</form>
			';
		}
        } elseif (isset($_GET['title'])) {
		$id = clean($_GET['title']);
		$run = sql_query("SELECT parent, name, title FROM fyp_pages WHERE id = '".$id."' ");
		$row = sql_fetch($run);
                if (isset($_GET['go'])) {
                    $n	= clean($_POST['name']);
                    $title = clean($_POST['title']);
                    $sql = "UPDATE fyp_pages SET name ='$n', title='$title', modified = '".date('Y-m-d H:i:s')."' WHERE id = $id";
		    sql_query($sql);
		    header('location: ?page=magazines&pages='.$row['parent'].'');
		} else {
			$content = '
			<h2>Editing Page "'.$row['name'].'"</h2>
			<form action="?page=pages&amp;title='.$id.'&amp;go" method="post">
                        <label for="name">Name</label><input type="text" name="name" id="name" value="'.$row['name'].'" />
			<label for="title">Title</label><input type="text" name="title" id="title" value="'.$row['title'].'" />
			<button type="submit" style="clear: both; display: block;">Edit Page</button>
			</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
                $id = clean($_GET['delete']);
                $mag_id = clean($_GET['mag_id']);
                if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$qry = sql_query("UPDATE fyp_pages SET modified = '".date('Y-m-d H:i:s')."' ");
                        $qry = sql_query("DELETE FROM fyp_pages WHERE id = '".$id."' ");
			header('location: ?page=magazines&pages='.$mag_id);
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_pages WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			Delete Article "'.$row['name'].'"? Action cannot be reversed! <br />
			<a href="?page=pages&amp;delete='.$id.'&mag_id='.$mag_id.'&amp;go">Yes</a> | <a href="javascript:history.back();">No</a>


			';
		}
	} else {
		$content = '
		<h2>Error</h2>
		<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
		';
	}
?>

<div id="subcontent">
<?php
echo $content;?>
</div>
