<?php
if ($srow['schdips'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['add'])) {
		if (isset($_GET['go'])) {
		$name = clean($_POST['name']);
		$email = clean($_POST['email']);
		sql_query("INSERT INTO fyp_schdips (name, email, modified) VALUES ('".$name."', '".$email."', '".date('Y-m-d H:i:s')."')");
		log_action('Added School / Diploma: '.$name.'/'.$email.'');
		header('location: ?page=schdip');
		} else {
		$content = '
		<h2>Add a School / Diploma</h2>
		<form action="?page=schdip&amp;add&amp;go" method="post">
		<label for="name">School / Diploma Name</label><input type="text" name="name" id="name" placeholder="255 Characters maximum" />
		<label for="email">School / Diploma Address</label><input type="text" name="email" id="email" placeholder="255 Characters maximum" />
		<button type="submit">Add School / Diploma</button>
		</form>
		';
		}
	} elseif (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$email = clean($_POST['email']);
			$exc = sql_query("SELECT name, email FROM fyp_schdips WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			$qry = sql_query("UPDATE fyp_schdips SET name = '".$name."', email = '".$email."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
			log_action('Updated School / Diploma: '.$old['name'].'/'.$old['email'].' to '.$name.'/'.$email.'');
			header('location: ?page=schdip');
		} else {
			$qry = sql_query("SELECT id, name, email FROM fyp_schdips WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
		<h2>Edit a School / Diploma</h2>
		<form action="?page=schdip&amp;edit='.$id.'&amp;go" method="post">
		<label for="name">School / Diploma Name</label><input type="text" name="name" id="name" placeholder="255 Characters maximum" value="'.$row['name'].'" />
		<label for="email">School / Diploma Address</label><input type="text" name="email" id="email" placeholder="255 Characters maximum" value="'.$row['email'].'" />
		<button type="submit">Update School / Diploma</button>
		</form>
			';
		}
	} elseif (isset($_GET['delete'])) {
		$id = clean($_GET['delete']);
		if (isset($_GET['go'])) {
			$name = clean($_POST['name']);
			$exc = sql_query("SELECT name, email FROM fyp_schdips WHERE id = '".$id."' ");
			$old = sql_fetch($exc);
			sql_query("UPDATE fyp_schdips SET modified = '".date('Y-m-d H:i:s')."' ");
			$qry = sql_query("DELETE FROM fyp_schdips WHERE id = '".$id."' ");
			log_action('Deleted School / Diploma: '.$old['name'].'/'.$old['email'].'');
			header('location: ?page=schdip');
		} else {
			$qry = sql_query("SELECT id, name FROM fyp_schdips WHERE id = '".$id."' ");
			$row = sql_fetch($qry);
			$content = '
			<h2>Confirm Deletion</h2>
			<p>
			Delete School "'.$row['name'].'"? Action cannot be reversed! <br />
			<button onclick="location.href=\'?page=schdip&amp;delete='.$id.'&amp;go\';" class="normal">Confirm</button> <a href="?page=schdip">Cancel</a>
			<p>
			';
		}
	} else {
		$subcontent = '
		<h2>List Schools / Diplomas</h2>
			<table>
			<tr>
				<th id="table_main">School / Diploma Name</th>
				<th>Actions</th>
			</tr>
		';
		$qry	= sql_query("SELECT id, name FROM fyp_schdips ORDER BY id DESC");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				<tr>
					<td colspan="2">There are currently no Schools / Diplomas on record.</td>
				</tr>
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				$subcontent .= '
					<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
						<td>'.$row['name'].'</td>
						<td class="td-actions">
							<a href="?page=schdip&amp;edit='.$row['id'].'"><img src="images/edit.png" title="Edit School" /></a> 
							<a href="?page=schdip&amp;delete='.$row['id'].'"><img src="images/delete.png" title="Delete School" /></a>
						</td>
					</tr>
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
	echo $content;
?>
</div>