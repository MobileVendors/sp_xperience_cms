<?php
if ($srow['users'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	$action_tool	= '<a href="?page=logging&amp;by=executed&amp;dir=ASC"><img class="icon" src="images/dir_asc.png" /></a><a href="?page=logging&amp;by=executed&amp;dir=DESC"><img class="icon" src="images/dir_desc.png" /></a>';
	$action_sub		= '
	<a href="?page=logging"><img class="icon" src="images/log_all.png" /></a>
	<a href="?page=logging&amp;view=deleted"><img class="icon" src="images/log_deleted.png" /></a>
	<a href="?page=logging&amp;view=added"><img class="icon" src="images/log_added.png" /></a>
	<a href="?page=logging&amp;view=updated"><img class="icon" src="images/log_updated.png" /></a>
	';
	$user_tool		= '<a href="?page=logging&amp;by=actor&amp;dir=ASC"><img class="icon" src="images/dir_asc.png" /></a><a href="?page=logging&amp;by=actor&amp;dir=DESC"><img class="icon" src="images/dir_desc.png" /></a>';
	$data_tool		= '<a href="?page=logging&amp;by=timedate&amp;dir=ASC"><img class="icon" src="images/dir_asc.png" /></a><a href="?page=logging&amp;by=timedate&amp;dir=DESC"><img class="icon" src="images/dir_desc.png" />';
	$content = '
	<h2><span class="fr"><a href="?page=logging&amp;purge">Purge Logs (No confirmation)</a></span>Administration Logs (Last 25)</h2>
		<table>
		<tr>
			<th id="table_main"><span class="fr">'.$action_sub.'</span>Action '.$action_tool.'</th>
			<th>User '.$user_tool.'</th>
			<th>Date '.$data_tool.'</th>
		</tr>
	';
	$views = array('added', 'deleted', 'updated');
	if (isset($_GET['view']) && in_array($_GET['view'], $views)) {
		$view	= $_GET['view'];
		$qry	= sql_query("SELECT id, executed, actor, timedate FROM fyp_logs WHERE executed LIKE '%".$view."%' ORDER BY id DESC");
	} else {
		$by 	= array('id', 'executed', 'actor', 'timedate');
		$dir	= array('ASC', 'DESC');
		if (isset($_GET['by']) && in_array($_GET['by'], $by)) {
			$what = $_GET['by'];
		} else {
			$what = 'timedate';
		}
		if (isset($_GET['dir']) && in_array($_GET['dir'], $dir)) {
			$direction = $_GET['dir'];
		} else {
			$direction = 'DESC';
		}
		$qry	= sql_query("SELECT id, executed, actor, timedate FROM fyp_logs ORDER BY ".$what." ".$direction."");
	}
	if (isset($_GET['purge'])) {
		sql_query("DELETE FROM fyp_logs");
		header('location: ?page=logging');
	}
	if (sql_num_rows($qry) == 0) {
		$content .= '
			<tr>
				<td colspan="3">There are currently no Administration Logs on record.</td>
			</tr>
		';
	} else {
		$mod = 0;
		while($row = sql_fetch($qry)) {
			$check = explode(' ', $row['executed']);
			if ($check[0] == 'Added') {
				$img = '<img class="icon" src="images/log_added.png" />';
			} elseif ($check[0] == 'Updated') {
				$img = '<img class="icon" src="images/log_updated.png" />';
			} elseif ($check[0] == 'Deleted') {
				$img = '<img class="icon" src="images/log_deleted.png" />';
			} else {
				$img = '<img class="icon" src="images/log_default.png" />';
			}
			$content .= '
				<tr'.($mod % 2 ? '' : ' class="table_alt"').'>
					<td><span class="fr">'.$img.'</span>'.$row['executed'].'</td>
					<td class="td-actions">'.$row['actor'].'</td>
					<td class="td-actions">'.format_date($row['timedate']).'</a> </td>
				</tr>
			';
			$mod++;
		}
	}
	$content	.= '
		</table>
	';
}
?>
<div id="subcontent"><?php
echo $content;?>
</div>
