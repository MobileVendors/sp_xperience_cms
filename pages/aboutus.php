<?php
//include('pages/secure.php');
if ($srow['users'] == 0) {
	$content = '
	<h2>Error</h2>
	<p>This page is off limits to you. If you feel that this is an error, please contact your Administrator.</p>
	';
} else {
	if (isset($_GET['edit'])) {
		$id = clean($_GET['edit']);
		if (isset($_POST['publish']) || isset($_POST['save']) || isset($_POST['test'])) {
			$info	= clean($_POST['info']);
			$assignedMedia	= clean($_POST['assignedMedia']);
			
			$info = mysqli_real_escape_string($dbLink, str_replace("\\r\\n", "", $info));
			
			sql_query("UPDATE fyp_aboutus SET info = '".$info."', modified = '".date('Y-m-d H:i:s')."'");
			log_action('Updated About Us: '.$id.'');
			
			if($assignedMedia == 'Select image from gallery'){
				$filesize = filesize($_FILES['file']['tmp_name']);
				
				if(!empty($filesize)){
					$type	= explode('/', $_FILES['file']['type']);
					
					if(!in_array($type[0], array('video', 'image')))
						$content = 'Filetype invalid. Please upload image or video format.';
					else {
				$isVideo = false;
				if(strpos($_FILES['file']['type'], 'video') !== false)
					$isVideo = true;
					
				$saveFlag = false;
						
				if($isVideo){
					if ($filesize <= 10000000)
						$saveFlag = true;
							else
								$content = 'Video size must not exceed 10MB';
				} else {
					list($width, $height, $type) = getimagesize($_FILES['file']['tmp_name']);
					if ($width <= 800 && $height <= 500 && $filesize <= 512000)
						$saveFlag = true;
							else
								$content = 'Image must not exceed maximum dimension of 800x500 and 512Kb filesize';
				}
					
				if($saveFlag){
					$destpath = 'videos/';
					if(!$isVideo)
						$destpath = 'covers/';
					$type	= explode('/', $_FILES['file']['type']);
					$source = $_FILES['file']['tmp_name'];
					$filename = rand(1,5).time().'-'.$_FILES['file']['name'];
					$filetype = $_FILES['file']['type'];
					move_uploaded_file($source, $destpath.$filename);
				
					if(!$isVideo)
						thumbify($destpath.$filename);
				
					$qry = sql_query("UPDATE fyp_aboutus SET media = '".$destpath.$filename."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
							header('location: ?page=aboutus');
						}
				}
				} else 
					header('location: ?page=aboutus');
				
			} else {
				//use the original if exists
				$assignedMediaTemp = str_replace('thumbs/', '', $assignedMedia);
				if(file_exists($assignedMediaTemp))
					$assignedMedia = $assignedMediaTemp;
					
				$qry = sql_query("UPDATE fyp_aboutus SET media = '".$assignedMedia."', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
				header('location: ?page=aboutus');
			}
			
			
		} else if (isset($_GET['removeImage'])){
			$qry = sql_query("UPDATE fyp_aboutus SET media = '', modified = '".date('Y-m-d H:i:s')."' WHERE id = '".$id."' ");
			log_action('Updated About Us: '.$id.'');
			
			header('location: ?page=aboutus&edit=1');
		} else {
			$qry = sql_query("SELECT * FROM fyp_aboutus WHERE id = '".$id."' ");
            $erow = sql_fetch($qry);
			
            $img = $erow['media'];
            chmod(ROOT.$img, 0777);
            $url = "/plugins/phpimageeditor/index.php?imagesrc=$img";
            if($mode != 'development') {
            	$url = "/$prefix/plugins/phpimageeditor/index.php?imagesrc=$img";
            }
            
            $filetype = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $img);
            
            $editImg = '';
            $removeImg = '';
            $previewImg = '';
            if(!empty($img)){
            	$removeImg = '<a href="javascript:clearAssignedImage();"><img src="images/cancel-on.png" title="Remove Current Assigned Image" /></a>';
            	$previewImg = '<a href="javascript:popup(\''.$erow['media'].'\')"><img src="images/view.png" title="View Image/Video" /></a>';
            	
            if(strpos($filetype, 'video') === false)
            $editImg = '<a href="javascript:popupeEditor(\''.$url.'\')"><img src="images/edit.png" title="Edit About Us Image/Video" /></a>';
            		
            }
            
			$content = '
			<h2>Editing About Us</h2>
			<form action="?page=aboutus&amp;edit='.$id.'&amp;go" method="post" enctype="multipart/form-data">
			<div class="upload" style="vertical-align: top">
				<table>
					<tr>
						<td style="width: 200px;">
							<label for="file">Image/Video</label>
						</td>
						<td>
							'.$removeImg.'
							'.$editImg.'
							'.$previewImg.'</label>
				<input type="file" name="file" id="file" class="realupload" onchange="this.form.fakeupload.value = this.value;" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
				<a href="javascript:popup(\'pages/media_files_popup.php?show=all\')"><img src="images/photo_album.png" title="Assign image/video from gallery" /></a>
				<input type="text" name="assignedMedia" id="assignedMedia" value="Select image from gallery" readonly style="display: inline"/>
							<a href="javascript:clearMediaField(\'assignedMedia\', \'Select image from gallery\')"><img src="images/delete.png" title="Clear" /></a>
						</td>
					</tr>
				</table>
			</div>
            <div id="txtmargin"><textarea id="txtarea" name="info" placeholder="Description of Magazine">'.html_entity_decode(stripcslashes($erow['info'])).'</textarea></div>
            <button type="submit" name="publish" class="normal">Save About Us</button>
			</form>
			';
		}
	} else {
		$subcontent = '
		<h2><a href="?page=aboutus&amp;edit=1" class="fr">Edit</a>About Us Management</h2>
		';
		$qry	= sql_query("SELECT media, info FROM fyp_aboutus WHERE id = 1");
		if (sql_num_rows($qry) == 0) {
			$subcontent .= '
				There are currently no About Us info on record.
			';
		} else {
			$mod = 0;
			while($row = sql_fetch($qry)) {
				if(!empty($row['media'])){
					$filetype = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $row['media']);
					
					if(!empty($filetype)){
					if(strpos($filetype, 'video') !== false){
					$subcontent .= '
						<video style="margin: 15px auto; display: block; width: 400px;" controls>
						  <source src="'.$url.''.$row['media'].'" />
						  Your browser does not support the video tag.
						</video>
					';
					} else {
					$subcontent .= '
						<img style="margin: 15px auto; display: block;" src="'.$url.''.$row['media'].'">
					';
					}
				}
				}
				
				$subcontent .= '<br>
				'.html_entity_decode(stripcslashes($row['info'])).'
				';
				$mod++;
			}
		}
		$subcontent	.= '
			</table>
		';
		$content = $subcontent;
	}
}
?>
<div id="subcontent">
<?php
echo $content;?>
</div>
<script type="text/javascript">
function clearAssignedImage(){
	var confirmRemove = confirm("Do you want to remove currently assigned image/video?");
	if (confirmRemove == true){
		var act = $('form').get(0).getAttribute('action');
		$('form').get(0).setAttribute('action', act+"&removeImage");
		$('form').submit();
	}
}

function popupeEditor(url){
    leftPos = (screen.width / 2) - 501
    topPos = (screen.height / 2) - 222
    mywindow = window.open(url, "_blank", "location=0, status=0, scrollbars=1, width=1024, height=800");
    mywindow.moveTo(leftPos, topPos);
}

function HandleAssignedMediaFromPopupResult(result){
	$('#assignedMedia').val(result);
	var control = $("#file");
	control.replaceWith( control = control.clone( true ) );
}

function clearMediaField(fieldName, defaultValue){
	var r = confirm("Do you want to reset image/video?");
	if (r == true){
		$('#'+fieldName).val(defaultValue);
		var control = $("#file");
		control.replaceWith( control = control.clone( true ) );
	}
}

$("document").ready(function(){
    $("#file").change(function() {
    	$('#assignedMedia').val('Select image from gallery');
    });
});
</script>
<style>
div.upload table td { 
	border: 0px solid white !important;
}
div.upload table td img {
	vertical-align: middle
}
</style>
