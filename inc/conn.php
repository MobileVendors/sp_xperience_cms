<?php
	ob_start();
	session_start();
	$script = dirname(__FILE__);
	include($script.'/basics.php');
	include($script.'/config.php');
	$dbLink = mysqli_connect($dbHost, $dbUser, $dbPass);
	if (!$dbLink) {
		die("Database Error: Unable to connect to MySQL Server.");
	}
	mysqli_select_db($dbLink, $dbName) or die("Database Error: Unable to open Database.");
	include($script.'/core.php');
	include($script.'/functions.php');
?>
