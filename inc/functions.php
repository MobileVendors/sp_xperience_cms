<?php
// functions.php
function show_page($id ,$pageorder) {
	global $tmp;
	$query1 = sql_query("SELECT id, name, pagenum, template FROM fyp_pages WHERE parent = '".$id."' AND pageorder = '".$pageorder."'");
	$row1 = sql_fetch($query1);
	$query2 = sql_query("SELECT articleid FROM fyp_articlepages WHERE pageid = '".$row1['id']."' ORDER BY articleslot");
	while ($row2 = sql_fetch($query2)) {
		$query3 = sql_query("SELECT name, subname, sharelink FROM fyp_articles WHERE id = '".$row2['articleid']."'");
		$row3	= sql_fetch($query3);
		$query4	= sql_query("SELECT contenttitle, contenttype, textexcerpt, contentvalue FROM fyp_content WHERE contentparent = '".$row2['articleid']."' ORDER BY defaultimg");
		$med	= '';
		$cap	= '';
		$num	= 1;
		while ($row4	= sql_fetch($query4)) {
			if ($row4['contenttype'] != 3) {
				if ($row4['contenttype'] == 1) {
					$med .= '<a><img src="adm'.$row4['contentvalue'].'" /></a>';
					$cap .= '<a href="#" rel="'.$num.'">'.$num.'<span>'.$row4['contenttitle'].'</span></a>';
					$num++;
				} else {
					$yt	= explode('=', $row4['contentvalue']);
					$med .= '<a href="'.$row4['contentvalue'].'" target="_blank">
					<img style="background: url(http://img.youtube.com/vi/'.$yt[1].'/0.jpg) center center no-repeat; width: 500px; height: 282px;" src="images/play.png" /></a>';
					$cap .= '<a href="#" rel="'.$num.'">'.$num.'<span>'.$row4['contenttitle'].'</span></a>';
					$num++;
				}
			} else {
				$text = $row4['textexcerpt'];
			}
		}
		$tmp['media'] = '
			<div class="media_block">
				<div class="main_view">
					<div class="window">	
						<div class="image_reel">
							'.$med.'
						</div>
					</div>
					<div class="paging">
						'.$cap.'
					</div>
				</div>
			</div>
			';
		$tmp['txt'] = '
			<div class="art_title">'.$row3['name'].'</div>
			<div class="art_subtitle">'.$row3['subname'].'</div>
			<div class="art_text">'.$text.'</div>
		';
	}
	include('templates/default.php');
}
function date_picker($id=NULL) {
	if (!empty($id)) {
		$id		= clean($id);
		$sql	= sql_query("SELECT published FROM fyp_mags WHERE id = '".$id."' LIMIT 1");
		$row	= sql_fetch($sql);
		if ($row['published'] == '0000-00-00') {
			$raw	= date('Y-m-d');
		} else {
			$raw	= $row['published'];
		}
	} else {
		$raw	= date('Y-m-d');
	}
	$date		= explode('-', $raw);
	$startyear	= date("Y")-10;
	$endyear	= date("Y")+50; 
	
	$months		= array('','January','February','March','April','May', 'June','July','August', 'September','October','November','December');
	
	// Day dropdown
	$display	= '<select name="inputday" style="display: inline; width: 50px; margin: 0px 5px 0px 0px;">';
	
	for($i=1; $i<=31; $i++) {
		if ($i == $date[2]) {
			$display .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';
		} else { 
			$display .= '<option value="'.$i.'">'.$i.'</option>';
		}
	}
	$display	.='</select>';
	
	// Month dropdown
	$display	.= '<select name="inputmonth" style="display: inline; margin: 0px 5px 0px 0px;">';
	
	for($i=1; $i<=12; $i++) {
		if ($i == $date[1]) {
			$display .= '<option value="'.$i.'" selected="selected">'.$months[$i].'</option>';
		} else { 
			$display .= '<option value="'.$i.'">'.$months[$i].'</option>';
		}
	}
	$display	.='</select>';
	
	// Year dropdown
	$display	.= '<select name="inputyear" style="display: inline; width: 100px; margin: 0px 5px 0px 0px;">';
	
	for($i=$startyear; $i<=$endyear; $i++) {
		if ($i == $date[0]) {
			$display .= '<option value="'.$i.'" selected="selected">'.$i.'</option>';
		} else { 
			$display .= '<option value="'.$i.'">'.$i.'</option>';
		}
	}
	$display	.='</select>';
	
	return '<div style="clear: right;">'.$display.'</div>';
}

function xml_get_timestamp_param(){
	$timestamp = '';
	if(isset($_GET['timestamp']))
		$timestamp = $_GET['timestamp'];
	
	if(!empty($timestamp))
		$timestamp = date('Y-m-d H:i:s', $timestamp);
	
	return $timestamp;
}

function xml_is_show_data($timestamp, $modified){
	return $timestamp < $modified || empty($timestamp);
}

function xml_get_server_time($updateFlag = '0'){
	return '
			<date timestamp="'.time().'" is_updated="'.$updateFlag.'"></date>';
}

function xml_encode_data($string){
	$string = utf8_encode($string);
	$string = htmlspecialchars( html_entity_decode(convertFromCP1252(mb_convert_encoding($string, mb_internal_encoding(), 'UTF-8')), ENT_COMPAT, 'UTF-8'), ENT_COMPAT, 'UTF-8');
	
	return html_entity_decode($string);
}

function convertFromCP1252($string)
{
	$search = array('&',
			'<',
			'>',
			'"',
			'�',
			'�',
			',',
			'�',
			'�',
			chr(212),
			chr(213),
			chr(210),
			chr(211),
			chr(209),
			chr(208),
			chr(201),
			chr(145),
			chr(146),
			chr(147),
			chr(148),
			chr(151),
			chr(150),
			chr(133),
			chr(194)
	);

	$replace = array(  '&amp;',
			'&lt;',
			'&gt;',
			'&quot;',
			'&quot;',
			'&#39;',
			'&#44;',
			'&#8211;',
			'&#8211;',
			'&#8216;',
			'&#8217;',
			'&#8220;',
			'&#8221;',
			'&#8211;',
			'&#8212;',
			'&#8230;',
			'&#8216;',
			'&#8217;',
			'&#8220;',
			'&#8221;',
			'&#8211;',
			'&#8212;',
			'&#8230;',
			''
	);
	
	//return str_replace($search, $replace, $string);
	$string = str_replace($search, $replace, $string);
	
	return iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8", $string);
	
}

?>
