<?php
// core.php
// core functions that is called on by other functions
function snip($text) {
	$chars = 40;
	$text = htmlspecialchars($text, ENT_QUOTES);
	$text = $text.' ';
	$text = substr($text,0,$chars);
	//$text = substr($text,0,strrpos($text,' '));
	$text = $text.'&hellip;';
	return $text;
}
function log_action($executed) {
	global $dbLink;
	$executed = clean($executed);
	sql_query("INSERT INTO fyp_logs (`executed`, `actor`, `timedate`) 
	VALUES ('".$executed."', '".$_SESSION['name']."', UNIX_TIMESTAMP())");
}
function clean($input, $html = 0, $paragraph = 0) {
	global $dbLink;
	if (!is_numeric($input)) {
		if (get_magic_quotes_gpc()) {
			$input = stripslashes($input);
		}
		if ($html == 0) {
			$input	= htmlentities($input);
		} 
		if ($paragraph == 1) {
			$input	= str_replace('<p>', '', $input);
			$input	= str_replace('</p>', '<br />', $input);
		}
		$input	= str_replace('&nbsp;', ' ', $input);
		$input	= mysqli_real_escape_string($dbLink, $input);
	}
	return $input;
}
function format_date($raw) {
	$x	= date("F j, Y", ($raw + 8 * 60 * 60)); // GMT 8
	return $x;
}
function sql_query($query) {
	global $dbLink;
	$run	= mysqli_query($dbLink, $query);
	if (!$run) {
		die('There was an error runnning the sql_query command: <pre>'.$query.'</pre> Error returned: <pre>'.mysqli_error($dbLink).'</pre>');
	} else {
		return $run;
	}
}
function sql_num_rows($start) {
	$run	= mysqli_num_rows($start);
	return $run;
}
function sql_fetch($array) {
	$run	= mysqli_fetch_assoc($array);
	return $run;
}
function sql_last_id() {
	global $dbLink;
	$run	= mysqli_insert_id($dbLink);
	return $run;
}

// Creates thumbnails of images being uploaded
// and save to subdirectory called "thumbs".
// For example if file is /my/images/file.jpg
// then thumbnail is /my/images/thumbs/file.jpg
// and returns the remote thumbnail file
function thumbify($file, $width=225, $height=225)
{
    if('..'==substr($file, 0, 2)) {
        $file = substr($file, 3);
    }
    else if('/'==substr($file, 0, 1)) {
        $file = substr($file, 1);
    }

    $path = explode(DS, $file);
    $filename = end($path);
    $thumbs = ROOT.$path[0].DS.'thumbs'.DS;
    $thumbfile = $thumbs.$filename;
    // creates thumbs directory if not exist
    if(!file_exists($thumbs)) 
    {
        mkdir($thumbs);
    }
    // if thumbfile already generated return it,
    // no need to generate again
    //if(file_exists($thumbfile)) {
    //    return HOST_URL.$path[0].DS.'thumbs'.DS.$filename;
    //}
    
    $source = ROOT.$file;
    if(!file_exists($source)) {
        return false;
    }
    $thumb = new PHPThumb\GD($source);
    $thumb->resize($width, $height);
    $thumb->save($thumbfile);

    return HOST_URL.$path[0].DS.'thumbs'.DS.$filename;
}

function setFlash($flash)
{
    $_SESSION['flash'] = $flash;
}

function showFlash()
{
    $flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : '';
    unset($_SESSION['flash']);
    return $flash;
}

function redirectTo($location)
{
    header("location: $location");
    die;
}

function setFlashAndRedirectTo($flash, $location)
{
    setFlash($flash);
    redirectTo($location);
}
?>
