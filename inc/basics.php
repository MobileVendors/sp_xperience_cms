<?php
    //define('DS', DIRECTORY_SEPARATOR);
    define('DS', '/');
    define('ROOT', dirname(dirname(__FILE__)).DS);
    define('INCLUDES', ROOT.'inc'.DS);
    define('VENDOR', ROOT.'vendor'.DS);
    define('MEDIA_DIR', ROOT.'media'.DS);
    define('COVERS_DIR', ROOT.'covers'.DS);

    ini_set('display_errors', 0); 
    error_reporting(0);

    switch(true) {
        case file_exists(ROOT.'.dev'):
            $mode = 'development';
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
            break;
        case file_exists(ROOT.'.staging'):
            $mode = 'staging';
            break;
        default:
            $mode = 'production';
    }

    define('ENV', $mode);

    require_once(VENDOR.'autoload.php'); // auto loads packages
?>
