CREATE TABLE IF NOT EXISTS `fyp_aboutus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media` varchar(255) NULL,
  `info` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `fyp_banners` (
  `id` int(11) NOT NULL auto_increment,
  `image` varchar(255) default NULL,
  `url` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE  `fyp_groups` ADD  `banners` ENUM(  '1',  '0' ) NOT NULL DEFAULT  '0' AFTER  `groups` ;
ALTER TABLE  `fyp_users` ADD  `banners` INT NOT NULL AFTER  `groups` ;
ALTER TABLE  `fyp_magcats` ADD  `color_code` VARCHAR(9) AFTER  `name` ;

INSERT INTO `fyp_aboutus` (`id`) VALUES (1);

UPDATE `fyp_groups` SET `banners` = 1 WHERE `id` = 1;
SET SQL_SAFE_UPDATES=0;
UPDATE `fyp_users` SET `banners` = 1 WHERE `usergroup` = 1;