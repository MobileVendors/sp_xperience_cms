CREATE TABLE IF NOT EXISTS `fyp_schools` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `description` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `name` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `fyp_groups` ADD  `groups` TINYINT NOT NULL DEFAULT  '0',
ADD INDEX (  `groups` );

UPDATE  `spxperience_db`.`fyp_groups` SET  `groups` =  '1' WHERE  `fyp_groups`.`id` =1;

INSERT INTO `spxperience_db`.`fyp_groups` (`id`, `name`, `schdips`, `categories`, `magazines`, `articles`, `users`, `groups`) VALUES (NULL, 'Group Administrator', '0', '0', '1', '1', '1', '0');

ALTER TABLE  `fyp_users` ADD  `school_id` INT NOT NULL AFTER  `usergroup` ,
ADD INDEX (  `school_id` );

ALTER TABLE  `fyp_users` ADD  `groups` TINYINT NOT NULL ,
ADD INDEX (  `groups` );
